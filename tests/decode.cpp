#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <rew/decoder/decoder.h>
#include <rew/encoder/encoder.h>
#include <rew/decoder/source_memory.h>
#include <rew/common/named_raw_file.h>
#include <map>
#include <array>

#define DUMP_F32(path, data, length)                                                                                   \
    {                                                                                                                  \
        std::fstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);                                    \
        out.write(reinterpret_cast<const char*>(data), length * sizeof(float));                                        \
    }

#define DUMP_I32(path, data, length)                                                                                   \
    {                                                                                                                  \
        std::fstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);                                    \
        out.write(reinterpret_cast<const char*>(data), length * sizeof(int));                                          \
    }

#define DUMP_I8(path, data, length)                                                                                    \
    {                                                                                                                  \
        std::fstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);                                    \
        out.write(reinterpret_cast<const char*>(data), length * sizeof(char));                                         \
    }

static const std::map<size_t, std::function<bool(float)>> expectedMagnitude01 = {
    {1400, [](float v) -> bool {return v < 0.1f; }}, {1600, [](float v) -> bool {return v < 0.1f; }},
    {1800, [](float v) -> bool {return v < 0.1f; }}, {2000, [](float v) -> bool {return v < 0.1f; }},
    {2000, [](float v) -> bool {return v < 0.1f; }}, {2200, [](float v) -> bool {return v < 0.1f; }},
    {2400, [](float v) -> bool {return v > 1.0f; }}, {2600, [](float v) -> bool {return v < 0.1f; }},
    {2800, [](float v) -> bool {return v < 0.1f; }}, {3000, [](float v) -> bool {return v < 0.1f; }},
    {3200, [](float v) -> bool {return v < 0.1f; }}, {3400, [](float v) -> bool {return v < 0.1f; }},
};

static const std::array<int, 10> frequencies01 = {1000, 1200, 1800, 2400, 4800, 5000, 5200, 6200, 8000, 9600};

static bool compare(const unsigned char* a, const unsigned char* b, const size_t length) {
    for (size_t i = 0; i < length; i++) {
        if (a[i] != b[i])
            return false;
    }
    return true;
}

static const unsigned char SAMPLE_HELLO_WORLD[] = {'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', '!'};

REW_NAMESPACE {
    template <size_t N> NamedRawFile fileFrom(const char(&contents)[N]) {
        NamedRawFile file;
        file.data.reset(new uint8_t[N]);
        file.length = N;
        file.index = 0;
        std::memcpy(file.data.get(), contents, N);
        return file;
    }

    NamedRawFile fileFrom(const char* contents, size_t length) {
        NamedRawFile file;
        file.data.reset(new uint8_t[length]);
        file.length = length;
        file.index = 0;
        std::memcpy(file.data.get(), contents, length);
        return file;
    }

    class DummyFileSink : public Input<NamedRawFile> {
      public:
        void process(const NamedRawFile* data, const size_t length) override {
            for (size_t i = 0; i < length; i++) {
                const auto& dat = data[i];
                values.emplace_back();
                values.back().data.reset(new uint8_t[dat.length]);
                values.back().length = dat.length;
                values.back().index = dat.index;
                std::memcpy(values.back().data.get(), dat.data.get(), dat.length);
            }
        }

        std::list<NamedRawFile> values;
    };

    template <typename T> class DummyInput : public Input<T> {
      public:
        void process(const T* data, const size_t length) override {
            for (size_t i = 0; i < length; i++) {
                if (values.capacity() <= values.size()) {
                    values.reserve(values.capacity() + 1024);
                }

                values.push_back(data[i]);
            }
        }

        std::vector<T> values;
    };

    typedef DummyInput<float> DummyFloatSink;

    template <typename T> class DummyOutput : public Output<T> {
      public:
        void consume(const T& value) { Output<T>::forward(&value, 1); }

        void consume(const T* values, const size_t total) { Output<T>::forward(values, total); }
    };

    typedef DummyOutput<NamedRawFile> DummyFileSource;
    typedef DummyOutput<float> DummyFloatSource;

    TEST_CASE("Decoder Input Output") {
        // auto decoder = std::move(Decoder());
        const auto input1 = std::make_shared<DummyInput<int>>();
        const auto input2 = std::make_shared<DummyInput<int>>();
        auto output = std::make_shared<DummyOutput<int>>();

        output->connect(input1);
        output->connect(input2);

        output->consume(42);
        output->consume(10);

        REQUIRE(input1->values.size() == 2);
        REQUIRE(input1->values[0] == 42);
        REQUIRE(input1->values[1] == 10);
        REQUIRE(input2->values.size() == 2);
        REQUIRE(input2->values[0] == 42);
        REQUIRE(input2->values[1] == 10);
    }

    TEST_CASE("Decoder Merge") {
        auto output1 = std::make_shared<DummyOutput<int>>();
        auto output2 = std::make_shared<DummyOutput<int>>();
        auto merge = std::make_shared<Merge<int, 2>>();
        const auto input = std::make_shared<DummyInput<MergedValue<int, 2>>>();

        output1->connect(merge->sources[0]);
        output2->connect(merge->sources[1]);

        merge->connect(input);

        output1->consume(10);
        output1->consume(20);
        output1->consume(30);

        REQUIRE(input->values.empty());

        output2->consume(-10);
        output2->consume(-20);
        output2->consume(-30);

        REQUIRE(input->values.size() == 3);

        REQUIRE(input->values[0].values[0] == 10);
        REQUIRE(input->values[1].values[0] == 20);
        REQUIRE(input->values[2].values[0] == 30);
        REQUIRE(input->values[0].values[1] == -10);
        REQUIRE(input->values[1].values[1] == -20);
        REQUIRE(input->values[2].values[1] == -30);

        input->values.clear();

        output1->consume(1);
        output1->consume(2);
        output1->consume(3);

        REQUIRE(input->values.size() == 0);

        output2->consume(-1);
        output2->consume(-2);
        output2->consume(-3);

        REQUIRE(input->values.size() == 3);

        REQUIRE(input->values[0].values[0] == 1);
        REQUIRE(input->values[1].values[0] == 2);
        REQUIRE(input->values[2].values[0] == 3);
        REQUIRE(input->values[0].values[1] == -1);
        REQUIRE(input->values[1].values[1] == -2);
        REQUIRE(input->values[2].values[1] == -3);
    }

    TEST_CASE("Decoder goertzel") {
        const SamplesGenerator encoder(DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ, DEFAULT_SAMPLE_LENGTH_MS);
        const std::unique_ptr<float[]> samples(encoder.encodeRaw(SAMPLE_HELLO_WORLD, sizeof(SAMPLE_HELLO_WORLD)));
        const auto total = encoder.getNumOfSamplesRaw(sizeof(SAMPLE_HELLO_WORLD));

        const auto blockSize = static_cast<size_t>((DEFAULT_SAMPLE_RATE / 1000.0) * DEFAULT_SAMPLE_LENGTH_MS * 1.0);

        auto goertzelLow =
            std::make_shared<Goertzel>(blockSize, float(DEFAULT_LOW_TONE_FREQ), float(DEFAULT_SAMPLE_RATE));
        auto goertzelHigh =
            std::make_shared<Goertzel>(blockSize, float(DEFAULT_HIGH_TONE_FREQ), float(DEFAULT_SAMPLE_RATE));

        auto source = std::make_shared<SourceMemory>();
        auto sinkLow = std::make_shared<DummyInput<float>>();
        auto sinkHigh = std::make_shared<DummyInput<float>>();

        source->connect(goertzelLow);
        source->connect(goertzelHigh);

        goertzelLow->connect(std::dynamic_pointer_cast<Input<float>>(sinkLow));
        goertzelHigh->connect(std::dynamic_pointer_cast<Input<float>>(sinkHigh));

        source->consume(samples.get(), total);

        DUMP_F32("decode_goertzel_samples_raw.bin", samples.get(), total);
        DUMP_F32("decode_goertzel_samples_low.bin", &sinkLow->values[0], sinkLow->values.size());
        DUMP_F32("decode_goertzel_samples_high.bin", &sinkHigh->values[0], sinkHigh->values.size());
    }

    TEST_CASE("Decoder goertzel fir") {
        const SamplesGenerator encoder(DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ, DEFAULT_SAMPLE_LENGTH_MS);
        const std::unique_ptr<float[]> samples(encoder.encodeRaw(SAMPLE_HELLO_WORLD, sizeof(SAMPLE_HELLO_WORLD)));
        const auto total = encoder.getNumOfSamplesRaw(sizeof(SAMPLE_HELLO_WORLD));

        const auto blockSize = static_cast<size_t>((DEFAULT_SAMPLE_RATE / 1000.0) * DEFAULT_SAMPLE_LENGTH_MS * 1.0);

        auto firFilterLow = std::make_shared<FirFilter>();
        auto firFilterHigh = std::make_shared<FirFilter>();
        auto goertzelLow =
            std::make_shared<Goertzel>(blockSize, float(DEFAULT_LOW_TONE_FREQ), float(DEFAULT_SAMPLE_RATE));
        auto goertzelHigh =
            std::make_shared<Goertzel>(blockSize, float(DEFAULT_HIGH_TONE_FREQ), float(DEFAULT_SAMPLE_RATE));

        auto source = std::make_shared<SourceMemory>();
        auto sinkLow = std::make_shared<DummyInput<float>>();
        auto sinkHigh = std::make_shared<DummyInput<float>>();

        source->connect(firFilterLow);
        source->connect(firFilterHigh);

        firFilterLow->connect(goertzelLow);
        firFilterHigh->connect(goertzelHigh);

        goertzelLow->connect(std::dynamic_pointer_cast<Input<float>>(sinkLow));
        goertzelHigh->connect(std::dynamic_pointer_cast<Input<float>>(sinkHigh));

        source->consume(samples.get(), total);

        DUMP_F32("decode_goertzel_fir_samples_raw.bin", samples.get(), total);
        DUMP_F32("decode_goertzel_fir_samples_low.bin", &sinkLow->values[0], sinkLow->values.size());
        DUMP_F32("decode_goertzel_fir_samples_high.bin", &sinkHigh->values[0], sinkHigh->values.size());
    }

    TEST_CASE("Decoder goertzel multiply") {
        const SamplesGenerator encoder(DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ, DEFAULT_SAMPLE_LENGTH_MS);
        const std::unique_ptr<float[]> samples(encoder.encodeRaw(SAMPLE_HELLO_WORLD, sizeof(SAMPLE_HELLO_WORLD)));
        const auto total = encoder.getNumOfSamplesRaw(sizeof(SAMPLE_HELLO_WORLD));

        const auto blockSize = static_cast<size_t>((DEFAULT_SAMPLE_RATE / 1000.0) * DEFAULT_SAMPLE_LENGTH_MS * 1.0);

        auto firFilterLow = std::make_shared<FirFilter>();
        auto firFilterHigh = std::make_shared<FirFilter>();
        auto goertzelLow =
            std::make_shared<Goertzel>(blockSize, float(DEFAULT_LOW_TONE_FREQ), float(DEFAULT_SAMPLE_RATE));
        auto goertzelHigh =
            std::make_shared<Goertzel>(blockSize, float(DEFAULT_HIGH_TONE_FREQ), float(DEFAULT_SAMPLE_RATE));
        auto multiplyLow = std::make_shared<Multiply<float>>(1.0f / 300.0f);
        auto multiplyHigh = std::make_shared<Multiply<float>>(1.0f / 300.0f);
        auto clampLow = std::make_shared<Clamp<float, int>>(0.5f, -1, 0);
        auto clampHigh = std::make_shared<Clamp<float, int>>(0.5f, 1, 0);

        auto source = std::make_shared<SourceMemory>();
        auto sinkLow = std::make_shared<DummyInput<int>>();
        auto sinkHigh = std::make_shared<DummyInput<int>>();

        source->connect(firFilterLow);
        source->connect(firFilterHigh);

        firFilterLow->connect(goertzelLow);
        firFilterHigh->connect(goertzelHigh);

        goertzelLow->connect(multiplyLow);
        goertzelHigh->connect(multiplyHigh);

        multiplyLow->connect(clampLow);
        multiplyHigh->connect(clampHigh);

        clampLow->connect(std::dynamic_pointer_cast<Input<int>>(sinkLow));
        clampHigh->connect(std::dynamic_pointer_cast<Input<int>>(sinkHigh));

        source->consume(samples.get(), total);

        DUMP_I32("decode_goertzel_multiply_samples_raw.bin", samples.get(), total);
        DUMP_I32("decode_goertzel_multiply_samples_low.bin", &sinkLow->values[0], sinkLow->values.size());
        DUMP_I32("decode_goertzel_multiply_samples_high.bin", &sinkHigh->values[0], sinkHigh->values.size());
    }

    TEST_CASE("Frame Sync") {
        SamplesGenerator samplesGenerator;
        const uint8_t signature[] = PACKET_SIGNATURE;

        // Generate samples
        std::unique_ptr<float[]> encoded(samplesGenerator.encodeRaw(signature, sizeof(signature)));
        const auto total = samplesGenerator.getNumOfSamplesRaw(sizeof(signature));

        // Add padding with zeros
        // The data will be in the middle
        std::vector<float> samples(total * 3);
        std::fill(samples.begin(), samples.end(), 0.0f);
        std::memcpy(samples.data() + total, encoded.get(), total * sizeof(float));

        DUMP_F32("sync_samples_raw.bin", samples.data(), samples.size());

        const auto frameSync = std::make_shared<FrameSync>();
        for (size_t i = 0; i < samples.size() / 32; i++) {
            frameSync->process(samples.data() + i * 32, 32);
        }

        REQUIRE(frameSync->hasFrame());
    }

    TEST_CASE("Decoder raw hello world") {
        const char contents[] =
            "index.html\0<html><head><title>Hello World</title></head><body><h1>Hello World</h1></body></html>";
        const auto file = fileFrom(contents);
        const auto fileSource = std::make_shared<DummyFileSource>();
        const auto fileSink = std::make_shared<DummyFileSink>();
        const auto samplesSource = std::make_shared<DummyFloatSource>();
        const auto samplesSink = std::make_shared<DummyFloatSink>();

        const auto encoder = Encoder(fileSource, samplesSink);
        const auto decoder = Decoder(samplesSource, fileSink);

        fileSource->consume(file);
        REQUIRE(samplesSink->values.size() > 1);

        DUMP_F32("hello_world_encoded_samples.bin", samplesSink->values.data(), samplesSink->values.size());

        WavWriter wav;
        wav.open("hello_world_encoded_samples.wav", 32, 48000, 1);
        wav.write((const unsigned char*)samplesSink->values.data(), samplesSink->values.size() * sizeof(float));
        wav.writeFooter();
        wav.close();

        for (size_t i = 0; i < samplesSink->values.size() / 32; i++) {
            // samplesSource->consume(samplesSink->values.data(), samplesSink->values.size());
            samplesSource->consume(samplesSink->values.data() + i * 32, 32);
        }
        REQUIRE(fileSink->values.size() == 1);

        REQUIRE(file.length == fileSink->values.back().length);
        REQUIRE(compare(file.data.get(), fileSink->values.back().data.get(), file.length));
    }

    TEST_CASE("Decoder raw 1MB") {
        const std::string name = "hello.jpg";
        const size_t total = 1024 * 1024 + 9 + 1;
        const auto bytes = std::make_unique<uint8_t[]>(total);
        char* contents = reinterpret_cast<char*>(bytes.get());

        std::memcpy(contents, name.c_str(), name.size() + 1);
        for (size_t i = 9 + 1; i < sizeof(contents); i++) {
            contents[i] = i % 256;
        }

        const auto file = fileFrom(contents, total);
        const auto fileSource = std::make_shared<DummyFileSource>();
        const auto fileSink = std::make_shared<DummyFileSink>();
        const auto samplesSource = std::make_shared<DummyFloatSource>();
        const auto samplesSink = std::make_shared<DummyFloatSink>();

        const auto encoder = Encoder(fileSource, samplesSink);
        const auto decoder = Decoder(samplesSource, fileSink);

        fileSource->consume(file);
        REQUIRE(samplesSink->values.size() > 1);

        auto start = std::chrono::system_clock::now();
        for (size_t i = 0; i < samplesSink->values.size() / 32; i++) {
            samplesSource->consume(samplesSink->values.data() + i * 32, 32);
        }
        auto end = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::cout << "1MB took: " << duration.count() << "ms" << std::endl;
        std::cout << "number of samples: " << samplesSink->values.size() << std::endl;

        REQUIRE(fileSink->values.size() == 1);

        REQUIRE(file.length == fileSink->values.back().length);
        REQUIRE(compare(file.data.get(), fileSink->values.back().data.get(), file.length));
    }
};
