#include <dirent.h>
#include "folder.h"

///=====================================================================================================================
rew::Folder::Folder(const std::string& path) {
    dir = opendir(path.c_str());
}

///=====================================================================================================================
rew::Folder::~Folder() {
    if (dir != NULL)
        closedir((DIR*)dir);
}

///=====================================================================================================================
bool rew::Folder::next(std::string& file) {
    if (dir == NULL)
        return false;

    struct dirent* ent = readdir((DIR*)dir);
    if (ent == NULL)
        return false;

    file = std::string(ent->d_name);
    return true;
}
