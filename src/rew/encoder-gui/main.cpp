#include <ui.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <Windows.h>
#include <fileapi.h>
#else
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#endif
#include <iostream>
#include <thread>
#include "folder.h"
#include <rew/encoder/encoder.h>
#include <rew/encoder/physical_audio_sink.h>

#ifdef _MSC_VER
#pragma comment(                                                                                                       \
    linker,                                                                                                            \
    "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

///=====================================================================================================================
std::string getFullPath(const std::string& path) {
#ifdef _WIN32
    LPSTR* lppPart = {nullptr};
    std::string buffer(4096, '\0');
    if (GetFullPathName(path.c_str(), buffer.size(), &buffer[0], lppPart) == 0) {
        throw std::runtime_error("Failed to get full path");
    }
#else
    std::string buffer(PATH_MAX, '\0');
    realpath(path.c_str(), &buffer[0]);
#endif
    return std::string(&buffer[0]);
}

///=====================================================================================================================
size_t getFileSize(const std::string& path) {
    struct stat info = {0};
    if (stat(path.c_str(), &info) != 0)
        return false;
    return static_cast<size_t>(info.st_size);
}

///=====================================================================================================================
bool folderIsValid(const std::string& path) {
    struct stat info = {0};
    if (stat(path.c_str(), &info) != 0)
        return false;
    else if (info.st_mode & S_IFDIR)
        return true;
    else
        return false;
}

///=====================================================================================================================
bool pathIsValid(const std::string& path) {
    struct stat info = {0};
    return stat(path.c_str(), &info) == 0;
}

///=====================================================================================================================
static int onClosing(uiWindow* w, void* data) {
    uiQuit();
    return 1;
}

///=====================================================================================================================
static int onShouldQuit(void* data) {
    uiWindow* mainwin = uiWindow(data);

    uiControlDestroy(uiControl(mainwin));
    return 1;
}

///=====================================================================================================================
std::vector<std::string> recursivelyParseFolder(const std::string& path) {
    std::vector<std::string> files;

    // std::cout << "Entering:   " << path << std::endl;
    auto folder = rew::Folder(path);
    std::string file;

    while (folder.next(file)) {
        if (file == "." || file == "..")
            continue;

        const auto filePath = path + DELIMITER + file;

        if (folderIsValid(filePath)) {
            const auto b = recursivelyParseFolder(filePath);
            files.insert(std::end(files), std::begin(b), std::end(b));
        } else {
            // std::cout << "Collecting: " << filePath << std::endl;
            files.push_back(filePath);
        }
    }

    return files;
}

///=====================================================================================================================
class DirectoryTraverser : public rew::Output<rew::FileName> {
  public:
    DirectoryTraverser() = default;
    virtual ~DirectoryTraverser() = default;

    void process(const std::string& path) {
        // Collect all files and forward them down the pipeline
        const auto files = recursivelyParseFolder(path, path);
        this->forward(&files[0], files.size());
    }

  private:
    std::vector<rew::FileName> recursivelyParseFolder(const std::string& base, const std::string& path) {
        std::vector<rew::FileName> files;

        auto folder = rew::Folder(path);
        std::string file;

        while (folder.next(file)) {
            if (file == "." || file == "..")
                continue;

            const auto filePath = path + DELIMITER + file;

            if (folderIsValid(filePath)) {
                const auto b = recursivelyParseFolder(base, filePath);
                files.insert(std::end(files), std::begin(b), std::end(b));
            } else {
                const auto name = filePath.substr(base.size());
                files.push_back({filePath, name});
            }
        }

        return files;
    }
};

///=====================================================================================================================
class Context;
static struct Globals {
    std::shared_ptr<Context> context;
    bool destinationFile = false;
    bool destinationStream = false;
    std::string sourceFileStr;
    std::string destinationFileStr;
    std::string extensions = "html";
} globals;

///=====================================================================================================================
class Context : public std::enable_shared_from_this<Context> {
  public:
    Context(Globals& globals, const std::function<void()>& callback) : globals(globals), callback(callback) {

        const auto inputPath = getFullPath(globals.sourceFileStr);

        if (!pathIsValid(inputPath))
            throw std::runtime_error("Input path: \"" + inputPath + "\" does not exist!");

        if (!folderIsValid(inputPath))
            throw std::runtime_error("Input path: \"" + inputPath + "\" is not a folder!");

        // Create our source which will go through the directory
        source = std::make_shared<DirectoryTraverser>();

        loader = std::make_shared<rew::FileLoader>();
        source->connect(loader);

        if (globals.destinationFile) {
            const auto outputPath = globals.destinationFileStr;

            // Create the output wav file
            wav = std::make_shared<rew::AudioSink>(std::make_shared<rew::WavWriter>());

            // Finally combine everything together into a single encoder
            encoder = std::make_shared<rew::Encoder>(loader, wav, DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ,
                                                     DEFAULT_SAMPLE_LENGTH_MS);

            thread = std::thread([=]() -> void {
                wav->open(outputPath);
                source->process(inputPath);
                wav->close();

                if (this->callback) {
                    this->stopped = true;
                    this->callback();
                }
            });
        }

        else if (globals.destinationStream) {
            audio = std::make_shared<rew::PhysicalAudioSink>();

            // Finally combine everything together into a single encoder
            encoder = std::make_shared<rew::Encoder>(loader, audio, DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ,
                                                     DEFAULT_SAMPLE_LENGTH_MS);

            thread = std::thread([=]() -> void {
                audio->start();
                source->process(inputPath);
                audio->join();
                audio->close();

                if (this->callback) {
                    this->stopped = true;
                    this->callback();
                }
            });
        }

        else {
            throw std::runtime_error("Please select a destination");
        }
    }

    ~Context() {
        stop();
    }

    void stop() {
        if (thread.joinable()) {
            if (audio)
                audio->close();
            thread.join();
        }
    }

    inline bool isStopped() const {
        return stopped;
    }

  private:
    Globals& globals;
    std::shared_ptr<rew::FileLoader> loader;
    std::shared_ptr<DirectoryTraverser> source;
    std::shared_ptr<rew::PhysicalAudioSink> audio;
    std::shared_ptr<rew::AudioSink> wav;
    std::shared_ptr<rew::Encoder> encoder;
    std::thread thread;
    std::function<void()> callback;
    bool stopped = false;
};

///=====================================================================================================================
static void widgetsSelectSource(uiWindow* mainwin, uiBox* box) {
    const auto group = uiNewGroup("Select source");
    uiGroupSetMargined(group, 1);
    uiBoxAppend(box, uiControl(group), 0);

    uiBoxAppend(box, uiControl(uiNewHorizontalSeparator()), 0);

    const auto grid = uiNewGrid();
    uiGridSetPadded(grid, 1);
    uiBoxAppend(box, uiControl(grid), 0);

    const auto fileButton = uiNewButton("Select Folder");
    const auto fileEntry = uiNewEntry();

    static struct SelectFileData {
        uiWindow* mainwin;
        Globals& globals;
        uiEntry* entry;
    } selectFileData{mainwin, globals, fileEntry};

    const auto onOpenFileClicked = [](uiButton* b, void* data) -> void {
        auto& d = *reinterpret_cast<SelectFileData*>(data);

        const auto filename = uiOpenFile(d.mainwin);
        if (filename == NULL) {
            uiEntrySetText(d.entry, "");
            return;
        }

        if (!folderIsValid(filename)) {
            uiMsgBoxError(d.mainwin, "Select folder error", "You must select a valid folder!");

            uiEntrySetText(d.entry, "");
            return;
        }

        uiEntrySetText(d.entry, filename);
        d.globals.sourceFileStr = std::string(filename);
        uiFreeText(filename);
    };

    uiEntryOnChanged(
        fileEntry,
        [](uiEntry* e, void* data) -> void {
            auto& globals = *reinterpret_cast<Globals*>(data);
            globals.sourceFileStr = std::string(uiEntryText(e));
        },
        &globals);

    uiEntrySetReadOnly(fileEntry, 0);
    uiButtonOnClicked(fileButton, onOpenFileClicked, &selectFileData);
    uiGridAppend(grid, uiControl(fileButton), 0, 0, 1, 1, 0, uiAlignFill, 0, uiAlignFill);
    uiGridAppend(grid, uiControl(fileEntry), 1, 0, 1, 1, 1, uiAlignFill, 0, uiAlignFill);

    auto rb = uiNewRadioButtons();
    uiRadioButtonsAppend(rb, "Html only");
    uiRadioButtonsAppend(rb, "Html with images");
    uiRadioButtonsAppend(rb, "Everything");
    uiBoxAppend(box, uiControl(rb), 0);

    uiRadioButtonsOnSelected(
        rb,
        [](uiRadioButtons* rb, void* data) -> void {
            auto& globals = *reinterpret_cast<Globals*>(data);
            switch (uiRadioButtonsSelected(rb)) {
                case 0:
                    globals.extensions = "html";
                    break;
                case 1:
                    globals.extensions = "html jpg jpeg png bmp svg tif tiff";
                    break;
                case 2:
                    globals.extensions = "";
                    break;
                default:
                    break;
            }
        },
        &globals);
}

///=====================================================================================================================
static void widgetsSelectDestination(uiWindow* mainwin, uiBox* box) {
    const auto group = uiNewGroup("Select destination");
    uiGroupSetMargined(group, 1);
    uiBoxAppend(box, uiControl(group), 0);

    uiBoxAppend(box, uiControl(uiNewHorizontalSeparator()), 0);

    const auto cboxDevice = uiNewCheckbox("To audio output device");
    const auto cboxFile = uiNewCheckbox("To audio file");

    static struct CBoxData {
        Globals& globals;
        uiCheckbox* device;
        uiCheckbox* file;
    } cboxData{globals, cboxDevice, cboxFile};

    uiBoxAppend(box, uiControl(cboxDevice), 0);
    uiCheckboxOnToggled(
        cboxDevice,
        [](uiCheckbox* cbox, void* data) -> void {
            auto& d = *reinterpret_cast<CBoxData*>(data);
            d.globals.destinationStream = uiCheckboxChecked(cbox);
            uiCheckboxSetChecked(d.file, !d.globals.destinationStream);
        },
        &cboxData);

    const auto combo = uiNewCombobox();
    const auto devices = rew::PhysicalAudioSink::getDevices();
    for (const auto& device : devices) {
        if (device.outputChannels == 0)
            continue;
        uiComboboxAppend(combo, device.name.c_str());
    }
    uiBoxAppend(box, uiControl(combo), 0);
    uiComboboxOnSelected(
        combo,
        [](uiCombobox* combo, void* data) -> void {
            auto& d = *reinterpret_cast<CBoxData*>(data);
            d.globals.destinationStream = true;
            uiCheckboxSetChecked(d.device, d.globals.destinationStream);
            uiCheckboxSetChecked(d.file, !d.globals.destinationStream);
        },
        &cboxData);

    uiBoxAppend(box, uiControl(cboxFile), 0);
    uiCheckboxOnToggled(
        cboxFile,
        [](uiCheckbox* cbox, void* data) -> void {
            auto& d = *reinterpret_cast<CBoxData*>(data);
            d.globals.destinationFile = uiCheckboxChecked(cbox);
            uiCheckboxSetChecked(d.device, !d.globals.destinationFile);
        },
        &cboxData);

    const auto grid = uiNewGrid();
    uiGridSetPadded(grid, 1);
    uiBoxAppend(box, uiControl(grid), 0);

    const auto fileButton = uiNewButton("Select File");
    const auto fileEntry = uiNewEntry();

    static struct SelectFileData {
        uiWindow* mainwin;
        Globals& globals;
        uiCheckbox* device;
        uiCheckbox* file;
        uiEntry* entry;
    } selectFileData{mainwin, globals, cboxDevice, cboxFile, fileEntry};

    const auto onOpenFileClicked = [](uiButton* b, void* data) -> void {
        auto& d = *reinterpret_cast<SelectFileData*>(data);

        const auto filename = uiSaveFile(d.mainwin);
        if (filename == NULL) {
            uiEntrySetText(d.entry, "");
            return;
        }

        uiEntrySetText(d.entry, filename);
        d.globals.destinationFileStr = std::string(filename);
        uiFreeText(filename);

        d.globals.destinationFile = true;
        uiCheckboxSetChecked(d.device, !d.globals.destinationFile);
        uiCheckboxSetChecked(d.file, d.globals.destinationFile);
    };

    uiEntryOnChanged(
        fileEntry,
        [](uiEntry* e, void* data) -> void {
            auto& globals = *reinterpret_cast<Globals*>(data);
            globals.sourceFileStr = std::string(uiEntryText(e));
        },
        &globals);

    uiEntrySetReadOnly(fileEntry, 0);
    uiButtonOnClicked(fileButton, onOpenFileClicked, &selectFileData);
    uiGridAppend(grid, uiControl(fileButton), 0, 0, 1, 1, 0, uiAlignFill, 0, uiAlignFill);
    uiGridAppend(grid, uiControl(fileEntry), 1, 0, 1, 1, 1, uiAlignFill, 0, uiAlignFill);
}

///=====================================================================================================================
static void widgets(uiWindow* mainwin) {
    const auto box = uiNewVerticalBox();
    uiBoxSetPadded(box, 1);
    uiWindowSetChild(mainwin, uiControl(box));

    widgetsSelectSource(mainwin, box);
    widgetsSelectDestination(mainwin, box);

    uiBoxAppend(box, uiControl(uiNewHorizontalSeparator()), 0);

    auto status = uiNewLabel("Press start to begin");
    uiBoxAppend(box, uiControl(status), 0);

    auto progressbar = uiNewProgressBar();
    uiProgressBarSetValue(progressbar, 0);
    uiBoxAppend(box, uiControl(progressbar), 0);

    auto buttonGrid = uiNewGrid();
    uiGridSetPadded(buttonGrid, 1);

    static struct StatusData {
        uiWindow* mainwin;
        Globals& globals;
        uiProgressBar* progress;
        uiLabel* label;
    } statusData{mainwin, globals, progressbar, status};

    auto button = uiNewButton("Start");
    uiButtonOnClicked(
        button,
        [](uiButton* b, void* data) -> void {
            const auto callback = [=]() {
                auto& d = *reinterpret_cast<StatusData*>(data);

                uiProgressBarSetValue(d.progress, 0);
                uiButtonSetText(b, "Start");
            };

            auto& d = *reinterpret_cast<StatusData*>(data);

            if (!d.globals.context || d.globals.context->isStopped()) {
                try {
                    d.globals.context = std::make_unique<Context>(d.globals, callback);
                    uiProgressBarSetValue(d.progress, -1);
                    uiButtonSetText(b, "Stop");
                } catch (std::exception& e) {
                    uiMsgBoxError(d.mainwin, "Error", e.what());
                }
            } else {
                d.globals.context.reset();
                uiProgressBarSetValue(d.progress, 0);
                uiButtonSetText(b, "Start");
            }
        },
        &statusData);
    uiGridAppend(buttonGrid, uiControl(button), 1, 0, 1, 1, 1, uiAlignCenter, 0, uiAlignFill);

    uiBoxAppend(box, uiControl(buttonGrid), 0);
}

///=====================================================================================================================
int main(const int argc, char* argv[]) {
    try {
        uiInitOptions o = {0};
        const char* err;
        if ((err = uiInit(&o)) != nullptr) {
            const auto e = std::string(err);
            uiFreeInitError(err);
            throw std::runtime_error(e);
        }

        const auto mainwin = uiNewWindow("Encoder", 640, 400, false);
        uiWindowSetMargined(mainwin, 1);
        uiWindowOnClosing(mainwin, onClosing, NULL);
        uiOnShouldQuit(onShouldQuit, mainwin);

        widgets(mainwin);

        uiControlShow(uiControl(mainwin));
        uiMain();
        uiUninit();
        return 0;

        return EXIT_SUCCESS;
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
