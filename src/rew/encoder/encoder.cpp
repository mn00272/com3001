#include <rew/encoder/encoder.h>
#include <algorithm>

///=====================================================================================================================
rew::Encoder::Encoder(const std::shared_ptr<Output<NamedRawFile>>& source, const std::shared_ptr<Input<float>>& sink,
                      const size_t lowToneFreq, const size_t highToneFreq, const double sampleLengthMs) {
    fileLoader = std::make_shared<FileLoader>();
    compressor = std::make_shared<Compressor>(PACKET_DATA_LENGTH);
    packetGenerator = std::make_shared<PacketGenerator>(compressor);
    samplesGenerator = std::make_shared<SamplesGenerator>(lowToneFreq, highToneFreq, sampleLengthMs);

    source->connect(packetGenerator);
    packetGenerator->connect(samplesGenerator);
    samplesGenerator->connect(sink);
}

///=====================================================================================================================
rew::Encoder::Encoder(Encoder&& other) noexcept {
    swap(other);
}

///=====================================================================================================================
void rew::Encoder::swap(Encoder& other) noexcept {
    using std::swap;
    swap(fileLoader, other.fileLoader);
    swap(compressor, other.compressor);
    swap(packetGenerator, other.packetGenerator);
    swap(samplesGenerator, other.samplesGenerator);
}

///=====================================================================================================================
rew::Encoder& rew::Encoder::operator=(Encoder&& other) noexcept {
    if (this != &other) {
        swap(other);
    }
    return *this;
}
