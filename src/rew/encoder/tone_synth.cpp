#include <cstring>
#include <cmath>
#ifdef _DEBUG
#include <iostream>
#endif
#include <rew/encoder/tone_synth.h>

///=====================================================================================================================
rew::ToneSynth::ToneSynth(const size_t frequency, const size_t sampleRate, const double ms)
    : frequency(frequency), total(0) {
    allocate(sampleRate, ms);
}

///=====================================================================================================================
void rew::ToneSynth::allocate(const size_t sampleRate, const double ms) {
    total = NUMBER_OF_SAMPLES_PER_BIT(sampleRate, frequency, ms);
    const auto cycleSamples = sampleRate / frequency;

    src.reset(new float[total]);
    const auto samples = src.get();

    const auto step = 360.0 / cycleSamples;
    auto d = 0.0;
    for (size_t i = 0; i < total; i++) {
        samples[i] = static_cast<float>(std::sin(d * DEG_TO_RAD));
        d += step;
    }
}

///=====================================================================================================================
size_t rew::ToneSynth::operator()(float* dst) const {
    std::memcpy(dst, src.get(), total * sizeof(float));
    return total;
}
