#include <portaudio.h>
#include <chrono>
#include <thread>
#include <rew/encoder/physical_audio_sink.h>

class rew::AudioInitializer {
  public:
    AudioInitializer() {
        const auto err = Pa_Initialize();
        if (err != paNoError) {
            throw std::runtime_error(Pa_GetErrorText(err));
        }
    }

    ~AudioInitializer() {
        const auto err = Pa_Terminate();
        if (err != paNoError) {
            std::cerr << Pa_GetErrorText(err) << std::endl;
        }
    }

    static std::shared_ptr<AudioInitializer> instance() {
        static const auto ptr = std::make_shared<AudioInitializer>();
        return ptr;
    }
};

rew::PhysicalAudioSink::PhysicalAudioSink(AudioDevice* device)
    : stream(nullptr), started(false), initializer(AudioInitializer::instance()), nextPos(0) {

    const auto lambda = [](const void* input, void* output, const unsigned long frameCount,
                           const PaStreamCallbackTimeInfo* timeInfo, const PaStreamCallbackFlags statusFlags,
                           void* userData) -> int {
        (void)output;
        (void)timeInfo;
        (void)statusFlags;

        auto ptr = reinterpret_cast<PhysicalAudioSink*>(userData);
        ptr->callback(reinterpret_cast<float*>(output), frameCount);
        return 0;
    };

    const auto err = Pa_OpenDefaultStream(&stream, 0, 2, paFloat32, 48000, BUFFER_SIZE, lambda, this);
    if (err != paNoError) {
        throw std::runtime_error(Pa_GetErrorText(err));
    }
}

rew::PhysicalAudioSink::~PhysicalAudioSink() {
    // void
    // This must be empty and non-default
}

void rew::PhysicalAudioSink::start() {
    const auto err = Pa_StartStream(stream);
    if (err != paNoError) {
        throw std::runtime_error(Pa_GetErrorText(err));
    }
}

void rew::PhysicalAudioSink::close() {
    if (stream && started) {
        started = false;
        Pa_StopStream(stream);
    }
    if (stream) {
        Pa_CloseStream(stream);
    }
}

void rew::PhysicalAudioSink::join() {
    std::unique_lock<std::mutex> lk(lock);
    cv.wait(lk, [this] { return chunks.empty(); });
    lk.unlock();
    // Lazy wait
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
}

std::vector<rew::AudioDevice> rew::PhysicalAudioSink::getDevices() {
    const auto initializer = AudioInitializer::instance();
    (void)initializer;

    std::vector<AudioDevice> devices;

    const auto num = Pa_GetDeviceCount();
    for (auto i = 0; i < num; i++) {
        const auto info = Pa_GetDeviceInfo(i);
        const auto api = Pa_GetHostApiInfo(info->hostApi);

        devices.emplace_back(AudioDevice{i, info->name, api->name, info->maxInputChannels, info->maxOutputChannels,
                                         info->defaultSampleRate, info->defaultLowInputLatency,
                                         info->defaultLowOutputLatency, info->defaultHighInputLatency,
                                         info->defaultHighOutputLatency});
    }

    return devices;
}

void rew::PhysicalAudioSink::process(const float* data, const size_t length) {
    std::vector<float> buffer{};
    buffer.resize(length);
    std::memcpy(buffer.data(), data, buffer.size() * sizeof(float));
    {
        std::lock_guard<std::mutex> lk(lock);
        chunks.emplace();
        std::swap(buffer, chunks.back());
    }
    cv.notify_one();
}

void rew::PhysicalAudioSink::callback(float* output, unsigned long frameCount) {
    while (frameCount > 0) {
        // Grab the next buffer in the queue
        if (next.empty()) {
            std::unique_lock<std::mutex> lk(lock);
            if (cv.wait_for(lk, std::chrono::milliseconds(1), [this] { return !chunks.empty(); })) {
                std::swap(next, chunks.front());
                chunks.pop();
                nextPos = 0;
                cv.notify_all();
            }
        }

        // If we have samples to copy
        if (!next.empty()) {
            auto toCopy = BUFFER_SIZE;
            if (nextPos + toCopy > next.size()) {
                toCopy = next.size() - nextPos;
            }

            if (toCopy > frameCount) {
                toCopy = frameCount;
            }

            for (auto i = nextPos; i < nextPos + toCopy; i++) {
                for (size_t c = 0; c < 2; c++) {
                    output[c] = next[i];
                }
                output += 2;
            }

            frameCount -= toCopy;
            nextPos += toCopy;

            if (nextPos >= next.size()) {
                next.clear();
            }

            if (frameCount == 0)
                break;
        } else {
            std::memset(output, 0x00, sizeof(float) * frameCount * 2);
            frameCount = 0;
        }
    }
}
