#include "../../include/rew/encoder/samples_generator.h"

static const unsigned char MASK[8] = {0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80};

static const uint8_t signature[] = PACKET_SIGNATURE;

///=====================================================================================================================
rew::SamplesGenerator::SamplesGenerator(const size_t lowToneFreq, const size_t highToneFreq,
                                        const double sampleLengthMs)
    : lowTone(lowToneFreq, DEFAULT_SAMPLE_RATE, sampleLengthMs),
      highTone(highToneFreq, DEFAULT_SAMPLE_RATE, sampleLengthMs), nullTone(new float[highTone.getSamplesPerBlock()]) {
    std::fill(nullTone.get(), nullTone.get() + highTone.getSamplesPerBlock(), 0.0f);
}

///=====================================================================================================================
void rew::SamplesGenerator::process(const Packet* data, const size_t length) {
    for (size_t i = 0; i < length; i++) {
        const auto& packet = data[i];

        const auto bytes = reinterpret_cast<const uint8_t*>(&packet);

        const auto numSamplesSignature = getNumOfSamplesRaw(sizeof(signature));
        const auto numSamplesPacket = getNumOfSamplesRaw(PACKET_REAL_SIZE);
        const auto total = numSamplesSignature + numSamplesPacket;

        std::unique_ptr<float[]> samples(new float[total]);
        std::fill(samples.get(), samples.get() + total, 0.0f);

        encodeRaw(signature, sizeof(signature), samples.get());
        encodeRaw(bytes, PACKET_REAL_SIZE, samples.get() + numSamplesSignature - highTone.getSamplesPerBlock());

        this->forward(samples.get(), total);
    }
}

///=====================================================================================================================
void rew::SamplesGenerator::encodeRaw(const uint8_t* data, const size_t length, float* dst) const {
    const auto begin = dst;
    for (size_t i = 0; i < length; i++) {
        const auto byte = data[i];
        for (const auto& mask : MASK) {
            const auto bit = byte & mask;
            if (bit) {
                (void)highTone(dst);
                dst += highTone.getSamplesPerBlock();
            } else {
                (void)lowTone(dst);
                dst += highTone.getSamplesPerBlock();
            }
        }

        std::memcpy(dst, nullTone.get(), sizeof(float) * lowTone.getSamplesPerBlock());
        dst += lowTone.getSamplesPerBlock();
    }

    std::memcpy(dst, nullTone.get(), sizeof(float) * lowTone.getSamplesPerBlock());
}

///=====================================================================================================================
float* rew::SamplesGenerator::encodeRaw(const uint8_t* data, const size_t length) const {
    const auto total = getNumOfSamplesRaw(length);
    std::unique_ptr<float[]> samples(new float[total]);
    std::fill(samples.get(), samples.get() + total, 0.0f);

    encodeRaw(data, length, samples.get());

    const auto ret = samples.get();
    samples.release();
    return ret;
}
