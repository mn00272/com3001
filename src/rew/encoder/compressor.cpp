#include <rew/encoder/compressor.h>
#include <zlib.h>
#include <memory>
#include <iostream>
#include <cstring>

///=====================================================================================================================
rew::Compressor::Compressor(const size_t chunkSize) : stream(new z_stream_s), chunkSize(chunkSize) {
    stream->zalloc = Z_NULL;
    stream->zfree = Z_NULL;
    stream->opaque = Z_NULL;
}

///=====================================================================================================================
rew::Compressor::~Compressor() {
}

///=====================================================================================================================
rew::Compressor::Compressor(Compressor&& other) noexcept {
    swap(other);
}

///=====================================================================================================================
void rew::Compressor::swap(Compressor& other) noexcept {
    using std::swap;
    swap(stream, other.stream);
    swap(chunkSize, other.chunkSize);
}

///=====================================================================================================================
rew::Compressor& rew::Compressor::operator=(Compressor&& other) noexcept {
    if (this != &other) {
        swap(other);
    }
    return *this;
}

///=====================================================================================================================
void rew::Compressor::getChunks(const uint8_t* data, const size_t length,
                                const std::function<void(const Chunk&)>& callback) const {
    Chunk chunk(chunkSize);
    deflateInit(stream.get(), Z_BEST_COMPRESSION);

    try {
        stream->avail_in = length;
        stream->next_in = static_cast<Bytef*>(const_cast<uint8_t*>(data));
        stream->avail_out = chunkSize;
        stream->next_out = &chunk[0];

        std::memset(&chunk[0], 0x00, chunkSize);

        while (true) {
            const auto status = deflate(stream.get(), Z_SYNC_FLUSH);
            if (status < 0) {
                throw std::runtime_error(std::string(stream->msg ? stream->msg : "unknown error"));
            }

            if (stream->avail_out == 0) {
                stream->next_out = &chunk[0];
                stream->avail_out = chunkSize;

                callback(chunk);
                std::memset(&chunk[0], 0x00, chunkSize);
            } else {
                chunk.resize(chunkSize - stream->avail_out);
                callback(chunk);
                break;
            }
        }

        deflateEnd(stream.get());
    } catch (std::exception_ptr& eptr) {
        deflateEnd(stream.get());
        std::rethrow_exception(eptr);
    }
}

///=====================================================================================================================
std::vector<rew::Compressor::Chunk> rew::Compressor::getChunks(const uint8_t* data, const size_t length) const {
    std::vector<Chunk> chunks;

    getChunks(data, length, [&](const Chunk& chunk) { chunks.push_back(chunk); });

    return chunks;
}
