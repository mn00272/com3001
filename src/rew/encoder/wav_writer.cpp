#include <rew/encoder/wav_writer.h>
#include <iostream>

///=====================================================================================================================
rew::WavWriter::WavWriter() : eos(false), dataOffset(0), dataWritten(0) {
}

///=====================================================================================================================
rew::WavWriter::~WavWriter() {
    close();
}

///=====================================================================================================================
bool rew::WavWriter::open(const std::string& path, const int bitsPerSample, const int sampleRate,
                          const int numChannels) {
    if (bitsPerSample == 0 || sampleRate == 0 || numChannels == 0) {
        return false;
    }

    output = std::fstream(path, std::fstream::out | std::fstream::trunc | std::fstream::binary);

    if (!output) {
        return false;
    }

    this->size = 0;
    this->bitsPerSample = bitsPerSample;
    this->channelCount = numChannels;
    this->sampleRate = sampleRate;

    writeHeader();

    dataOffset = static_cast<size_t>(output.tellg());

    loaded = true;
    return true;
}

///=====================================================================================================================
void rew::WavWriter::writeHeader() {
    // Wrtie header ID
    char chunkID[4] = {'R', 'I', 'F', 'F'};
    output.write(chunkID, sizeof(chunkID));

    const unsigned int subChunk1Size = 16; // PCM
    const auto subChunk2Size = size;       // This will be updated at the end
    const auto chunk1Size = 4 + (8 + subChunk1Size) + (8 + subChunk2Size);
    output.write(reinterpret_cast<const char*>(&chunk1Size), sizeof(unsigned int));

    char format[4] = {'W', 'A', 'V', 'E'};
    output.write(format, sizeof(format));

    // Wrtie FMT sub-chunk
    const char subchunk1Id[4] = {'f', 'm', 't', ' '};
    output.write(subchunk1Id, sizeof(subchunk1Id));

    output.write(reinterpret_cast<const char*>(&subChunk1Size), sizeof(unsigned int));

    unsigned short audioFormat = 0;
    if (bitsPerSample == 16)
        audioFormat = 1;
    else if (bitsPerSample == 32)
        audioFormat = 3;
    const auto blockAlign = static_cast<unsigned short>(channelCount * bitsPerSample / 8);

    unsigned int byteRate = sampleRate * channelCount * (bitsPerSample / 8);

    output.write(reinterpret_cast<const char*>(&audioFormat), sizeof(unsigned short));
    output.write(reinterpret_cast<const char*>(&channelCount), sizeof(unsigned short));
    output.write(reinterpret_cast<const char*>(&sampleRate), sizeof(unsigned int));
    output.write(reinterpret_cast<const char*>(&byteRate), sizeof(unsigned int));
    output.write(reinterpret_cast<const char*>(&blockAlign), sizeof(unsigned short));
    output.write(reinterpret_cast<const char*>(&bitsPerSample), sizeof(unsigned short));

    // Wrtie chunk ID
    const char subchunk2Id[4] = {'d', 'a', 't', 'a'};
    output.write(subchunk2Id, sizeof(subchunk2Id));
    // Wrtie size of data
    output.write(reinterpret_cast<const char*>(&subChunk2Size), sizeof(unsigned int));
}

///=====================================================================================================================
void rew::WavWriter::close() {
    eos = false;
    loaded = false;
    if (output.is_open())
        output.close();
    dataWritten = 0;
}

///=====================================================================================================================
bool rew::WavWriter::write(const unsigned char* data, const size_t length) {
    if (!loaded)
        return false;

    output.write(reinterpret_cast<const char*>(data), length);
    size += length;

    return true;
}

///=====================================================================================================================
bool rew::WavWriter::writeFooter() {
    if (!loaded)
        return false;
    // We need to update the header due to changed size!
    output.seekg(0, std::fstream::beg);
    writeHeader();

    // There is no footer in WAV file!
    // Nothing to do...

    return true;
}

///=====================================================================================================================
bool rew::WavWriter::eof() const {
    if (!loaded)
        return true;
    return eos;
}

///=====================================================================================================================
size_t rew::WavWriter::getPos() const {
    if (!loaded)
        return 0;
    return dataWritten;
}
