#include "../../include/rew/encoder/packet_generator.h"
#include "../../include/rew/common/crc8.h"

///=====================================================================================================================
rew::PacketGenerator::PacketGenerator(std::shared_ptr<Compressor> compressor) : compressor(std::move(compressor)) {

    if (this->compressor->getChunkSize() != PACKET_DATA_LENGTH)
        throw std::runtime_error("compressor output chunk size does not match packet data size");
}

///=====================================================================================================================
void rew::PacketGenerator::process(const NamedRawFile* data, const size_t length) {
    for (size_t i = 0; i < length; i++) {
        const auto& file = data[i];

        decltype(Packet::fileIndex) index = 0;

        compressor->getChunks(file.data.get(), file.length, [&](const Compressor::Chunk& chunk) {
            Packet packet{};
            packet.packetIndex = index++;
            packet.fileIndex = file.index;
            std::memcpy(packet.data, &chunk[0], chunk.size());

            /*std::cout << "packet: " << std::endl;
            for (size_t i = 0; i < chunk.size(); i++) {
                std::cout << int(chunk[i]) << ", ";
            }
            std::cout << std::endl;*/

            // Fill the rest of the packet data with zeros if this chunk is smaller.
            // Meaning, it is the final chunk which does not align to PACKET_DATA_LENGTH.
            if (chunk.size() < PACKET_DATA_LENGTH) {
                std::memset(packet.data + chunk.size(), 0x00, PACKET_DATA_LENGTH - chunk.size());
            }

            const auto crcLength = PACKET_REAL_SIZE - sizeof(packet.crc);
            packet.crc = crc8(reinterpret_cast<const uint8_t*>(&packet.fileIndex), crcLength);
            this->forward(&packet, 1);
        });
    }
}
