#include <fstream>
#include "../../include/rew/encoder/file_loader.h"

///=====================================================================================================================
void rew::FileLoader::process(const FileName* data, const size_t length) {
    for (size_t i = 0; i < length; i++) {
        const auto& f = data[i];
        const auto& filename = f.path;
        const auto& name = f.name;

        std::cout << "Loading: " << filename.c_str() << std::endl;

        // First open the file
        std::fstream input(filename.c_str(), std::ios::binary | std::ios::in | std::ios::ate);
        const auto size = static_cast<size_t>(input.tellg());
        const auto total = name.size() + 1 + static_cast<size_t>(input.tellg());
        input.seekg(0, std::ios::beg);

        // Read the entire file, but prepend the filename with null terminator
        NamedRawFile file;
        file.data.reset(new uint8_t[total]);
        file.length = total;
        file.index = i;

        std::memcpy(file.data.get(), name.c_str(), name.size() + 1);
        input.read(reinterpret_cast<char*>(file.data.get() + name.size() + 1), size);
        input.close();

        std::cout << "  size: " << file.length << " bytes" << std::endl;

        Output<NamedRawFile>::forward(&file, 1);
    }
}
