#include "../include/rew/encoder/audio_sink.h"

///=====================================================================================================================
rew::AudioSink::AudioSink(std::shared_ptr<AudioWriter> audioWriter) : audioWriter(std::move(audioWriter)) {
}

///=====================================================================================================================
rew::AudioSink::~AudioSink() {
    if (audioWriter && audioWriter->isOpen()) {
        audioWriter->writeFooter();
        audioWriter->close();
    }
}

///=====================================================================================================================
void rew::AudioSink::open(const std::string& path) {
    if (!audioWriter->open(path, 32, DEFAULT_SAMPLE_RATE, 1)) {
        throw std::runtime_error("output audio file failed to open for writing");
    }
}

///=====================================================================================================================
void rew::AudioSink::process(const float* data, const size_t length) {
    audioWriter->write(reinterpret_cast<const unsigned char*>(data), length * sizeof(float));
}

///=====================================================================================================================
void rew::AudioSink::close() {
    audioWriter->writeFooter();
    audioWriter->close();
}
