#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <Windows.h>
#include <fileapi.h>
#include <stdio.h>
#else
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#endif
#include <argagg/argagg.hpp>
#include <iostream>
#include <thread>
#include <future>
#include <rew/decoder/decoder.h>
#include <rew/common/directory_sink.h>
#include <rew/decoder/physical_audio_source.h>
#include <rew/decoder/tcp_http_server.h>

///=====================================================================================================================
std::string getFullPath(const std::string& path) {
#ifdef _WIN32
    LPSTR* lppPart = {nullptr};
    std::string buffer(4096, '\0');
    if (GetFullPathName(path.c_str(), buffer.size(), &buffer[0], lppPart) == 0) {
        throw std::runtime_error("Failed to get full path");
    }
#else
    std::string buffer(PATH_MAX, '\0');
    realpath(path.c_str(), &buffer[0]);
#endif
    return std::string(&buffer[0]);
}

///=====================================================================================================================
size_t getFileSize(const std::string& path) {
    struct stat info = {0};
    if (stat(path.c_str(), &info) != 0)
        return false;
    return static_cast<size_t>(info.st_size);
}

///=====================================================================================================================
bool folderIsValid(const std::string& path) {
    struct stat info = {0};
    if (stat(path.c_str(), &info) != 0)
        return false;
    else if (info.st_mode & S_IFDIR)
        return true;
    else
        return false;
}

///=====================================================================================================================
bool pathIsValid(const std::string& path) {
    struct stat info = {0};
    return stat(path.c_str(), &info) == 0;
}

///=====================================================================================================================
argagg::parser argparser{{{"help", {"-h", "--help"}, "Shows this help message", 0},
                          {"input", {"-i", "--input"}, "The input WAV file", 1},
                          {"listen", {"-l", "--listen"}, "Listen to a specific device", 0},
                          {"listen-devices", {"--listen-devices"}, "Print out all available listen devices", 0},
                          {"output", {"-o", "--output"}, "The output folder", 1},
                          {"serve", {"-s", "--serve"}, "Serve the files over http server", 0}}};

///=====================================================================================================================
int main(const int argc, char* argv[]) {
    static bool terminate = false;
#ifdef _WIN32
#ifndef _MSC_VER
    SetConsoleCtrlHandler(
        [](const DWORD fdwCtrlType) WINAPI -> BOOL {
#else
    SetConsoleCtrlHandler(
        [](const DWORD fdwCtrlType) -> BOOL {
#endif
            switch (fdwCtrlType) {
                case CTRL_C_EVENT:
                case CTRL_CLOSE_EVENT:
                case CTRL_BREAK_EVENT:
                case CTRL_LOGOFF_EVENT:
                case CTRL_SHUTDOWN_EVENT:
                    terminate = true;
                    return FALSE;
                default:
                    return FALSE;
            }
        },
        TRUE);
#else
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = [](int num) -> void {
        (void)num;
        terminate = true;
    };
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, NULL);
#endif

    try {
        const auto args = argparser.parse(argc, argv);
        std::shared_ptr<rew::Input<rew::NamedRawFile>> sink;
        std::shared_ptr<rew::TcpHttpServer> server;

        if (args["help"]) {
            std::cerr << argparser << std::endl;
            return EXIT_SUCCESS;
        }

        if (args["output"]) {
            const auto outputPath = getFullPath(args["output"].as<std::string>());

            if (!pathIsValid(outputPath))
                throw std::runtime_error("Output path: \"" + outputPath + "\" does not exist!");

            if (!folderIsValid(outputPath))
                throw std::runtime_error("Output path: \"" + outputPath + "\" is not a folder!");

            sink = std::make_shared<rew::DirectorySink>(outputPath);
        }

        else if (args["serve"]) {
            server = std::make_shared<rew::TcpHttpServer>("localhost", 80);
            sink = server;
            server->start();
        }

        else {
            throw std::runtime_error("You need to specify either --output or --serve!");
        }

        if (args["listen"]) {
            const auto audio = std::make_shared<rew::PhysicalAudioSource>();

            auto decoder = std::make_shared<rew::Decoder>(audio, sink, DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ,
                                                          DEFAULT_SAMPLE_LENGTH_MS);

            auto t = std::thread([=]() -> void {
                audio->start();
                audio->process();
            });

            while (!terminate) {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }

            audio->close();
            t.join();

            if (server)
                server->stop();
            return EXIT_SUCCESS;
        } else if (args["listen-devices"]) {
            const auto devices = rew::PhysicalAudioSource::getDevices();
            auto found = 0;
            for (const auto& device : devices) {
                if (device.inputChannels == 0)
                    continue;
                found++;
                std::cout << "Device: " << device.name << std::endl;
                std::cout << "\tApi:\t\t" << device.api << std::endl;
                std::cout << "\tChannels:\t" << device.inputChannels << std::endl;
                std::cout << "\tSample Rate:\t" << device.sampleRate << std::endl;
            }
            if (!found) {
                std::cerr << "No devices found" << std::endl;
            }

            return EXIT_SUCCESS;
        } else if (args["input"]) {
            const auto inputPath = args["input"].as<std::string>();

            if (!pathIsValid(inputPath))
                throw std::runtime_error("Input file: \"" + inputPath + "\" does not exist!");

            auto wav = std::make_shared<rew::WavReader>();
            auto source = std::make_shared<rew::AudioSource>(wav);

            auto decoder = std::make_shared<rew::Decoder>(source, sink, DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ,
                                                          DEFAULT_SAMPLE_LENGTH_MS);

            auto t = std::thread([=]() -> void {
                source->open(inputPath);
                source->process();
                source->close();
            });

            t.join();

            if (server)
                server->stop();
            return EXIT_SUCCESS;
        } else {
            throw std::runtime_error("You need to specify either --input or --listen");
        }
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
