#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <Windows.h>
#include <fileapi.h>
#else
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#endif
#include <argagg/argagg.hpp>
#include <iostream>
#include "folder.h"
#include <rew/encoder/encoder.h>
#include <rew/encoder/physical_audio_sink.h>

///=====================================================================================================================
argagg::parser argparser{{{"help", {"-h", "--help"}, "Shows this help message", 0},
                          {"input", {"-i", "--input"}, "The input folder", 1},
                          {"output", {"-o", "--output"}, "The output WAV file", 1},
                          {"stream", {"-s", "--stream"}, "Stream the audio directly to the audio output device", 0},
                          {"stream-devices", {"--stream-devices"}, "Print out all available stream devices", 0}}};

///=====================================================================================================================
std::string getFullPath(const std::string& path) {
#ifdef _WIN32
    LPSTR* lppPart = {nullptr};
    std::string buffer(4096, '\0');
    if (GetFullPathName(path.c_str(), buffer.size(), &buffer[0], lppPart) == 0) {
        throw std::runtime_error("Failed to get full path");
    }
#else
    std::string buffer(PATH_MAX, '\0');
    realpath(path.c_str(), &buffer[0]);
#endif
    return std::string(&buffer[0]);
}

///=====================================================================================================================
size_t getFileSize(const std::string& path) {
    struct stat info = {0};
    if (stat(path.c_str(), &info) != 0)
        return false;
    return static_cast<size_t>(info.st_size);
}

///=====================================================================================================================
bool folderIsValid(const std::string& path) {
    struct stat info = {0};
    if (stat(path.c_str(), &info) != 0)
        return false;
    else if (info.st_mode & S_IFDIR)
        return true;
    else
        return false;
}

///=====================================================================================================================
bool pathIsValid(const std::string& path) {
    struct stat info = {0};
    return stat(path.c_str(), &info) == 0;
}

///=====================================================================================================================
std::vector<std::string> recursivelyParseFolder(const std::string& path) {
    std::vector<std::string> files;

    // std::cout << "Entering:   " << path << std::endl;
    auto folder = rew::Folder(path);
    std::string file;

    while (folder.next(file)) {
        if (file == "." || file == "..")
            continue;

        const auto filePath = path + DELIMITER + file;

        if (folderIsValid(filePath)) {
            const auto b = recursivelyParseFolder(filePath);
            files.insert(std::end(files), std::begin(b), std::end(b));
        } else {
            // std::cout << "Collecting: " << filePath << std::endl;
            files.push_back(filePath);
        }
    }

    return files;
}

///=====================================================================================================================
class DirectoryTraverser : public rew::Output<rew::FileName> {
  public:
    DirectoryTraverser() = default;
    virtual ~DirectoryTraverser() = default;

    void process(const std::string& path) {
        // Collect all files and forward them down the pipeline
        const auto files = recursivelyParseFolder(path, path);
        this->forward(&files[0], files.size());
    }

  private:
    std::vector<rew::FileName> recursivelyParseFolder(const std::string& base, const std::string& path) {
        std::vector<rew::FileName> files;

        auto folder = rew::Folder(path);
        std::string file;

        while (folder.next(file)) {
            if (file == "." || file == "..")
                continue;

            const auto filePath = path + DELIMITER + file;

            if (folderIsValid(filePath)) {
                const auto b = recursivelyParseFolder(base, filePath);
                files.insert(std::end(files), std::begin(b), std::end(b));
            } else {
                const auto name = filePath.substr(base.size());
                files.push_back({filePath, name});
            }
        }

        return files;
    }
};

///=====================================================================================================================
int main(const int argc, char* argv[]) {
    try {
        const auto args = argparser.parse(argc, argv);

        if (args["help"]) {
            std::cerr << argparser << std::endl;
            return EXIT_SUCCESS;
        }

        if (args["stream"]) {
            if (!args["input"])
                throw std::runtime_error("Input argument is required");

            const auto inputPath = getFullPath(args["input"].as<std::string>());

            if (!pathIsValid(inputPath))
                throw std::runtime_error("Input path: \"" + inputPath + "\" does not exist!");

            if (!folderIsValid(inputPath))
                throw std::runtime_error("Input path: \"" + inputPath + "\" is not a folder!");

            // Create our source which will go through the directory
            auto source = std::make_shared<DirectoryTraverser>();

            // Create the output audio sink
            auto audioSink = std::make_shared<rew::PhysicalAudioSink>();

            // Finally combine everything together into a single encoder
            const auto fileLoader = std::make_shared<rew::FileLoader>();
            source->connect(fileLoader);
            auto encoder = rew::Encoder(fileLoader, audioSink, DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ,
                                        DEFAULT_SAMPLE_LENGTH_MS);

            // Open stream for writing
            audioSink->start();

            // Parse all files
            std::cout << "Reading directory: " << inputPath << std::endl;
            source->process(inputPath);

            // Wait for all buffers to be consumed
            audioSink->join();

            // Close the stream
            audioSink->close();
        } else if (args["stream-devices"]) {
            const auto devices = rew::PhysicalAudioSink::getDevices();
            auto found = 0;
            for (const auto& device : devices) {
                if (device.outputChannels == 0)
                    continue;
                found++;
                std::cout << "Device: " << device.name << std::endl;
                std::cout << "\tApi:\t\t" << device.api << std::endl;
                std::cout << "\tChannels:\t" << device.inputChannels << std::endl;
                std::cout << "\tSample Rate:\t" << device.sampleRate << std::endl;
            }
            if (!found) {
                std::cerr << "No devices found" << std::endl;
            }
            return EXIT_SUCCESS;

        } else if (args["output"]) {
            if (!args["input"])
                throw std::runtime_error("Input argument is required");

            const auto inputPath = getFullPath(args["input"].as<std::string>());
            const auto outputPath = args["output"].as<std::string>();

            if (!pathIsValid(inputPath))
                throw std::runtime_error("Input path: \"" + inputPath + "\" does not exist!");

            if (!folderIsValid(inputPath))
                throw std::runtime_error("Input path: \"" + inputPath + "\" is not a folder!");

            // Create our source which will go through the directory
            auto source = std::make_shared<DirectoryTraverser>();

            // Create the output wav file
            auto audioSink = std::make_shared<rew::AudioSink>(std::make_shared<rew::WavWriter>());

            // Finally combine everything together into a single encoder
            const auto fileLoader = std::make_shared<rew::FileLoader>();
            source->connect(fileLoader);
            auto encoder = rew::Encoder(fileLoader, audioSink, DEFAULT_LOW_TONE_FREQ, DEFAULT_HIGH_TONE_FREQ,
                                        DEFAULT_SAMPLE_LENGTH_MS);

            // Open file for writing
            std::cout << "Openning: " << outputPath << std::endl;
            audioSink->open(outputPath);

            // Parse all files
            std::cout << "Reading directory: " << inputPath << std::endl;
            source->process(inputPath);

            // Close the WAV file
            audioSink->close();
            std::cout << "Done!" << std::endl;

            return EXIT_SUCCESS;
        } else {
        }
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
