#ifndef REW_UTILS_FOLDER_H
#define REW_UTILS_FOLDER_H

#include <string>
#include "rew/config.h"

REW_NAMESPACE {
	class Folder {
	public:
		Folder(const std::string& path);
		virtual ~Folder();
		bool next(std::string& file);

	private:
		void* dir;
	};
};

#endif