#define ASIO_STANDALONE
#if defined(WIN32) && !defined(_WIN32_WINNT)
#define _WIN32_WINNT 0x0601
#endif
#include <asio.hpp>
#include <thread>
#include <rew/decoder/tcp_http_server.h>
#include <rew/decoder/tcp_http_connection.h>

///=====================================================================================================================
class rew::TcpHttpServer::Impl {
public:
    Impl(TcpHttpServer& server, const std::string& address, const int port)
        : server(server),
          acceptor(ioContext) {

        // Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
        asio::ip::tcp::resolver resolver(ioContext);
        asio::ip::tcp::endpoint endpoint = 
            *resolver.resolve(address, std::to_string(port)).begin();
        acceptor.open(endpoint.protocol());
        acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
        acceptor.bind(endpoint);
        acceptor.listen();

        doAccept();

        std::cout << "Starting http server at " << endpoint.address().to_string() << ":" << endpoint.port() << std::endl;
    }

    ~Impl() {
        stop();
    }

    void doAccept() {
        acceptor.async_accept([this](const std::error_code ec, asio::ip::tcp::socket socket){
            if (!acceptor.is_open()){
                return;
            }

            if (!ec) {
                std::shared_ptr<HttpConnection> connection 
                    = std::make_shared<TcpHttpConnection>(server, reinterpret_cast<void*>(&socket));
                server.accept(connection);
            } else {
                std::cerr << "Http server error: " << ec.message();
            }

            doAccept();
        });
    }

    void start() {
        thread = std::thread([this]() -> void {
            ioContext.run();
        });
    }

    void stop() {
        if (thread.joinable()) {
            ioContext.stop();
            thread.join();
        }
    }

    TcpHttpServer& server;
    asio::io_context ioContext;
    asio::ip::tcp::acceptor acceptor;
    std::thread thread;
};

///=====================================================================================================================
rew::TcpHttpServer::TcpHttpServer(const std::string& address, const int port)
    :pimpl(new Impl(*this, address, port)) {

}

///=====================================================================================================================
rew::TcpHttpServer::~TcpHttpServer() {

}

///=====================================================================================================================
void rew::TcpHttpServer::start() {
    pimpl->start();
}

///=====================================================================================================================
void rew::TcpHttpServer::stop() {
    pimpl->stop();
}

