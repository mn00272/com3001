#include <rew/decoder/frame_sync.h>
#include <rew/common/packet.h>

static const std::vector<uint8_t> signature = PACKET_SIGNATURE;

///=====================================================================================================================
rew::FrameSync::FrameSync(const size_t lowToneFreq, const size_t highToneFreq, const double sampleLengthMs) {
    const auto blockSize = static_cast<size_t>((48000.0 / 1000.0) * sampleLengthMs * 1.0);
    samplesPerBit = NUMBER_OF_SAMPLES_PER_BIT(48000.0, lowToneFreq, sampleLengthMs);

    samplesPerPacket = PACKET_REAL_SIZE * samplesPerBit * 8 + PACKET_REAL_SIZE * samplesPerBit;

    firFilterLow = std::make_shared<FirFilter>();
    firFilterHigh = std::make_shared<FirFilter>();
    // source->connect(firFilterLow);
    // source->connect(firFilterHigh);

    goertzelLow = std::make_shared<Goertzel>(blockSize, static_cast<float>(lowToneFreq), 48000.0f);
    goertzelHigh = std::make_shared<Goertzel>(blockSize, static_cast<float>(highToneFreq), 48000.0f);
    firFilterLow->connect(goertzelLow);
    firFilterHigh->connect(goertzelHigh);

    multiplyLow = std::make_shared<Multiply<float>>(1.0f / 300.0f);
    multiplyHigh = std::make_shared<Multiply<float>>(1.0f / 300.0f);
    goertzelLow->connect(multiplyLow);
    goertzelHigh->connect(multiplyHigh);

    clampLow = std::make_shared<Clamp<float, int>>(0.5f, 1, 0);
    clampHigh = std::make_shared<Clamp<float, int>>(0.5f, 1, 0);
    multiplyLow->connect(clampLow);
    multiplyHigh->connect(clampHigh);

    merger = std::make_shared<Merge<int, 2>>();
    clampLow->connect(merger->sources[0]);
    clampHigh->connect(merger->sources[1]);

    merger->connect(dynamic_cast<Input<MergedValue<int, 2>>*>(this));

    byteAssembler = std::make_shared<ByteAssembler>(lowToneFreq, 48000, sampleLengthMs);
    byteAssembler->connect(dynamic_cast<Input<ByteWithIndex>*>(this));

    forwardData = false;
    lastByteSamples = 0;
    totalForwarded = 0;
    samplesLeftoverIndex = 0;
}

///=====================================================================================================================
void rew::FrameSync::process(const float* data, size_t length) {
    if (!forwardData) {
        firFilterLow->process(data, length);
        firFilterHigh->process(data, length);

        if (samplesLeftoverIndex > 0) {
            if (samplesLeftoverIndex < length) {
                length -= samplesLeftoverIndex;
                data += samplesLeftoverIndex;
            } else {
                length = 0;
            }
            samplesLeftoverIndex = 0;
        }
    }

    if (forwardData) {
        if (length) {
            auto toForwardLength = length;
            if (totalForwarded + length > samplesPerPacket) {
                toForwardLength = samplesPerPacket - totalForwarded;
                forwardData = false;
                forward(data, toForwardLength);
                length -= toForwardLength;
                data += toForwardLength;

                if (length > 0) {
                    this->process(data, length);
                }
            } else {
                forward(data, length);
                totalForwarded += length;
            }
        }
    }
}

///=====================================================================================================================
void rew::FrameSync::process(const MergedValue<int, 2>* data, const size_t length) {
    byteAssembler->process(data, length);
    lastByteSamples += length;
}

///=====================================================================================================================
void rew::FrameSync::process(const ByteWithIndex* data, const size_t length) {
    syncBytes.push_back(SyncByte{lastByteSamples + data->index, data->byte});

    if (syncBytes.size() == signature.size()) {
        if (checkSequenceMatch() && checkTimingMatch()) {
            totalForwarded = 0;
            forwardData = true;
            lastByteSamples = 0;
            samplesLeftoverIndex = data->index;
        }
        syncBytes.clear();
    }
}

///=====================================================================================================================
bool rew::FrameSync::checkSequenceMatch() {
    for (size_t i = 0; i < signature.size(); i++) {
        if (syncBytes[i].byte != signature[i]) {
            return false;
        }
    }
    return true;
}

///=====================================================================================================================
bool rew::FrameSync::checkTimingMatch() {
    for (size_t i = 1; i < syncBytes.size(); i++) {
        const auto diff = syncBytes[i].timing - syncBytes[i - 1].timing;

        if (!(diff > samplesPerBit * 7 && diff < samplesPerBit * 10)) {
            return false;
        }
    }
    return true;
}