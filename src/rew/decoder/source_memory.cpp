#include <rew/decoder/source_memory.h>

///=====================================================================================================================
rew::SourceMemory::SourceMemory() {
}

///=====================================================================================================================
void rew::SourceMemory::consume(const float* data, const size_t length) {
    for (size_t i = 0; i < length; i += 32) {
        Output<float>::forward(&data[i], i + 32 > length ? length - i : 32);
    }
}
