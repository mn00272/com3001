#include <algorithm>
#include <fstream>
#include <rew/decoder/file_assembler.h>
#include <rew/common/crc8.h>

static const auto zlibEndOfStream = std::vector<uint8_t>{0xff, 0xff, 0x00, 0x00};

///=====================================================================================================================
rew::FileAssembler::FileAssembler(std::shared_ptr<Decompressor> decompressor) : decompressor(std::move(decompressor)) {
}

///=====================================================================================================================
void rew::FileAssembler::process(const Packet* data, const size_t length) {
    for (size_t i = 0; i < length; i++) {
        push(data[i]);
    }
}

///=====================================================================================================================
void rew::FileAssembler::push(const Packet& packet) {
    const auto fileId = static_cast<const int>(packet.fileIndex);
    auto& packets = cache[fileId];

    for (size_t i = 0; i < packets.size(); i++) {
        const auto& it = packets.begin() + i;

        if (packet.packetIndex == it->packetIndex) {
            *it = packet;
            break;
        }

        if (i == 0 && packet.packetIndex < it->packetIndex) {
            packets.insert(it, packet);
            break;
        }

        else if (i + 1 == packets.size()) {
            packets.push_back(packet);
            break;
        }

        else if (packet.packetIndex > it->packetIndex && packet.packetIndex < (packets.begin() + i + 1)->packetIndex) {
            packets.insert(it, packet);
            break;
        }
    }

    if (packets.empty()) {
        packets.push_back(packet);
    }

    const auto& data = packets.back().data;
    const auto it = std::search(data, data + sizeof(data), zlibEndOfStream.begin(), zlibEndOfStream.end());
    if (it != data + sizeof(data)) {

        if (packets.back().packetIndex + 1 == packets.size()) {
            std::vector<const uint8_t*> ptr;
            ptr.reserve(packets.size());
            for (const auto& p : packets) {
                ptr.push_back(p.data);
            }

            auto next = packets.begin();
            auto inflated = decompressor->fromChunks([&]() -> std::pair<const uint8_t*, size_t> {
                if (next == packets.end())
                    return std::make_pair<const uint8_t*, size_t>(nullptr, 0);
                else if (&(*next) == &packets.back()) {
                    auto ret = std::make_pair<const uint8_t*, size_t>(&next->data[0], it - &next->data[0]);
                    ++next;
                    return ret;
                } else {
                    auto ret = std::make_pair<const uint8_t*, size_t>(&next->data[0], PACKET_DATA_LENGTH);
                    ++next;
                    return ret;
                }
            });

            auto namedFile = NamedRawFile{};
            namedFile.data.reset(new uint8_t[inflated.size()]);
            std::memcpy(namedFile.data.get(), inflated.data(), inflated.size());
            namedFile.index = packet.fileIndex;
            namedFile.length = inflated.size();

            forward(&namedFile, 1);
        }
    }
}
