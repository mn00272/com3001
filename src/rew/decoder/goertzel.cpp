#include <cmath>
#include <rew/decoder/goertzel.h>
#include <iostream>

/*
 * The original author of this Goertzel algorithm is Kevin Banks
 * From https://www.embedded.com/design/configurable-systems/4024443/The-Goertzel-Algorithm
 * Minor modifications and converted to C++ by Matus Novak (mn00272)
 */

///=====================================================================================================================
rew::Goertzel::Goertzel(const size_t blockSize, const float targetFrequency, const float sampleRate)
    : coeff(0), Q1(0), Q2(0), sine(0), cosine(0), blockSize(blockSize) {
    init(blockSize, targetFrequency, sampleRate);
}

///=====================================================================================================================
void rew::Goertzel::process(const float* data, const size_t length) {
    if (forwardLength < length) {
        forwardLength = length;
        forwardBuffer.reset(new float[length]);
    }

    auto forwardDst = forwardBuffer.get();

    const auto dst = buffer.get();
    auto src = data;
    while (src != data + length) {
        // Move by one
        for (size_t i = 0; i < blockSize - 1; i++) {
            dst[i] = dst[i + 1];
        }
        dst[blockSize - 1] = *src++;

        const auto mag = this->operator()(dst);
        // Output<float>::forward(&mag, 1);

        *forwardDst++ = mag;
    }

    if (length) {
        Output<float>::forward(forwardBuffer.get(), length);
    }
}

///=====================================================================================================================
float rew::Goertzel::operator()(const float* samples) {
    // for (size_t i = 0; i < blockSize; i++) processSample(100.0f * samples[i] + 100.0f);
    processSamples(samples, blockSize);
    const auto mag = getMagnitude();
    reset();
    return mag;
}

///=====================================================================================================================
void rew::Goertzel::init(const size_t blockSize, const float targetFrequency, const float sampleRate) {
    const auto floatN = static_cast<float>(blockSize);
    const auto k = static_cast<int>(0.5 + ((floatN * targetFrequency) / sampleRate));
    const auto omega = (2.0 * M_PI * k) / floatN;
    sine = static_cast<float>(std::sin(omega));
    cosine = static_cast<float>(std::cos(omega));
    coeff = static_cast<float>(2.0 * cosine);
    reset();

    buffer.reset(new float[blockSize]);
    std::fill(buffer.get(), buffer.get() + blockSize, 0.0f);

    forwardBuffer.reset(new float[128]);
    forwardLength = 128;
}

///=====================================================================================================================
void rew::Goertzel::reset() {
    // Call this routine before every "block" (size=N) of samples.
    Q2 = 0;
    Q1 = 0;
}

///=====================================================================================================================
void rew::Goertzel::processSamples(const float* samples, const size_t length) {
    auto src = samples;
    while (src != samples + length) {
        const auto Q0 = coeff * Q1 - Q2 + *src++;
        Q2 = Q1;
        Q1 = Q0;
    }
}

///=====================================================================================================================
void rew::Goertzel::processSample(const float sample) {
    // Call this routine for every sample.
    const auto Q0 = coeff * Q1 - Q2 + sample;
    Q2 = Q1;
    Q1 = Q0;
}

///=====================================================================================================================
float rew::Goertzel::getMagnitude() const {
    // Call this after every block to get the RELATIVE magnitude squared.
    return Q1 * Q1 + Q2 * Q2 - Q1 * Q2 * coeff;
}
