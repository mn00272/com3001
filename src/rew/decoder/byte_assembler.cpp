#include <rew/decoder/byte_assembler.h>

///=====================================================================================================================
rew::ByteAssembler::ByteAssembler(const size_t frequency, const size_t sampleRate, const double ms) {
    samplesPerBit = NUMBER_OF_SAMPLES_PER_BIT(sampleRate, frequency, ms);

    buffer.resize(samplesPerBit * 8);
    begin = buffer.data();
    end = begin + buffer.size();
    dst = begin;

    checkLength = samplesPerBit / 2;
    found = false;
}

///=====================================================================================================================
void rew::ByteAssembler::swap(ByteAssembler& other) noexcept {
    using std::swap;
    swap(buffer, other.buffer);
    swap(begin, other.begin);
    swap(end, other.end);
    swap(dst, other.dst);
    swap(checkLength, other.checkLength);
    swap(found, other.found);
}

///=====================================================================================================================
rew::ByteAssembler::ByteAssembler(ByteAssembler&& other) noexcept {
    swap(other);
}

///=====================================================================================================================
rew::ByteAssembler& rew::ByteAssembler::operator=(ByteAssembler&& other) noexcept {
    if (this != &other) {
        swap(other);
    }
    return *this;
}

///=====================================================================================================================
void rew::ByteAssembler::process(const MergedValue<int, 2>* data, const size_t length) {
    for (size_t i = 0; i < length; i++) {
        uint8_t bit;
        if (data[i].values[0] == 1)
            bit = 1;
        else if (data[i].values[1] == 1)
            bit = 2;
        else
            bit = 0;

        if (!found && bit > 0) {

            if (dst + 1 >= end) {
                dst = begin;
            }

            *dst++ = bit;

            if (size_t(dst - begin) > checkLength) {
                const auto c = count(dst - checkLength, dst, 0x00);
                if (c < checkLength) {
                    found = true;
                } else {
                    dst = begin;
                    found = false;
                }
            }
        } else {
            if (found) {
                *dst++ = bit;

                if (dst + 1 >= end) {
                    // Got all bits!
                    const auto byte = toByte(begin, end);
                    const auto dat = ByteWithIndex{byte, i};
                    forward(&dat, 1);

                    dst = begin;
                    found = false;
                }
            } else {
                dst = begin;
            }
        }
    }
}

///=====================================================================================================================
size_t rew::ByteAssembler::count(const uint8_t* start, const uint8_t* end, const uint8_t target) {
    size_t out = 0;
    while (start != end) {
        if (*start++ == target)
            out++;
    }
    return out;
}

///=====================================================================================================================
uint8_t rew::ByteAssembler::toByte(const uint8_t* begin, const uint8_t* end) {
    uint8_t ret = 0;
    const auto eight = (end - begin) / 8.0;
    for (size_t i = 0; i < 8; i++) {
        const auto countLow = count(begin + size_t(eight * i), begin + size_t(eight * (i + 1)), 1);
        const auto countHigh = count(begin + size_t(eight * i), begin + size_t(eight * (i + 1)), 2);
        if (countHigh > countLow) {
            ret |= 1 << i;
        }
    }
    return ret;
}