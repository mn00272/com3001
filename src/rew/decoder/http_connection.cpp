#include <sstream>
#include <rew/decoder/http_connection.h>
#include <rew/decoder/http_server.h>
#include <rew/common/tokenizer.h>

// These codes and names are extracted from:
// https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html
static const std::unordered_map<int, std::string> statusCodes = {
    {100, "Continue"},
    {101, "Switching Protocols"},
    {200, "OK"},
    {201, "Created"},
    {202, "Accepted"},
    {203, "Non-Authoritative Information"},
    {204, "No Content"},
    {205, "Reset Content"},
    {206, "Partial Content"},
    {300, "Multiple Choices"},
    {301, "Moved Permanently"},
    {302, "Found"},
    {303, "See Other"},
    {304, "Not Modified"},
    {305, "Use Proxy"},
    {307, "Temporary Redirect"},
    {400, "Bad Request"},
    {401, "Unauthorized"},
    {402, "Payment Required"},
    {403, "Forbidden"},
    {404, "Not Found"},
    {405, "Method Not Allowed"},
    {406, "Not Acceptable"},
    {407, "Proxy Authentication Required"},
    {408, "Request Time-out"},
    {409, "Conflict"},
    {410, "Gone"},
    {411, "Length Required"},
    {412, "Precondition Failed"},
    {413, "Request Entity Too Large"},
    {414, "Request-URI Too Large"},
    {415, "Unsupported Media Type"},
    {416, "Requested range not satisfiable"},
    {417, "Expectation Failed"},
    {500, "Internal Server Error"},
    {501, "Not Implemented"},
    {502, "Bad Gateway"},
    {503, "Service Unavailable"},
    {504, "Gateway Time-out"},
    {505, "HTTP Version not supported"},
};

///=====================================================================================================================
rew::HttpConnection::HttpConnection(HttpServer& server) : server(server) {
}

///=====================================================================================================================
void rew::HttpConnection::send(const HttpRequest& req, const HttpResponse& res) {
    std::stringstream ss;
    ss << "HTTP/1.1 " << res.status << " " << statusCodes.at(res.status) << "\n";
    ss << "Server: Decoder\n";
    ss << "Accept-Ranges: bytes\n";
    ss << "Content-Length: " << std::to_string(res.body.size()) << "\n";
    ss << "Connection: close\n";
    ss << "Content-Type: " << res.contentType << "\n";
    ss << "\n";
    ss << res.body;
    sendRaw(std::make_shared<std::string>(ss.str()));
    std::cout << req.address << " \"" << req.method << " " << req.path << " " << req.type << "\" " << res.status << " "
              << res.body.size() << std::endl;
}

///=====================================================================================================================
void rew::HttpConnection::receive(const std::string& address, const std::string& payload) {
    HttpRequest req;
    try {
        req = getRequest(address, payload);
    } catch (std::exception& e) {
        send(HttpRequest{"", "", "", address}, HttpResponse{"text/plain", 400, "Bad request"});
        return;
    }

    try {
        if (req.method != "GET") {
            send(req, HttpResponse{"text/plain", 405, "Method not allowed"});
        } else {
            const auto res = server.serve(req);
            send(req, res);
        }
    } catch (std::exception& e) {
        send(req, HttpResponse{"text/plain", 500, "Internal server error"});
    }
}

///=====================================================================================================================
rew::HttpRequest rew::HttpConnection::getRequest(const std::string& address, const std::string& payload) const {
    const auto tokens = getTokens(payload, "\r\n");
    if (tokens.size() < 1) {
        throw std::runtime_error("Invalid http request");
    } else {
        const auto words = getTokens(tokens[0], " ");
        if (words.size() != 3) {
            throw std::runtime_error("Invalid http request");
        } else {
            return HttpRequest{words[0], words[1], words[2], address};
        }
    }
}
