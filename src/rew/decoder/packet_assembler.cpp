#include <rew/decoder/packet_assembler.h>
#include <rew/common/crc8.h>

///=====================================================================================================================
void rew::PacketAssembler::process(const ByteWithIndex* data, const size_t length) {
    for (size_t i = 0; i < length; i++) {
        buffer.push_back(data[i].byte);
        if (buffer.size() >= PACKET_REAL_SIZE) {
            const Packet& packet = *reinterpret_cast<const Packet*>(buffer.data());

            /*std::cout << "packet: " << std::endl;
            for (size_t i = 0; i < buffer.size(); i++) {
                std::cout << int(buffer[i]) << ", ";
            }
            std::cout << std::endl;*/

            // Verify checksum
            const auto crcLength = PACKET_REAL_SIZE - sizeof(packet.crc);
            const auto crc = crc8(reinterpret_cast<const uint8_t*>(&packet.fileIndex), crcLength);
            if (crc != packet.crc) {
                std::cout << "Bad CRC discarding packet" << std::endl;
                std::cout << "Packet crc: " << int(packet.crc) << " our crc: " << int(crc) << std::endl;
            } else {
                // std::cout << "accepting packet" << std::endl;
                forward(&packet, 1);
            }
            buffer.clear();
        }
    }
}
