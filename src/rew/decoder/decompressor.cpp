#include <rew/decoder/decompressor.h>
#include <iostream>
#include <zlib.h>
#include <algorithm>

///=====================================================================================================================
rew::Decompressor::Decompressor() : stream(new z_stream) {
    stream->zalloc = Z_NULL;
    stream->zfree = Z_NULL;
    stream->opaque = Z_NULL;
}

///=====================================================================================================================
rew::Decompressor::~Decompressor() {
}

///=====================================================================================================================
rew::Decompressor::Decompressor(Decompressor&& other) noexcept {
    swap(other);
}

///=====================================================================================================================
void rew::Decompressor::swap(Decompressor& other) noexcept {
    using std::swap;
    swap(stream, other.stream);
}

///=====================================================================================================================
rew::Decompressor& rew::Decompressor::operator=(Decompressor&& other) noexcept {
    if (this != &other) {
        swap(other);
    }
    return *this;
}

///=====================================================================================================================
std::vector<uint8_t>
rew::Decompressor::fromChunks(const std::function<std::pair<const uint8_t*, size_t>()>& callback) const {
    std::vector<uint8_t> ret;
    ret.resize(32);

    inflateInit(stream.get());

    try {
        stream->avail_out = ret.size();                       // size of output
        stream->next_out = reinterpret_cast<Bytef*>(&ret[0]); // output char array

        while (true) {
            const auto next = callback();
            if (next.first == nullptr || next.second == 0)
                break;

            stream->avail_in = next.second;                                               // size of input
            stream->next_in = reinterpret_cast<Bytef*>(const_cast<uint8_t*>(next.first)); // input char array

            while (stream->avail_in != 0) {
                const auto status = inflate(stream.get(), Z_SYNC_FLUSH);
                if (status < 0) {
                    throw std::runtime_error(std::string(stream->msg ? stream->msg : "unknown error"));
                }

                if (stream->avail_out == 0) {
                    const auto oldSize = ret.size();
                    ret.resize(oldSize + 32);
                    stream->avail_out = 32;                                     // size of output
                    stream->next_out = reinterpret_cast<Bytef*>(&ret[oldSize]); // output char array
                }
            }
        }

        if (stream->total_out < ret.size()) {
            ret.resize(stream->total_out);
        }

        inflateEnd(stream.get());
        return ret;
    } catch (std::exception_ptr& eptr) {
        inflateEnd(stream.get());
        std::rethrow_exception(eptr);
    }
}

///=====================================================================================================================
std::vector<uint8_t> rew::Decompressor::fromChunks(const std::vector<Chunk>& chunks) const {
    auto it = chunks.begin();
    return fromChunks([&]() {
        if (it == chunks.end())
            return std::make_pair<const uint8_t*, size_t>(nullptr, 0L);
        auto pair = std::make_pair(it->data(), it->size());
        ++it;
        return pair;
    });

    /*std::vector<uint8_t> ret;
    ret.resize(32);

    inflateInit(stream.get());

    try {
        stream->avail_out = ret.size(); // size of output
        stream->next_out = reinterpret_cast<Bytef*>(&ret[0]); // output char array

        for (const auto& chunk : chunks) {
            stream->avail_in = chunk.size(); // size of input
            stream->next_in = reinterpret_cast<Bytef*>(const_cast<uint8_t*>(&chunk[0])); // input char array

            while (stream->avail_in != 0) {
                const auto status = inflate(stream.get(), Z_SYNC_FLUSH);
                if (status < 0) {
                    throw std::runtime_error(std::string(stream->msg ? stream->msg : "unknown error"));
                }

                if (stream->avail_out == 0) {
                    const auto oldSize = ret.size();
                    ret.resize(oldSize + 32);
                    stream->avail_out = 32; // size of output
                    stream->next_out = reinterpret_cast<Bytef*>(&ret[oldSize]); // output char array
                }
            }
        }

        if (stream->total_out < ret.size()) {
            ret.resize(stream->total_out);
        }

        inflateEnd(stream.get());
        return ret;
    } catch (std::exception_ptr& eptr) {
        inflateEnd(stream.get());
        std::rethrow_exception(eptr);
    }*/
}
