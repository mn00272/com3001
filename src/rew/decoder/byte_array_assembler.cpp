#include <rew/decoder/byte_array_assembler.h>
#include <rew/common/packet.h>

///=====================================================================================================================
rew::ByteArrayAssembler::ByteArrayAssembler(const size_t frequency, const size_t sampleRate, const double ms) {
    samplesPerBit = NUMBER_OF_SAMPLES_PER_BIT(sampleRate, frequency, ms);
    samplesPerPacket = PACKET_REAL_SIZE * samplesPerBit * 8 + PACKET_REAL_SIZE * samplesPerBit;
    filled = 0;
    buffer.reserve(samplesPerPacket);
}

///=====================================================================================================================
void rew::ByteArrayAssembler::swap(ByteArrayAssembler& other) noexcept {
    using std::swap;
    swap(buffer, other.buffer);
    swap(samplesPerBit, other.samplesPerBit);
    swap(samplesPerPacket, other.samplesPerPacket);
}

///=====================================================================================================================
rew::ByteArrayAssembler::ByteArrayAssembler(ByteArrayAssembler&& other) noexcept {
    swap(other);
}

///=====================================================================================================================
rew::ByteArrayAssembler& rew::ByteArrayAssembler::operator=(ByteArrayAssembler&& other) noexcept {
    if (this != &other) {
        swap(other);
    }
    return *this;
}

///=====================================================================================================================
void rew::ByteArrayAssembler::process(const MergedValue<int, 2>* data, const size_t length) {
    if (filled <= buffer.size()) {
        for (size_t i = 0; i < length; i++) {
            uint8_t bit;
            if (data[i].values[1] == 1)
                bit = 2;
            else if (data[i].values[0] == 1)
                bit = 1;
            else
                bit = 0;
            buffer.push_back(bit);

            if (buffer.size() == buffer.capacity()) {
                const auto samplesPerByte = samplesPerPacket / PACKET_REAL_SIZE;
                const auto temp = std::unique_ptr<uint8_t[]>(new uint8_t[PACKET_REAL_SIZE]);

                for (size_t b = 0; b < PACKET_REAL_SIZE; b++) {
                    const auto begin = buffer.data() + b * samplesPerByte;
                    const auto end = buffer.data() + (b + 1) * samplesPerByte - samplesPerBit;
                    temp.get()[b] = toByte(begin, end);
                }
                forward(temp.get(), PACKET_REAL_SIZE);

                buffer.clear();
                buffer.reserve(samplesPerPacket);
            }
        }
    }
}

///=====================================================================================================================
size_t rew::ByteArrayAssembler::count(const uint8_t* start, const uint8_t* end, const uint8_t target) {
    size_t out = 0;
    while (start != end) {
        if (*start++ == target)
            out++;
    }
    return out;
}

///=====================================================================================================================
uint8_t rew::ByteArrayAssembler::toByte(const uint8_t* begin, const uint8_t* end) {
    uint8_t ret = 0;
    const auto eight = (end - begin) / 8.0;
    for (size_t i = 0; i < 8; i++) {
        const auto countLow = count(begin + size_t(eight * i), begin + size_t(eight * (i + 1)), 1);
        const auto countHigh = count(begin + size_t(eight * i), begin + size_t(eight * (i + 1)), 2);
        if (countHigh > countLow) {
            ret |= 1 << i;
        }
    }
    return ret;
}
