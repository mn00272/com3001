#define ASIO_STANDALONE
#if defined(WIN32) && !defined(_WIN32_WINNT)
#define _WIN32_WINNT 0x0601
#endif
#include <asio.hpp>
#include <sstream>
#include <rew/decoder/tcp_http_connection.h>
#include <rew/decoder/tcp_http_server.h>

///=====================================================================================================================
class rew::TcpHttpConnection::Impl {
  public:
    Impl(TcpHttpServer& server, asio::ip::tcp::socket socket) : server(server), socket(std::move(socket)) {
    }

    TcpHttpServer& server;
    asio::ip::tcp::socket socket;
    std::array<char, 8192> buffer;
};

///=====================================================================================================================
rew::TcpHttpConnection::TcpHttpConnection(TcpHttpServer& server, void* socket)
    : HttpConnection(server), pimpl(new Impl(server, std::move(*reinterpret_cast<asio::ip::tcp::socket*>(socket)))) {
}

///=====================================================================================================================
rew::TcpHttpConnection::~TcpHttpConnection() {
    stop();
}

///=====================================================================================================================
void rew::TcpHttpConnection::start() {
    doRead();
}

///=====================================================================================================================
void rew::TcpHttpConnection::stop() {
    pimpl->socket.close();
}

///=====================================================================================================================
void rew::TcpHttpConnection::doRead() {
    auto self(shared_from_this());
    pimpl->socket.async_read_some(
        asio::buffer(pimpl->buffer), [this, self](const std::error_code ec, const size_t length) {
            if (!ec) {
                const auto payload = std::string(reinterpret_cast<const char*>(pimpl->buffer.data()), length);
                self->receive(self->pimpl->socket.remote_endpoint().address().to_string(), payload);
            } else if (ec != asio::error::operation_aborted) {
                pimpl->server.disconnect(shared_from_this());
            }
        });
}

///=====================================================================================================================
void rew::TcpHttpConnection::sendRaw(std::shared_ptr<std::string> payload) {
    auto self(shared_from_this());
    auto buffer = asio::buffer(payload->data(), payload->size());
    asio::async_write(pimpl->socket, buffer, [this, self, payload](const std::error_code ec, const size_t) {
        (void)payload; // Extend the life of the payload

        if (!ec) {
            // Initiate graceful connection closure.
            asio::error_code ignored_ec;
            pimpl->socket.shutdown(asio::ip::tcp::socket::shutdown_both, ignored_ec);
        }

        if (ec != asio::error::operation_aborted) {
            pimpl->server.disconnect(shared_from_this());
        }
    });
}
