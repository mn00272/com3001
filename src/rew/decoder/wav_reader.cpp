#include <rew/decoder/wav_reader.h>

///=============================================================================
rew::WavReader::WavReader()
    : subChunk2Size(0),
      readTotal(0),
      dataOffset(0) {
}

///=============================================================================
rew::WavReader::~WavReader() {
    if (loaded) input.close();
}

///=============================================================================
bool rew::WavReader::open(const std::string& path) {
    if (loaded) return false;

    input.open(path.c_str(), std::ios::in | std::ios::binary);

    if (!input) {
        return false;
    }

    // Check if file is RIFF
    char chunkID[4];
    input.read(&chunkID[0], sizeof(char));
    input.read(&chunkID[1], sizeof(char));
    input.read(&chunkID[2], sizeof(char));
    input.read(&chunkID[3], sizeof(char));
    if (!(chunkID[0] == 'R' && chunkID[1] == 'I' && chunkID[2] == 'F' && chunkID[3] == 'F')) {
        input.close();
        return false;
    }

    // Get chunk size
    unsigned int chunk;
    input.read(reinterpret_cast<char*>(&chunk), sizeof(unsigned int));

    // Check if file is WAVE
    char format[4];
    input.read(&format[0], sizeof(char));
    input.read(&format[1], sizeof(char));
    input.read(&format[2], sizeof(char));
    input.read(&format[3], sizeof(char));
    if (!(format[0] == 'W' && format[1] == 'A' && format[2] == 'V' && format[3] == 'E')) {
        input.close();
        return false;
    }

    // Find FMT sub-chunk
    bool foundFMT = false;
    while (!foundFMT && !input.eof()) {
        char subchunk1ID[4];
        input.read(reinterpret_cast<char*>(&subchunk1ID[0]), sizeof(char));
        if (subchunk1ID[0] == 'f') {
            input.read(&subchunk1ID[1], sizeof(char));
            input.read(&subchunk1ID[2], sizeof(char));
            input.read(&subchunk1ID[3], sizeof(char));
            if (subchunk1ID[1] == 'm' && subchunk1ID[2] == 't' && subchunk1ID[3] == ' ')foundFMT = true;
        }
    }

    if (!foundFMT) {
        input.close();
        return false;
    }

    // Get sub chunk data
    unsigned int subChunk1Size = 0;
    unsigned short audioFormat = 0;
    unsigned short numChannels = 0;
    unsigned int sampleR = 0;
    unsigned int byteR = 0;
    unsigned short blockAlign = 0;
    unsigned short bitsSample = 0;

    input.read(reinterpret_cast<char*>(&subChunk1Size), sizeof(unsigned int));
    input.read(reinterpret_cast<char*>(&audioFormat), sizeof(unsigned short));
    input.read(reinterpret_cast<char*>(&numChannels), sizeof(unsigned short));
    input.read(reinterpret_cast<char*>(&sampleR), sizeof(unsigned int));
    input.read(reinterpret_cast<char*>(&byteR), sizeof(unsigned int));
    input.read(reinterpret_cast<char*>(&blockAlign), sizeof(unsigned short));
    input.read(reinterpret_cast<char*>(&bitsSample), sizeof(unsigned short));

    // Find data sub-chunk
    bool foundData = false;
    while (!foundData && !input.eof()) {
        char subchunk1ID[4];
        input.read(&subchunk1ID[0], sizeof(char));
        if (subchunk1ID[0] == 'd') {
            input.read(&subchunk1ID[1], sizeof(char));
            input.read(&subchunk1ID[2], sizeof(char));
            input.read(&subchunk1ID[3], sizeof(char));
            if (subchunk1ID[1] == 'a' && subchunk1ID[2] == 't' && subchunk1ID[3] == 'a')foundData = true;
        }
    }

    if (!foundData) {
        input.close();
        return false;
    }

    // Get size of data
    input.read(reinterpret_cast<char*>(&subChunk2Size), sizeof(unsigned int));
    readTotal = 0;

    dataOffset = input.tellg();

    size = subChunk2Size;
    bitsPerSample = bitsSample;
    sampleRate = sampleR;
    channelCount = numChannels;
    chunkSize = 4096 * numChannels * (bitsSample / 8);
    loaded = true;
    return true;
}

///=============================================================================
void rew::WavReader::close() {
    if (!loaded) return;
    input.close();
    loaded = false;
}

///=============================================================================
bool rew::WavReader::read(unsigned char* data, size_t* length) {
    if (!loaded)return false;
    if (readTotal >= subChunk2Size)return false;

    int toRead;
    if (readTotal + chunkSize > subChunk2Size) toRead = subChunk2Size - readTotal;
    else toRead = chunkSize;

    input.read(reinterpret_cast<char*>(data), toRead);
    const auto read = static_cast<size_t>(input.gcount());

    readTotal += read;
    *length = read;
    return read == toRead;
}

///=============================================================================
bool rew::WavReader::eof() const {
    if (!loaded) return true;
    return readTotal >= size;
}
