#include <rew/decoder/physical_audio_source.h>
#include <portaudio.h>

class rew::AudioInitializer {
  public:
    AudioInitializer() {
        const auto err = Pa_Initialize();
        if (err != paNoError) {
            throw std::runtime_error(Pa_GetErrorText(err));
        }
    }

    ~AudioInitializer() {
        const auto err = Pa_Terminate();
        if (err != paNoError) {
            std::cerr << Pa_GetErrorText(err) << std::endl;
        }
    }

    static std::shared_ptr<AudioInitializer> instance() {
        static const auto ptr = std::make_shared<AudioInitializer>();
        return ptr;
    }
};

rew::PhysicalAudioSource::PhysicalAudioSource(AudioDevice* device)
    : stream(nullptr), started(false), initializer(AudioInitializer::instance()) {
    PaStreamParameters params;

    const auto lambda = [](const void* input, void* output, const unsigned long frameCount,
                           const PaStreamCallbackTimeInfo* timeInfo, const PaStreamCallbackFlags statusFlags,
                           void* userData) -> int {
        (void)output;
        (void)timeInfo;
        (void)statusFlags;

        auto ptr = reinterpret_cast<PhysicalAudioSource*>(userData);
        ptr->callback(reinterpret_cast<const float*>(input), frameCount);
        return 0;
    };
    const auto err = Pa_OpenDefaultStream(&stream, 1, 0, paFloat32, 48000, BUFFER_SIZE, lambda, this);
    if (err != paNoError) {
        throw std::runtime_error(Pa_GetErrorText(err));
    }
}

rew::PhysicalAudioSource::~PhysicalAudioSource() {
    // void
    // This must be empty and non-default
}

void rew::PhysicalAudioSource::callback(const float* input, const unsigned long frameCount) {
    std::array<float, 256> buffer{};
    std::memcpy(buffer.data(), input, buffer.size() * sizeof(float));

    {
        std::lock_guard<std::mutex> lk(lock);
        chunks.emplace();
        std::swap(buffer, chunks.back());
    }
    cv.notify_one();
}

void rew::PhysicalAudioSource::start() {
    const auto err = Pa_StartStream(stream);
    if (err != paNoError) {
        throw std::runtime_error(Pa_GetErrorText(err));
    }
    started = true;
}

void rew::PhysicalAudioSource::close() {
    if (stream && started) {
        started = false;
        Pa_StopStream(stream);
    }
    if (stream) {
        Pa_CloseStream(stream);
    }
}

void rew::PhysicalAudioSource::process() {
    while (started) {
        std::array<float, BUFFER_SIZE> buffer{};

        std::unique_lock<std::mutex> lk(lock);
        if (cv.wait_for(lk, std::chrono::milliseconds(100), [this] { return !chunks.empty(); })) {
            std::swap(buffer, chunks.front());
            chunks.pop();
            lk.unlock();

            for (size_t i = 0; i < buffer.size(); i += 32) {
                Output<float>::forward(buffer.data() + i, 32);
            }
        }
    }
}

std::vector<rew::AudioDevice> rew::PhysicalAudioSource::getDevices() {
    const auto initializer = AudioInitializer::instance();
    (void)initializer;

    std::vector<AudioDevice> devices;

    const auto num = Pa_GetDeviceCount();
    for (auto i = 0; i < num; i++) {
        const auto info = Pa_GetDeviceInfo(i);
        const auto api = Pa_GetHostApiInfo(info->hostApi);

        devices.emplace_back(AudioDevice{i, info->name, api->name, info->maxInputChannels, info->maxOutputChannels,
                                         info->defaultSampleRate, info->defaultLowInputLatency,
                                         info->defaultLowOutputLatency, info->defaultHighInputLatency,
                                         info->defaultHighOutputLatency});
    }

    return devices;
}
