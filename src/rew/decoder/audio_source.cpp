#include "../include/rew/decoder/audio_source.h"

///=====================================================================================================================
rew::AudioSource::AudioSource(std::shared_ptr<AudioReader> audioReader) : audioReader(std::move(audioReader)) {
}

///=====================================================================================================================
rew::AudioSource::~AudioSource() {
    if (audioReader && audioReader->isOpen()) {
        audioReader->close();
    }
}

///=====================================================================================================================
void rew::AudioSource::open(const std::string& path) {
    if (!audioReader->open(path))
        throw std::runtime_error("output audio file failed to open for writing");

    if (audioReader->getBitsPerSample() != 32)
        throw std::runtime_error("audio file must be float32");

    if (audioReader->getNumOfChannels() != 1)
        throw std::runtime_error("audio file must be mono only");

    if (audioReader->getSampleRate() != 48000)
        throw std::runtime_error("audio file must be 48.0 KHz");
}

///=====================================================================================================================
void rew::AudioSource::process() {
    auto samples = std::make_unique<float[]>(audioReader->getChunkSize() / sizeof(float));
    while (!audioReader->eof()) {
        size_t length;
        if (!audioReader->read(reinterpret_cast<unsigned char*>(samples.get()), &length)) {
            throw std::runtime_error("error reading audio file");
        }

        if (length % 4 != 0) {
            throw std::runtime_error("the number of bytes read must be multiple of 4");
        }

        const auto total = length / sizeof(float);
        const auto ptr = reinterpret_cast<const float*>(samples.get());
        for (size_t i = 0; i < total; i += 32) {
            this->forward(ptr + i, 32);
        }
    }
}

///=====================================================================================================================
void rew::AudioSource::close() {
    audioReader->close();
}
