#include <rew/decoder/decoder.h>
#include <algorithm>
#include <iostream>
#include <iomanip>

///=====================================================================================================================
rew::Decoder::Decoder(const std::shared_ptr<Output<float>>& source, const std::shared_ptr<Input<NamedRawFile>>& sink,
                      const size_t lowToneFreq, const size_t highToneFreq, const double sampleLengthMs) {
    const auto blockSize = static_cast<size_t>((48000.0 / 1000.0) * sampleLengthMs * 1.0);

    frameSync = std::make_shared<FrameSync>(lowToneFreq, highToneFreq, sampleLengthMs);
    source->connect(frameSync);

    firFilterLow = std::make_shared<FirFilter>();
    firFilterHigh = std::make_shared<FirFilter>();
    frameSync->connect(firFilterLow);
    frameSync->connect(firFilterHigh);

    goertzelLow = std::make_shared<Goertzel>(blockSize, static_cast<float>(lowToneFreq), 48000.0f);
    goertzelHigh = std::make_shared<Goertzel>(blockSize, static_cast<float>(highToneFreq), 48000.0f);
    firFilterLow->connect(goertzelLow);
    firFilterHigh->connect(goertzelHigh);

    multiplyLow = std::make_shared<Multiply<float>>(1.0f / 200.0f);
    multiplyHigh = std::make_shared<Multiply<float>>(1.0f / 200.0f);
    goertzelLow->connect(multiplyLow);
    goertzelHigh->connect(multiplyHigh);

    clampLow = std::make_shared<Clamp<float, int>>(0.5f, 1, 0);
    clampHigh = std::make_shared<Clamp<float, int>>(0.5f, 1, 0);
    multiplyLow->connect(clampLow);
    multiplyHigh->connect(clampHigh);

    merger = std::make_shared<Merge<int, 2>>();
    clampLow->connect(merger->sources[0]);
    clampHigh->connect(merger->sources[1]);

    byteAssembler = std::make_shared<ByteAssembler>(lowToneFreq, 48000, sampleLengthMs);
    merger->connect(byteAssembler);

    packetAssembler = std::make_shared<PacketAssembler>();
    byteAssembler->connect(packetAssembler);

    decompressor = std::make_shared<Decompressor>();
    fileAssembler = std::make_shared<FileAssembler>(decompressor);
    packetAssembler->connect(fileAssembler);

    fileAssembler->connect(sink);
}
