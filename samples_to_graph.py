import matplotlib.pyplot as plt
from scipy.io import wavfile
import numpy as np
from os import path


def wav_to_plot(file):
    samplerate, data = wavfile.read(file)
    times = np.arange(len(data)) / float(samplerate)

    plt.figure(figsize=(30, 6))
    plt.fill_between(times, data)
    plt.xlim(times[0], times[-1])
    plt.xlabel('time (s)')
    plt.ylabel('amplitude')
    plt.savefig(file + '.png', dpi=100)


def bin_to_plot(outpath, files, dtype):
    plt.figure(figsize=(60, 5))
    for file in files:
        with open(file, "rb") as fb:
            data = np.fromfile(fb, dtype=dtype)
            data = data[0:40000]
            plt.plot(data)
    plt.xlabel('samples')
    plt.ylabel('magnitude')
    plt.savefig(outpath, dpi=300)


def bin_to_plot_f32(outpath, files):
    bin_to_plot(outpath, files, np.float32)


def bin_to_plot_i32(outpath, files):
    bin_to_plot(outpath, files, np.int32)


def bin_to_plot_i8(outpath, files):
    bin_to_plot(outpath, files, np.int8)


bin_to_plot_f32('build/tests/decode_goertzel_samples_raw.png',
                ['build/tests/decode_goertzel_samples_raw.bin'])
bin_to_plot_f32('build/tests/decode_goertzel_samples.png',
                ['build/tests/decode_goertzel_samples_low.bin', 'build/tests/decode_goertzel_samples_high.bin'])

bin_to_plot_f32('build/tests/decode_goertzel_fir_samples_raw.png',
                ['build/tests/decode_goertzel_fir_samples_raw.bin'])
bin_to_plot_f32('build/tests/decode_goertzel_fir_samples.png',
                ['build/tests/decode_goertzel_fir_samples_low.bin', 'build/tests/decode_goertzel_fir_samples_high.bin'])

bin_to_plot_i32('build/tests/decode_goertzel_multiply_samples_raw.png',
                ['build/tests/decode_goertzel_multiply_samples_raw.bin'])
bin_to_plot_i32('build/tests/decode_goertzel_multiply_samples.png',
                ['build/tests/decode_goertzel_multiply_samples_low.bin', 'build/tests/decode_goertzel_multiply_samples_high.bin'])

bin_to_plot_i32('build/tests/hello_world_encoded_samples.png',
                ['build/tests/hello_world_encoded_samples.bin'])
