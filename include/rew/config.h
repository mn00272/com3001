/*** This file is part of FineFramework project ***/
#ifndef REW_CONFIG
#define REW_CONFIG

// Using Visual Studio
#ifdef _MSC_VER
#define REW_WINDOWS_MSVC 1
#define REW_WINDOWS 1
#pragma warning(disable : 4251)
#endif

// Using MinGW
#ifdef __MINGW32__
#define REW_WINDOWS_MINGW 1
#define REW_WINDOWS 1
#endif

// Using Linux GCC
#ifdef __linux__
#define REW_LINUX_GCC 1
#define REW_LINUX 1
#endif

// Using MAX OS X
#if defined(__APPLE__) && defined(__MACH__)
#define REW_OSX_CLANG 1
#define REW_OSX 1
#endif

#if !defined(REW_WINDOWS_MSVC) && !defined(REW_WINDOWS_MINGW) && !defined(REW_LINUX_GCC) && !defined(REW_OSX_CLANG)
#error Please use Visual Studio, MinGW, Linux GCC, or OS X Clang to compile COM3001!
#endif
#if !defined(REW_WINDOWS) && !defined(REW_LINUX) && !defined(REW_OSX)
#error Target platform not selected!
#endif

// COM3001 export for Windows
#if defined(REW_WINDOWS)
#ifdef REW_EXPORTS
#define REW_API __declspec(dllexport)
#else
#define REW_API __declspec(dllimport)
#endif
#else
#define REW_API
#endif

#define REW_NAMESPACE namespace rew

#if !defined(REW_API)
#error REW_API dllexport not defined!
#endif

#if !defined(HAS_NOEXCEPT)
#if defined(__clang__)
#if __has_feature(cxx_noexcept)
#define HAS_NOEXCEPT
#endif
#else
#if defined(__GXX_EXPERIMENTAL_CXX0X__) && __GNUC__ * 10 + __GNUC_MINOR__ >= 46 ||                                     \
    defined(_MSC_FULL_VER) && _MSC_FULL_VER >= 190023026
#define HAS_NOEXCEPT
#endif
#endif

#ifdef HAS_NOEXCEPT
#define NOEXCEPT noexcept
#else
#define NOEXCEPT
#endif
#endif

#include <cstddef>
#include <cstring>
#include <iostream>
#include <memory>
#include <cmath>

#ifndef RAD_TO_DEG
#define RAD_TO_DEG 57.2957795131
#endif
#ifndef DEG_TO_RAD
#define DEG_TO_RAD 0.01745329251
#endif
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef DEFAULT_LOW_TONE_FREQ
#define DEFAULT_LOW_TONE_FREQ 2400
#endif

#ifndef DEFAULT_HIGH_TONE_FREQ
#define DEFAULT_HIGH_TONE_FREQ 4800
#endif

#ifndef DEFAULT_SAMPLE_LENGTH_MS
#define DEFAULT_SAMPLE_LENGTH_MS 1.25
#endif

#ifndef DEFAULT_SAMPLE_RATE
#define DEFAULT_SAMPLE_RATE 48000
#endif

#ifndef PACKET_DATA_LENGTH
#define PACKET_DATA_LENGTH 32
#endif

#ifndef PACKET_SIGNATURE
#define PACKET_SIGNATURE                                                                                               \
    { 0xf0, 0xf0, 0xf0, 0xf0 }
#endif

#ifndef PACKET_SIGNATURE_SIZE
#define PACKET_SIGNATURE_SIZE 4
#endif

static_assert(sizeof(float) == 4, "size of the float must be 4 bytes");

#ifdef _WIN32
#define DELIMITER "\\"
#else
#define DELIMITER "/"
#endif

inline size_t NUMBER_OF_SAMPLES_PER_BIT(size_t SAMPLE_RATE, size_t FREQUENCY, double MS) {
    const auto samplesPerMs = SAMPLE_RATE / 1000.0;
    const auto cycleMs = (1.0 / static_cast<double>(FREQUENCY)) * 1000.0;
    const auto cycleNum = MS / cycleMs;
    const auto cycleSamples = SAMPLE_RATE / FREQUENCY;
    return static_cast<size_t>(std::ceil(cycleNum * cycleMs * samplesPerMs));
}

#define ARRAY_DELETER(T) [](const T* arr) { delete[] arr; }

#endif
