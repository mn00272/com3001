#ifndef REW_COMMON_INPUT_H
#define REW_COMMON_INPUT_H

#include "../config.h"

REW_NAMESPACE {
    /*!
     * @ingroup common
     */
    template <typename T>
    class Input {
      public:
        Input() = default;
        virtual ~Input() = default;

        virtual void process(const T* data, size_t length) = 0;
    };
}

#endif
