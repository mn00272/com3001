#ifndef REW_COMMON_CLAMP_H
#define REW_COMMON_CLAMP_H

#include "output.h"

REW_NAMESPACE {
    /*!
     * @ingroup common
     */
    template <typename I, typename O>
    class Clamp : public Input<I>, public Output<O> {
      public:
        Clamp(I treshold, O high, O low) : threshold(treshold), high(high), low(low) {
            forwardBuffer.reset(new O[128]);
            forwardLength = 128;
        }
        virtual ~Clamp() = default;

        void process(const I* data, const size_t length) override {
            if (forwardLength < length) {
                forwardBuffer.reset(new O[length]);
                forwardLength = length;
            }

            auto dst = forwardBuffer.get();

            for (size_t i = 0; i < length; i++) {
                const auto v = data[i] > threshold ? high : low;
                *dst++ = v;
            }

            Output<O>::forward(forwardBuffer.get(), length);
        }

      private:
        I threshold;
        O high;
        O low;
        std::unique_ptr<O> forwardBuffer;
        size_t forwardLength;
    };
}

#endif
