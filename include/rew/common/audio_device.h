#ifndef REW_AUDIO_DEVICE
#define REW_AUDIO_DEVICE

#include "../config.h"

REW_NAMESPACE {
    /*!
     * @ingroup common
     */
    struct AudioDevice {
        int index;
        std::string name;
        std::string api;
        int inputChannels;
        int outputChannels;
        double sampleRate;
        double lowInputLatency;
        double lowOutputLatency;
        double highInputLatency;
        double highOutputLatency;
    };
}

#endif
