#ifndef REW_COMMON_MULTIPLY_H
#define REW_COMMON_MULTIPLY_H

#include <memory>
#include "output.h"

REW_NAMESPACE {
    /*!
     * @ingroup common
     */
    template <typename T>
    class Multiply : public Input<T>, public Output<T> {
      public:
        Multiply(const T& value) : value(value) {
            forwardBuffer.reset(new T[128]);
            forwardLength = 128;
        }
        virtual ~Multiply() = default;

        void process(const T* data, const size_t length) override {
            if (forwardLength < length) {
                forwardBuffer.reset(new T[length]);
                forwardLength = length;
            }

            auto dst = forwardBuffer.get();

            for (size_t i = 0; i < length; i++) {
                const auto v = data[i] * value;
                // Output<T>::forward(&v, 1);
                *dst++ = v;
            }

            Output<T>::forward(forwardBuffer.get(), length);
        }

      private:
        T value;
        std::unique_ptr<T> forwardBuffer;
        size_t forwardLength;
    };
}

#endif
