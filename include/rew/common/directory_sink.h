#ifndef REW_COMMON_DIRECTORY_SINK_H
#define REW_COMMON_DIRECTORY_SINK_H

#include <fstream>
#include "named_raw_file.h"
#include "input.h"

REW_NAMESPACE {
    class DirectorySink : public rew::Input<rew::NamedRawFile> {
      public:
        DirectorySink(const std::string& destination) : destination(destination) {
        }
        virtual ~DirectorySink() = default;

        void process(const rew::NamedRawFile* data, const size_t length) override {
            for (size_t i = 0; i < length; i++) {
                const auto& namedFile = data[i];

                auto terminator = namedFile.length;
                for (size_t i = 0; i < namedFile.length; i++) {
                    if (namedFile.data[i] == '\0') {
                        terminator = i;
                        break;
                    }
                }

                if (terminator != namedFile.length) {
                    const auto sptr = reinterpret_cast<const char*>(namedFile.data.get());
                    const auto name = std::string(sptr, terminator);

                    const auto contents = reinterpret_cast<const char*>(namedFile.data.get() + terminator + 1);
                    const auto total = namedFile.length - terminator - 1;
                    std::string path = destination + DELIMITER + name;

                    std::cout << "Writing file: " << path << " of size: " << total << " bytes!" << std::endl;
                    std::fstream file(path, std::ios::out | std::ios::binary);
                    if (!file) {
                        std::cerr << "Failed to open file: " << path << " for writing!" << std::endl;
                    } else {
                        file.write(contents, total);
                    }
                } else {
                    std::cerr << "File index: " << namedFile.index
                              << " successfully inflated but contains bad filename!" << std::endl;
                }
            }
        }

      private:
        std::string destination;
    };
}

#endif
