#ifndef REW_NAMED_RAW_FILE_H
#define REW_NAMED_RAW_FILE_H

#include <memory>
#include "../config.h"

REW_NAMESPACE {
    /*!
     * @ingroup common
     */
    class NamedRawFile {
      public:
        NamedRawFile() = default;
        ~NamedRawFile() = default;
        NamedRawFile(const NamedRawFile& other) = delete;
        NamedRawFile& operator=(const NamedRawFile& other) = delete;
        NamedRawFile(NamedRawFile&& other) noexcept {
            swap(other);
        }
        void swap(NamedRawFile& other) noexcept {
            std::swap(data, other.data);
            std::swap(length, other.length);
            std::swap(index, other.index);
        }
        NamedRawFile& operator=(NamedRawFile&& other) noexcept {
            if (this != &other) {
                swap(other);
            }
            return *this;
        }

        std::unique_ptr<uint8_t[]> data;
        size_t length = 0;
        int index = 0;
    };
}

#endif
