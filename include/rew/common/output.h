#ifndef REW_COMMON_OUTPUT_H
#define REW_COMMON_OUTPUT_H

#include <list>
#include <memory>
#include <stdexcept>
#include "input.h"

REW_NAMESPACE {
    /*!
     * @ingroup common
     */
    template <typename T>
    class Output {
      public:
        Output() = default;
        Output(const Output& other) = delete;
        Output(Output&& other) noexcept {
            swap(other);
        }
        Output& operator=(const Output& other) = delete;
        Output& operator=(Output&& other) noexcept {
            if (this != &other) {
                swap(other);
            }
            return *this;
        }
        void swap(Output& other) noexcept {
            std::swap(inputs, other.inputs);
        }
        virtual ~Output() = default;

        void connect(const std::shared_ptr<Input<T>>& input) {
            if (!input)
                throw std::invalid_argument("cannot connect nullptr input to output");
            inputs.push_back(input.get());
        }

        void connect(Input<T>* input) {
            if (!input)
                throw std::invalid_argument("cannot connect nullptr input to output");
            inputs.push_back(input);
        }

        void forward(const T* data, const size_t length) {
            for (auto& input : inputs)
                input->process(data, length);
        }

      private:
        std::list<Input<T>*> inputs;
    };
}

#endif
