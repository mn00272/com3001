#ifndef REW_CRC8_H
#define REW_CRC8_H

#include <stdlib.h>
#include <stdint.h>
#include "../config.h"

REW_NAMESPACE {
    namespace detail {
        /**
         * The type of the CRC values.
         *
         * This type must be big enough to contain at least 8 bits.
         */
        typedef uint8_t crc_t;

        /**
         * The configuration type of the CRC algorithm.
         */
        typedef struct {
            crc_t poly;       /*!< The CRC polynomial */
            bool reflect_in;  /*!< Whether the input shall be reflected or not */
            crc_t xor_in;     /*!< The initial value of the register */
            bool reflect_out; /*!< Whether the output shall be reflected or not */
            crc_t xor_out;    /*!< The value which shall be XOR-ed to the final CRC value */
        } crc_cfg_t;

        /*inline crc_t crc_reflect(crc_t data, size_t data_len)
        {
            unsigned int i;
            crc_t ret;

            ret = data & 0x01;
            for (i = 1; i < data_len; i++) {
                data >>= 1;
                ret = (ret << 1) | (data & 0x01);
            }
            return ret;
        }*/

        /**
         * Calculate the initial crc value.
         *
         * \param[in] cfg  A pointer to an initialised crc_cfg_t structure.
         * \return     The initial crc value.
         */
        /*inline crc_t crc_init(const crc_cfg_t *cfg)
        {
            unsigned int i;
            bool bit;
            crc_t crc = cfg->xor_in;
            for (i = 0; i < 8; i++) {
                bit = crc & 0x01;
                if (bit) {
                    crc = ((crc ^ cfg->poly) >> 1) | 0x80;
                } else {
                    crc >>= 1;
                }
            }
            return crc & 0xff;
        }*/

        /**
         * Update the crc value with new data.
         *
         * \param[in] crc      The current crc value.
         * \param[in] cfg      A pointer to an initialised crc_cfg_t structure.
         * \param[in] data     Pointer to a buffer of \a data_len bytes.
         * \param[in] data_len Number of bytes in the \a data buffer.
         * \return             The updated crc value.
         */
        /*inline crc_t crc_update(const crc_cfg_t *cfg, crc_t crc, const void *data, size_t data_len)
        {
            const unsigned char *d = (const unsigned char *)data;
            unsigned int i;
            bool bit;
            unsigned char c;

            while (data_len--) {
                if (cfg->reflect_in) {
                    c = crc_reflect(*d++, 8);
                } else {
                    c = *d++;
                }
                for (i = 0; i < 8; i++) {
                    bit = crc & 0x80;
                    crc = (crc << 1) | ((c >> (7 - i)) & 0x01);
                    if (bit) {
                        crc ^= cfg->poly;
                    }
                }
                crc &= 0xff;
            }
            return crc & 0xff;
        }*/

        /**
         * Calculate the final crc value.
         *
         * \param[in] cfg  A pointer to an initialised crc_cfg_t structure.
         * \param[in] crc  The current crc value.
         * \return     The final crc value.
         */
        /*inline crc_t crc_finalize(const crc_cfg_t *cfg, crc_t crc)
        {
            unsigned int i;
            bool bit;

            for (i = 0; i < 8; i++) {
                bit = crc & 0x80;
                crc <<= 1;
                if (bit) {
                    crc ^= cfg->poly;
                }
            }
            if (cfg->reflect_out) {
                crc = crc_reflect(crc, 8);
            }
            return (crc ^ cfg->xor_out) & 0xff;
        }*/

        static constexpr crc_t crc_table[256] = {
            0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15, 0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d, 0x70, 0x77,
            0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65, 0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d, 0xe0, 0xe7, 0xee, 0xe9,
            0xfc, 0xfb, 0xf2, 0xf5, 0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd, 0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b,
            0x82, 0x85, 0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd, 0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
            0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea, 0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2, 0x8f, 0x88,
            0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a, 0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32, 0x1f, 0x18, 0x11, 0x16,
            0x03, 0x04, 0x0d, 0x0a, 0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42, 0x6f, 0x68, 0x61, 0x66, 0x73, 0x74,
            0x7d, 0x7a, 0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c, 0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
            0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec, 0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4, 0x69, 0x6e,
            0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c, 0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44, 0x19, 0x1e, 0x17, 0x10,
            0x05, 0x02, 0x0b, 0x0c, 0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34, 0x4e, 0x49, 0x40, 0x47, 0x52, 0x55,
            0x5c, 0x5b, 0x76, 0x71, 0x78, 0x7f, 0x6a, 0x6d, 0x64, 0x63, 0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
            0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13, 0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb, 0x96, 0x91,
            0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83, 0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb, 0xe6, 0xe1, 0xe8, 0xef,
            0xfa, 0xfd, 0xf4, 0xf3};

        inline crc_t crc_update(crc_t crc, const void* data, size_t data_len) {
            const unsigned char* d = (const unsigned char*)data;
            unsigned int tbl_idx;

            while (data_len--) {
                tbl_idx = crc ^ *d;
                crc = crc_table[tbl_idx] & 0xff;
                d++;
            }
            return crc & 0xff;
        }
    } // namespace detail

    /*!
     * @ingroup common
     */
    inline uint8_t crc8(const uint8_t* data, const size_t length) {
        /*detail::crc_cfg_t cfg;
        detail::crc_t crc;

        std::cout << "crc(";
        for (size_t i = 0; i < length; i++) {
            std::cout << int(data[i]) << ", ";
        }

        crc = detail::crc_init(&cfg);
        crc = detail::crc_update(&cfg, crc, data, length);
        crc = detail::crc_finalize(&cfg, crc);

        std::cout << ") = " << int(crc) << std::endl;

        return crc;*/

        detail::crc_cfg_t cfg = {0};
        detail::crc_t crc = {0};

        return detail::crc_update(crc, data, length);
    }
}

#endif
