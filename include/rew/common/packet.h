#ifndef REW_COMMON_PACKET_H
#define REW_COMMON_PACKET_H

#include "../config.h"
#include <cstdint>
#include <vector>
#include <cstring>

REW_NAMESPACE {
    /*!
     * @ingroup common
     */
    struct Packet {
        Packet() = default;
        Packet(const Packet& other) {
            std::memcpy(this, &other, sizeof(Packet));
        }

        uint16_t fileIndex = 0;
        uint16_t packetIndex = 0;
        uint8_t data[PACKET_DATA_LENGTH] = {0};
        uint8_t crc = 0;

        const static size_t realSize =
            +sizeof(Packet::fileIndex) + sizeof(Packet::packetIndex) + sizeof(Packet::data) + sizeof(Packet::crc);

        Packet& operator=(const Packet& other) {
            std::memcpy(this, &other, sizeof(Packet));
            return *this;
        }
    };
}

#define PACKET_REAL_SIZE (rew::Packet::realSize)

static_assert(sizeof(rew::Packet) == 38, "the compiler size of the Packet structure must be 42");

#endif
