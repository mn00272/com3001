#ifndef REW_FILE_ASSEMBLER_H
#define REW_FILE_ASSEMBLER_H

#include <unordered_map>
#include <vector>
#include "decompressor.h"
#include "../config.h"
#include "../common/packet.h"
#include "../common/output.h"
#include "../common/named_raw_file.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API FileAssembler : public Input<Packet>, public Output<NamedRawFile> {
      public:
        FileAssembler(std::shared_ptr<Decompressor> decompressor);
        virtual ~FileAssembler() = default;

        void process(const Packet* data, size_t length) override;

        void push(const Packet& packet);

      private:
        std::shared_ptr<Decompressor> decompressor;
        std::unordered_map<int, std::vector<Packet>> cache;
    };
}

#endif
