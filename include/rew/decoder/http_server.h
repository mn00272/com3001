#ifndef REW_HTTP_SERVER_H
#define REW_HTTP_SERVER_H

#include <unordered_map>
#include <mutex>
#include <memory>
#include <string>
#include <vector>
#include <list>
#include "../common/input.h"
#include "../common/named_raw_file.h"
#include "http_request.h"
#include "http_response.h"

REW_NAMESPACE {
    class HttpConnection;

    /*!
     * @ingroup decoder
     */
    class REW_API HttpServer : public Input<NamedRawFile> {
      public:
        HttpServer() = default;
        virtual ~HttpServer() = default;

        void process(const NamedRawFile* data, size_t length) override;
        HttpResponse serve(const HttpRequest& request);
        void disconnect(const std::shared_ptr<HttpConnection>& connection);
        virtual void start() = 0;
        virtual void stop() = 0;

      protected:
        void accept(const std::shared_ptr<HttpConnection>& connection);

      private:
        std::unordered_map<std::string, std::string> files;
        std::list<std::shared_ptr<HttpConnection>> connections;
        std::mutex mutex;
    };
}

#endif
