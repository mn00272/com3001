#ifndef REW_BYTE_ARRAY_ASSEMBLER_H
#define REW_BYTE_ARRAY_ASSEMBLER_H

#include "../common/output.h"
#include "../common/merge.h"
#include "bucket.h"
#include <functional>

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API ByteArrayAssembler : public Input<MergedValue<int, 2>>, public Output<uint8_t> {
      public:
        ByteArrayAssembler(size_t frequency, size_t sampleRate, double ms);
        virtual ~ByteArrayAssembler() = default;
        ByteArrayAssembler(ByteArrayAssembler&& other) noexcept;
        ByteArrayAssembler(const ByteArrayAssembler& other) = delete;
        void swap(ByteArrayAssembler& other) noexcept;
        ByteArrayAssembler& operator=(ByteArrayAssembler&& other) noexcept;
        ByteArrayAssembler& operator=(const ByteArrayAssembler& other) = delete;

        void process(const MergedValue<int, 2>* data, const size_t length) override;

      private:
        static size_t count(const uint8_t* start, const uint8_t* end, const uint8_t target);
        static uint8_t toByte(const uint8_t* begin, const uint8_t* end);

        std::vector<uint8_t> buffer;
        size_t samplesPerBit;
        size_t samplesPerPacket;
        size_t filled;
    };
}

#endif
