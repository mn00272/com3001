#ifndef REW_AUDIO_SOURCE_H
#define REW_AUDIO_SOURCE_H

#include "../config.h"
#include "../common/output.h"
#include "audio_reader.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API AudioSource : public Output<float> {
      public:
        AudioSource(std::shared_ptr<AudioReader> audioReader);
        virtual ~AudioSource();

        void open(const std::string& path);
        void process();
        void close();

      private:
        std::shared_ptr<AudioReader> audioReader;
    };
}

#endif
