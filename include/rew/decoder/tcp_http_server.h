#ifndef REW_TCP_HTTP_SERVER_H
#define REW_TCP_HTTP_SERVER_H

#include "http_server.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API TcpHttpServer : public HttpServer {
      public:
        TcpHttpServer(const std::string& address, int port);
        virtual ~TcpHttpServer();

        void start() override;
        void stop() override;

      private:
        class Impl;
        std::unique_ptr<Impl> pimpl;
    };
}

#endif
