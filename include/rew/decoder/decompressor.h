#ifndef REW_DECOMPRESSOR_H
#define REW_DECOMPRESSOR_H

#include "../config.h"
#include <memory>
#include <vector>
#include <functional>

struct z_stream_s;

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API Decompressor {
      public:
        typedef std::vector<uint8_t> Chunk;

        Decompressor();
        virtual ~Decompressor();
        Decompressor(const Decompressor& other) = delete;
        Decompressor(Decompressor&& other) noexcept;
        void swap(Decompressor& other) noexcept;
        Decompressor& operator=(const Decompressor& other) = delete;
        Decompressor& operator=(Decompressor&& other) noexcept;

        std::vector<uint8_t> fromChunks(const std::vector<Chunk>& chunks) const;
        std::vector<uint8_t> fromChunks(const std::function<std::pair<const uint8_t*, size_t>()>& callback) const;

      private:
        std::unique_ptr<z_stream_s> stream;
    };
}

#endif
