#ifndef REW_AUDIO_READER
#define REW_AUDIO_READER

#include <fstream>
#include <string>
#include "../config.h"

REW_NAMESPACE {
    /*!
     * @brief Audio writer base class
     * @ingroup decoder
     */
    class REW_API AudioReader {
      public:
        AudioReader();
        virtual ~AudioReader() = default;
        /*!
         * @brief Opens a file for reading
         */
        virtual bool open(const std::string& path) = 0;
        /*!
         * @brief Reads a chunk of data from a file
         */
        virtual bool read(unsigned char* data, size_t* length) = 0;
        /*!
         * @brief Closes the file
         */
        virtual void close() = 0;
        /*!
         * @brief Checks if end of file
         */
        virtual bool eof() const = 0;
        /*!
         * @brief Checks if file is open
         */
        bool isOpen() const;
        /*!
         * @brief Returns the target output size specified in function create()
         */
        size_t getSize() const;
        /*!
         * @brief Returns the sample rate
         */
        int getSampleRate() const;
        /*!
         * @brief Returns the number of channels
         */
        int getNumOfChannels() const;
        /*!
         * @brief Returns the bit depth of one sample
         */
        int getBitsPerSample() const;
        /*!
         * @brief Returns maximum output size of a single read() call
         */
        int getChunkSize() const;
        /*!
         * @brief Checks if a file is open
         */
        operator bool() const;

      protected:
        bool loaded;
        size_t size;
        int sampleRate;
        int channelCount;
        int bitsPerSample;
        int chunkSize;
    };
};
#endif
