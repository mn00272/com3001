#ifndef REW_DECODER_FRAME_SYNC_H
#define REW_DECODER_FRAME_SYNC_H

#include <vector>
#include "../config.h"
#include "../common/multiply.h"
#include "../common/merge.h"
#include "../common/clamp.h"
#include "byte_assembler.h"
#include "goertzel.h"
#include "fir_filter.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API FrameSync : public Input<float>,
                              public Input<MergedValue<int, 2>>,
                              public Input<ByteWithIndex>,
                              public Output<float> {
      public:
        FrameSync(size_t lowToneFreq = DEFAULT_LOW_TONE_FREQ, size_t highToneFreq = DEFAULT_HIGH_TONE_FREQ,
                  double sampleLengthMs = DEFAULT_SAMPLE_LENGTH_MS);
        virtual ~FrameSync() = default;

        void process(const float* data, size_t length) override;
        void process(const MergedValue<int, 2>* data, size_t length) override;
        void process(const ByteWithIndex* data, size_t length) override;

        inline bool hasFrame() const {
            return forwardData;
        }

      private:
        struct SyncByte {
            size_t timing;
            uint8_t byte;
        };

        bool checkSequenceMatch();
        bool checkTimingMatch();

        std::vector<SyncByte> syncBytes;

        std::shared_ptr<FirFilter> firFilterLow;
        std::shared_ptr<FirFilter> firFilterHigh;
        std::shared_ptr<Goertzel> goertzelLow;
        std::shared_ptr<Goertzel> goertzelHigh;
        std::shared_ptr<Clamp<float, int>> clampLow;
        std::shared_ptr<Clamp<float, int>> clampHigh;
        std::shared_ptr<Multiply<float>> multiplyLow;
        std::shared_ptr<Multiply<float>> multiplyHigh;
        std::shared_ptr<Merge<int, 2>> merger;

        size_t lastByteSamples;
        std::shared_ptr<ByteAssembler> byteAssembler;
        size_t samplesPerBit;
        bool forwardData;
        size_t samplesPerPacket;
        size_t totalForwarded;
        size_t samplesLeftoverIndex;
    };
}

#endif
