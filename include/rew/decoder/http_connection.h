#ifndef REW_HTTP_CONNECTION_H
#define REW_HTTP_CONNECTION_H

#include "../config.h"
#include "http_response.h"
#include "http_request.h"

REW_NAMESPACE {
    class HttpServer;

    /*!
     * @ingroup decoder
     */
    class REW_API HttpConnection {
      public:
        HttpConnection(HttpServer& server);
        virtual ~HttpConnection() = default;

        virtual void start() = 0;
        virtual void stop() = 0;

      protected:
        void receive(const std::string& address, const std::string& payload);
        void send(const HttpRequest& req, const HttpResponse& res);
        virtual void sendRaw(std::shared_ptr<std::string> payload) = 0;
        HttpRequest getRequest(const std::string& address, const std::string& payload) const;

      private:
        HttpServer& server;
    };
}

#endif
