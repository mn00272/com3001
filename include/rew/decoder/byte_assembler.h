#ifndef REW_BYTE_ASSEMBLER_H
#define REW_BYTE_ASSEMBLER_H

#include "../common/output.h"
#include "../common/merge.h"
#include "bucket.h"
#include <functional>

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    struct ByteWithIndex {
        uint8_t byte;
        size_t index;
    };
    /*!
     * @ingroup decoder
     */
    class REW_API ByteAssembler : public Input<MergedValue<int, 2>>, public Output<ByteWithIndex> {
      public:
        ByteAssembler(size_t frequency, size_t sampleRate, double ms);
        virtual ~ByteAssembler() = default;
        ByteAssembler(ByteAssembler&& other) noexcept;
        ByteAssembler(const ByteAssembler& other) = delete;
        void swap(ByteAssembler& other) noexcept;
        ByteAssembler& operator=(ByteAssembler&& other) noexcept;
        ByteAssembler& operator=(const ByteAssembler& other) = delete;

        void process(const MergedValue<int, 2>* data, const size_t length) override;

      private:
        static size_t count(const uint8_t* start, const uint8_t* end, const uint8_t target);
        static uint8_t toByte(const uint8_t* begin, const uint8_t* end);

        std::vector<uint8_t> buffer;
        uint8_t* dst;
        uint8_t* end;
        uint8_t* begin;
        size_t samplesPerBit;
        size_t checkLength;
        bool found;
    };
}

#endif
