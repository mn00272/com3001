#ifndef REW_DECODER_H
#define REW_DECODER_H

#include <utility>
#include <functional>
#include <memory>
#include "goertzel.h"
#include "byte_assembler.h"
#include "decompressor.h"
#include "wav_reader.h"
#include "fir_filter.h"
#include "audio_source.h"
#include "packet_assembler.h"
#include "file_assembler.h"
#include "../common/packet.h"
#include "../common/output.h"
#include "../common/clamp.h"
#include "../common/multiply.h"
#include "../common/merge.h"
#include "../common/named_raw_file.h"
#include "frame_sync.h"

/*!
 * @defgroup decoder Decoder
 * @brief The clientside part of the project for decoding the signal
 */

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API Decoder {
      public:
        Decoder(const std::shared_ptr<Output<float>>& source, const std::shared_ptr<Input<NamedRawFile>>& sink,
                size_t lowToneFreq = DEFAULT_LOW_TONE_FREQ, size_t highToneFreq = DEFAULT_HIGH_TONE_FREQ,
                double sampleLengthMs = DEFAULT_SAMPLE_LENGTH_MS);

        Decoder(Decoder&& other) noexcept;
        Decoder(const Decoder& other) = delete;
        virtual ~Decoder() = default;
        void swap(Decoder& other) noexcept;
        Decoder& operator=(Decoder&& other) noexcept;
        Decoder& operator=(const Decoder& other) = delete;

        std::shared_ptr<FrameSync> frameSync;
        std::shared_ptr<FirFilter> firFilterLow;
        std::shared_ptr<FirFilter> firFilterHigh;
        std::shared_ptr<Goertzel> goertzelLow;
        std::shared_ptr<Goertzel> goertzelHigh;
        std::shared_ptr<Clamp<float, int>> clampLow;
        std::shared_ptr<Clamp<float, int>> clampHigh;
        std::shared_ptr<Multiply<float>> multiplyLow;
        std::shared_ptr<Multiply<float>> multiplyHigh;
        std::shared_ptr<Merge<int, 2>> merger;
        std::shared_ptr<ByteAssembler> byteAssembler;
        std::shared_ptr<PacketAssembler> packetAssembler;
        std::shared_ptr<Decompressor> decompressor;
        std::shared_ptr<FileAssembler> fileAssembler;
    };
}

#endif
