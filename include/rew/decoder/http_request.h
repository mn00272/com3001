#ifndef REW_HTTP_REQUEST_H
#define REW_HTTP_REQUEST_H

#include "../config.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    struct HttpRequest {
        std::string method;
        std::string path;
        std::string type;
        std::string address;
    };
}

#endif
