#ifndef REW_DECODER_FIR_FILTER_H
#define REW_DECODER_FIR_FILTER_H

#include "../config.h"
#include "../common/output.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API FirFilter : public Input<float>, public Output<float> {
      public:
        FirFilter();
        virtual ~FirFilter() = default;

        void process(const float* data, size_t length) override;

      private:
    };
}

#endif
