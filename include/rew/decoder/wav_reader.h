#ifndef REW_WAV_READER
#define REW_WAV_READER

#include "audio_reader.h"

REW_NAMESPACE {
    /*!
     * @brief WAV file reader
     * @ingroup decoder
     */
    class REW_API WavReader : public AudioReader {
      public:
        WavReader();
        virtual ~WavReader();

        /*!
         * Opens a file for reading
         * @param path Relative or absolute path to the file
         * @return True if opening the file did not generate an error
         */
        bool open(const std::string& path) override;
        bool read(unsigned char* data, size_t* length) override;
        void close() override;
        bool eof() const override;

      private:
        std::fstream input;
        unsigned int subChunk2Size;
        unsigned int readTotal;
        unsigned int dataOffset;
    };
}

#endif
