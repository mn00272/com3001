#ifndef REW_HTTP_RESPONSE_H
#define REW_HTTP_RESPONSE_H

#include "../config.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    struct HttpResponse {
        std::string contentType;
        int status;
        std::string body;
    };
}

#endif
