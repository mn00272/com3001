#ifndef REW_DECODER_SOURCE_MEMORY_H
#define REW_DECODER_SOURCE_MEMORY_H

#include "../config.h"
#include "../common/output.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API SourceMemory : public Output<float> {
      public:
        SourceMemory();
        virtual ~SourceMemory() = default;

        void consume(const float* src, size_t length);

      private:
    };
}

#endif
