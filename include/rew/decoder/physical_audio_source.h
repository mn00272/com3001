#ifndef REW_PHYSICAL_AUDIO_READER
#define REW_PHYSICAL_AUDIO_READER

#include <vector>
#include <mutex>
#include <queue>
#include <array>
#include <condition_variable>
#include "../common/output.h"
#include "../common/audio_device.h"

#ifndef PORTAUDIO_H
typedef void PaStream;
#endif

REW_NAMESPACE {
    class AudioInitializer;

    /*!
     * @brief Physical Audio Reader using PortAudio
     * @ingroup decoder
     */
    class REW_API PhysicalAudioSource : public Output<float> {
      public:
        PhysicalAudioSource(AudioDevice* device = nullptr);
        virtual ~PhysicalAudioSource();
        void start();
        void close();
        void process();
        static std::vector<AudioDevice> getDevices();

      private:
        void callback(const float* input, unsigned long frameCount);

        static constexpr size_t BUFFER_SIZE = 256;

        PaStream* stream;
        std::shared_ptr<AudioInitializer> initializer;
        bool started;
        std::mutex lock;
        std::condition_variable cv;
        std::queue<std::array<float, BUFFER_SIZE>> chunks;
    };
}

#endif
