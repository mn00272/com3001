#ifndef REW_PACKET_ASSEMBLER_H
#define REW_PACKET_ASSEMBLER_H

#include "../config.h"
#include "../common/packet.h"
#include "../common/output.h"
#include "byte_assembler.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API PacketAssembler : public Input<ByteWithIndex>, public Output<Packet> {
      public:
        PacketAssembler() = default;
        virtual ~PacketAssembler() = default;

        void process(const ByteWithIndex* data, size_t length) override;

      private:
        std::vector<uint8_t> buffer;
    };
}

#endif