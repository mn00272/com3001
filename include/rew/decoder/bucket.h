#ifndef REW_BUCKET_H
#define REW_BUCKET_H

#include "../config.h"
#include <vector>

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    template <typename T>
    class REW_API Bucket {
      public:
        Bucket() = default;
        Bucket(Bucket&& other) noexcept : vector(std::move(other.vector)) {
        }
        Bucket(const Bucket& other) = delete;
        virtual ~Bucket() = default;
        void swap(Bucket& other) noexcept {
            using std::swap;
            swap(vector, other.vector);
        }
        Bucket& operator=(Bucket&& other) noexcept {
            if (this != &other) {
                swap(other);
            }
            return *this;
        }
        Bucket& operator=(const Bucket& other) = delete;

        void pushBack(const typename std::vector<T>::value_type& value) {
            if (vector.capacity() <= vector.size()) {
                vector.reserve(vector.capacity() + 256);
            }
            vector.push_back(value);
        }

        size_t size() const {
            return vector.size();
        }

        typename std::vector<T>::iterator begin() {
            return vector.begin();
        }

        typename std::vector<T>::const_iterator begin() const {
            return vector.begin();
        }

        typename std::vector<T>::iterator end() {
            return vector.end();
        }

        typename std::vector<T>::const_iterator end() const {
            return vector.end();
        }

        void clear() {
            vector.clear();
        }

        typename std::vector<T>::value_type& operator[](size_t i) {
            return vector[i];
        }

        const typename std::vector<T>::value_type& operator[](size_t i) const {
            return vector[i];
        }

      private:
        std::vector<T> vector;
    };
}

#endif
