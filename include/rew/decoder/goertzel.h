#ifndef REW_DECODER_GOERTZEL_H
#define REW_DECODER_GOERTZEL_H

#include "../config.h"
#include "../common/output.h"

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API Goertzel : public Input<float>, public Output<float> {
      public:
        Goertzel(size_t blockSize, float targetFrequency, float sampleRate);
        virtual ~Goertzel() = default;

        void process(const float* data, size_t length) override;
        float operator()(const float* samples);
        inline size_t getBlockSize() const {
            return blockSize;
        }

      private:
        void processSample(float sample);
        void processSamples(const float* samples, size_t length);
        void init(size_t blockSize, float targetFrequency, float sampleRate);
        void reset();
        float getMagnitude() const;

        float coeff;
        float Q1;
        float Q2;
        float sine;
        float cosine;
        size_t blockSize;
        std::unique_ptr<float[]> buffer;
        std::unique_ptr<float[]> forwardBuffer;
        size_t forwardLength;
    };
}

#endif
