#ifndef REW_TCP_HTTP_CONNECTION_H
#define REW_TCP_HTTP_CONNECTION_H

#include "http_connection.h"

REW_NAMESPACE {
    class TcpHttpServer;

    /*!
     * @ingroup decoder
     */
    class REW_API TcpHttpConnection : public HttpConnection, public std::enable_shared_from_this<TcpHttpConnection> {
      public:
        TcpHttpConnection(TcpHttpServer& server, void* socket);
        virtual ~TcpHttpConnection();

        void start() override;
        void stop() override;
        void sendRaw(std::shared_ptr<std::string> payload);

      private:
        void doRead();

        class Impl;
        std::shared_ptr<Impl> pimpl;
    };
}

#endif
