#ifndef REW_EMITTER_SYNTH_H
#define REW_EMITTER_SYNTH_H

#include "../config.h"

REW_NAMESPACE {
    /*!
     * @ingroup encoder
     */
    class REW_API Synth {
      public:
        virtual size_t operator()(float* dst) const = 0;
    };
}

#endif
