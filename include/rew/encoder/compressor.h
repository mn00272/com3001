#ifndef REW_COMPRESSOR_H
#define REW_COMPRESSOR_H

#include "../config.h"
#include <vector>
#include <memory>
#include <functional>
#include "../common/packet.h"

struct z_stream_s;

REW_NAMESPACE {
    /*!
     * @ingroup decoder
     */
    class REW_API Compressor {
      public:
        typedef std::vector<uint8_t> Chunk;

        Compressor(size_t chunkSize);
        virtual ~Compressor();
        Compressor(const Compressor& other) = delete;
        Compressor(Compressor&& other) noexcept;
        void swap(Compressor& other) noexcept;
        Compressor& operator=(const Compressor& other) = delete;
        Compressor& operator=(Compressor&& other) noexcept;

        std::vector<Chunk> getChunks(const uint8_t* data, size_t length) const;
        void getChunks(const uint8_t* data, size_t length, const std::function<void(const Chunk&)>& callback) const;

        inline size_t getChunkSize() const {
            return chunkSize;
        }

      private:
        std::unique_ptr<z_stream_s> stream;
        size_t chunkSize = 0;
    };
}

#endif
