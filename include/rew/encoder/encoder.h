#ifndef REW_ENCODER_H
#define REW_ENCODER_H

#include "../config.h"
#include "tone_synth.h"
#include "compressor.h"
#include "file_loader.h"
#include "packet_generator.h"
#include "samples_generator.h"
#include "wav_writer.h"
#include "audio_sink.h"

/*!
 * @defgroup encoder Encoder
 * @brief The server side part of the project for encoding the data into signal
 */

REW_NAMESPACE {
    /*!
     * @ingroup encoder
     */
    class REW_API Encoder {
      public:
        Encoder(const std::shared_ptr<Output<NamedRawFile>>& source, const std::shared_ptr<Input<float>>& sink,
                size_t lowToneFreq = DEFAULT_LOW_TONE_FREQ, size_t highToneFreq = DEFAULT_HIGH_TONE_FREQ,
                double sampleLengthMs = DEFAULT_SAMPLE_LENGTH_MS);
        Encoder(Encoder&& other) noexcept;
        Encoder(const Encoder& other) = delete;
        virtual ~Encoder() = default;
        void swap(Encoder& other) noexcept;
        Encoder& operator=(Encoder&& other) noexcept;
        Encoder& operator=(const Encoder& other) = delete;

        std::shared_ptr<Output<FileName>> source;
        std::shared_ptr<Input<float>> sink;
        std::shared_ptr<FileLoader> fileLoader;
        std::shared_ptr<Compressor> compressor;
        std::shared_ptr<PacketGenerator> packetGenerator;
        std::shared_ptr<SamplesGenerator> samplesGenerator;
    };
}

#endif
