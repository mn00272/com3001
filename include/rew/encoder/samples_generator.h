#ifndef REW_SAMPLES_GENERATOR_H
#define REW_SAMPLES_GENERATOR_H

#include "../common/output.h"
#include "../common/packet.h"
#include "tone_synth.h"

REW_NAMESPACE {
    class REW_API SamplesGenerator : public Input<Packet>, public Output<float> {
      public:
        SamplesGenerator(size_t lowToneFreq = DEFAULT_LOW_TONE_FREQ, size_t highToneFreq = DEFAULT_HIGH_TONE_FREQ,
                         double sampleLengthMs = DEFAULT_SAMPLE_LENGTH_MS);
        virtual ~SamplesGenerator() = default;

        void process(const Packet* data, size_t length) override;

        float* encodeRaw(const uint8_t* data, size_t length) const;
        void encodeRaw(const uint8_t* data, size_t length, float* dst) const;
        inline size_t getNumOfSamplesRaw(const size_t length) const {
            return (length * 8 + 1) * highTone.getSamplesPerBlock() + (length)*highTone.getSamplesPerBlock();
        }

      private:
        ToneSynth lowTone;
        ToneSynth highTone;
        std::unique_ptr<float[]> nullTone;
    };
}

#endif
