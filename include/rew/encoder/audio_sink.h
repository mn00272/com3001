#ifndef REW_AUDIO_SINK_H
#define REW_AUDIO_SINK_H

#include "../config.h"
#include "../common/output.h"
#include "audio_writer.h"

REW_NAMESPACE{
    class REW_API AudioSink : public Input<float> {
    public:
        AudioSink(std::shared_ptr<AudioWriter> audioWriter);
        virtual ~AudioSink();

        void open(const std::string& path);
        void process(const float* data, size_t length) override;
        void close();
    private:
        std::shared_ptr<AudioWriter> audioWriter;
    };
}

#endif