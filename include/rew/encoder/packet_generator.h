#ifndef REW_PACKET_GENERATOR_H
#define REW_PACKET_GENERATOR_H

#include "file_loader.h"
#include "../common/packet.h"
#include "compressor.h"

REW_NAMESPACE {
    class REW_API PacketGenerator : public Input<NamedRawFile>, public Output<Packet> {
      public:
        PacketGenerator(std::shared_ptr<Compressor> compressor);
        virtual ~PacketGenerator() = default;

        void process(const NamedRawFile* data, size_t length) override;

      private:
        std::shared_ptr<Compressor> compressor;
    };
}

#endif
