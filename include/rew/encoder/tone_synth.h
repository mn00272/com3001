#ifndef REW_EMITTER_TONE_SYNTH_H
#define REW_EMITTER_TONE_SYNTH_H

#include <memory>
#include "synth.h"

REW_NAMESPACE {
    /*!
     * @ingroup encoder
     */
    class REW_API ToneSynth : public Synth {
      public:
        ToneSynth() = default;
        ToneSynth(size_t frequency, size_t sampleRate, double ms);
        ToneSynth(ToneSynth&& other) noexcept = default;
        ToneSynth(const ToneSynth& other) = delete;
        ToneSynth& operator=(ToneSynth&& other) noexcept = default;
        ToneSynth& operator=(const ToneSynth& other) = delete;

        size_t operator()(float* dst) const override;
        inline size_t getSamplesPerBlock() const {
            return total;
        }

      private:
        void allocate(size_t sampleRate, double ms);

        size_t frequency;
        size_t total;
        std::unique_ptr<float[]> src;
    };
}

#endif
