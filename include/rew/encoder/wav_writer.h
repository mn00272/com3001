#ifndef REW_WAV_SAVER
#define REW_WAV_SAVER

#include <fstream>
#include "audio_writer.h"

REW_NAMESPACE {
    /*!
     * @brief WAV file saver
     * @ingroup encoder
     */
    class REW_API WavWriter : public AudioWriter {
    public:
        WavWriter();
        virtual ~WavWriter();

        bool open(const std::string& path, int bitsPerSample, int sampleRate, int numChannels) override;
        void close() override;
        bool write(const unsigned char* data, size_t length) override;
        bool writeFooter() override;
        bool eof() const override;
        size_t getPos() const override;
    private:
        void writeHeader();

        std::fstream output;
        bool eos;
        size_t dataOffset;
        size_t dataWritten;
    };
};

#endif
