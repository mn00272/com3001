#ifndef REW_PHYSICAL_AUDIO_SINK
#define REW_PHYSICAL_AUDIO_SINK

#include <mutex>
#include <queue>
#include <vector>
#include <condition_variable>
#include "../common/output.h"
#include "../common/audio_device.h"

#ifndef PORTAUDIO_H
typedef void PaStream;
#endif

REW_NAMESPACE {
    class AudioInitializer;

    class REW_API PhysicalAudioSink : public Input<float> {
      public:
        PhysicalAudioSink(AudioDevice* device = nullptr);
        virtual ~PhysicalAudioSink();

        void start();
        void close();
        static std::vector<AudioDevice> getDevices();
        void process(const float* data, size_t length) override;
        void join();

      private:
        void callback(float* output, unsigned long frameCount);

        static constexpr size_t BUFFER_SIZE = 256;

        PaStream* stream;
        std::shared_ptr<AudioInitializer> initializer;
        bool started;
        std::mutex lock;
        std::condition_variable cv;
        std::queue<std::vector<float>> chunks;
        std::vector<float> next;
        size_t nextPos;
    };
}

#endif
