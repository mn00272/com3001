#ifndef REW_AUDIO_SAVER
#define REW_AUDIO_SAVER

#include <fstream>
#include <string>
#include "../config.h"

REW_NAMESPACE {
    /*!
     * @brief Audio saver base class
     * @ingroup encoder
     */
    class REW_API AudioWriter {
      public:
        AudioWriter();
        virtual ~AudioWriter() = default;
        /*!
         * @brief Opens a file for writing
         */
        virtual bool open(const std::string& path, int bitsPerSample, int sampleRate, int numChannels) = 0;
        /*!
         * @brief Writes a chunk of data to file
         * @see getChunkSize
         */
        virtual bool write(const unsigned char* data, size_t length) = 0;
        /*!
         * @brief Writes a footer
         */
        virtual bool writeFooter() = 0;
        /*!
         * @brief Closes the file
         */
        virtual void close() = 0;
        /*!
         * @brief Checks if end of file
         */
        virtual bool eof() const = 0;
        /*!
         * @brief Returns current write position in bytes
         */
        virtual size_t getPos() const = 0;
        /*!
         * @brief Checks if file is open
         */
        bool isOpen() const;
        /*!
         * @brief Returns the target output size specified in function create()
         */
        size_t getSize() const;
        /*!
         * @brief Returns the sample rate
         */
        int getSampleRate() const;
        /*!
         * @brief Returns the number of channels
         */
        int getNumOfChannels() const;
        /*!
         * @brief Returns the bit depth of one sample
         */
        int getBitsPerSample() const;
        /*!
         * @brief Checks if a file is open
         */
        operator bool() const;

      protected:
        bool loaded;
        size_t size;
        int sampleRate;
        int channelCount;
        int bitsPerSample;
    };
};
#endif
