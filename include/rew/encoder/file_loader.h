#ifndef REW_FILE_LOADER_H
#define REW_FILE_LOADER_H

#include "../config.h"
#include "../common/output.h"
#include "../common/named_raw_file.h"

REW_NAMESPACE {
    struct FileName {
        std::string path;
        std::string name;
    };

    class REW_API FileLoader : public Input<FileName>, public Output<NamedRawFile> {
      public:
        FileLoader() = default;
        virtual ~FileLoader() = default;

        void process(const FileName* data, size_t length) override;
    };
}

#endif