# COM3001 - Matus Novak - Final Year Project

## Build Status

| System | Compilers | Continous Integration | Status |
| ------ | --------- | --------------------- | ------ |
| Windows | MinGW-w64 i686 | AppVeyor | [![Build status](https://ci.appveyor.com/api/projects/status/061ia3humbr6gvgt?svg=true)](https://ci.appveyor.com/project/matusnovak/com3001) |
| Windows | MinGW-w64 x86_64 | AppVeyor | [![Build status](https://ci.appveyor.com/api/projects/status/061ia3humbr6gvgt?svg=true)](https://ci.appveyor.com/project/matusnovak/com3001) |
| Windows | Visual Studio 2017 win32 | AppVeyor | [![Build status](https://ci.appveyor.com/api/projects/status/061ia3humbr6gvgt?svg=true)](https://ci.appveyor.com/project/matusnovak/com3001) |
| Windows | Visual Studio 2017 win64 | AppVeyor | [![Build status](https://ci.appveyor.com/api/projects/status/061ia3humbr6gvgt?svg=true)](https://ci.appveyor.com/project/matusnovak/com3001) |
| Linux | GCC 7.3.0 x64 | Travis CI | [![Build Status](https://travis-ci.org/matusnovak/mn00272-com3001-final-year-project.svg?branch=master)](https://travis-ci.org/matusnovak/mn00272-com3001-final-year-project) |
| OSX | Apple Clang LLVM version 8.0.0 | Travis CI | [![Build Status](https://travis-ci.org/matusnovak/mn00272-com3001-final-year-project.svg?branch=master)](https://travis-ci.org/matusnovak/mn00272-com3001-final-year-project) |
| Linux | GCC 5.5 x64 | Circle CI | [![CircleCI](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master.svg?style=svg)](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master) |
| Linux | GCC 6.4 x64 | Circle CI | [![CircleCI](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master.svg?style=svg)](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master) |
| Linux | GCC 7.3 x64 | Circle CI | [![CircleCI](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master.svg?style=svg)](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master) |
| Linux | GCC 8.2 x64 | Circle CI | [![CircleCI](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master.svg?style=svg)](https://circleci.com/gh/matusnovak/mn00272-com3001-final-year-project/tree/master) |

## Description

This is the source code of the Matus Novak's final year project for COM3001.
This project is a standalone application which can encode a small http based
website, encode it into an 48Khz audio, transmit over large distanced using
FM transmitter, and then decode it on the other side and serve the files
to the user.

## Compiling

You will need CMake 3.1 or newer with a relevant compiler (GCC, Clang, or Visual Studio 2017). The compiler must support C++17

```
cd com3001
mkdir build
cd build
cmake .. -G "Visual Studio 15 2017" -DCMAKE_BUILD_TYPE=MinSizeRel -DBUILD_TESTS=ON
cmake --build . --target ALL_BUILD --config MinSizeRel
```

## Testing

```
ctest --verbose -C "MinSizeRel"
```
