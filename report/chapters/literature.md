# 2. Literature Review

This chapter will discuss any relevant technologies and relevant encoding or decoding strategies explained.

## 2.1 Existing solutions

There are many solutions to the same problems we are facing in this project. However, most of them are hardware based. Because this project uses an existing FM radio to transmit the data, it would only make things complicated by introduced a hardware based middle-man. Additionally, the core idea of this project is not to create any new hardware, instead use an existing one. Therefore, we need to do signal processing on the software side. GNU Radio [@gnuradio] does offer a software solution for this project, however, the GNU Radio was primarily developed on Linux platform and the Windows or Mac OSX support is non-existing. There are some unofficial user made ports that do work on the Windows, but I was unable to find any that work on the Mac OSX. Moreover, the GNU Radio is a massive framework and in many cases requires a modern fast CPU [@gnuradioperformance]. For this project, I would only use a small fraction of what the GNU Radio provides. But, because of it size and the fact that it is oriented around Linux, I simply can not use it. While the GNU Radio can not be used for this project, the blocked based approach to create a system can be adopted.

![Screenshot of the block based interface of the GNU Radio software. Credits for the picture go to the GNU Radio wiki. [@gnuradio]](images/gnuradio.png)

There are other libraries that do offer similar functionality as the GNU Radio, but they often have heavily outdated packaging tools, or simply are not cross platform portable. There are similar libraries more targeted for a general audio/music processing but not necessarily targeted at modulation. One of such libraries is kfrlib [@kfr], but their licensing does not allows me to use their library freely. There are also many less known similar libraries, such as the stk [@stk], which is more targeted at audio synthesis, but is missing many core features that this projects needs in order to encode or decode the data from or into an audio.

In the end, I have chosen not to look further for a library that could provide the building blocks I need for this projects. Many of the libraries are either badly licensed (GPLv3), often use outdated building and packaging technologies (autoconf) and therefore not portable, or are simply too big to easily integrate into a modern C++ application. I have chosen not to pursue the solution through a third party algorithms, and instead decided to implement my own.

## 2.2 Modulation

There are many different methods of modulation, but only the digital modulation techniques are relevant. This section will cover the basics of different modulations and why FSK was chosen.

### 2.2.1 Phase-Shift Keying

The Phase Shift Keying [@psk] (PSK) is a type of modulation that changes the phase of the frequency based on the binary data. This is used in many different devices such as Bluetooth, or WLANs. The PSK uses a symbol based approach where the binary ones and zeros are not encoded exactly the same. The combination of bits creates a different unique phase. The PSK demodulator than has to map the unique sequence of phased waveform back to the original data. This would be extremely difficult to create and run purely through a software without any hardware acceleration.

### 2.2.2 Amplitude-Shift Keying

The Amplitude Shift Keying [@ask] (ASK) is a type of modulation that uses the height of the amplitude to encode (modulate) the digital signal. A similar modulation called AM [@am] was primarily used as an analog modulation for old public radio equipment that was later replaced by more reliable FM modulation. This type of modulation is very easy to implement, as we only need to keep track of the amplitude height. But we lose precision when the strength of the signal gets weaker.

### 2.2.3 Quadrature Amplitude Modulation

The Quadrature Amplitude Modulation [@qam] (QAM) is a type of modulation that carries two digital signals via amplitude shift keying [@ask]. The two digital signals are 90 degrees out of phase. This type of modulation is used mainly in telecommunications and WiFi based systems. This type of modulation is again difficult to decode through a custom software. For this project, we need something simpler that we can guarantee it will work.

### 2.3.4 On-Off Keying

The On-Off Keying [@onoff] is a modified type of amplitude shit keying modulation, in which the digital signal is modulated by presence or absence of the carrier signal. This type of modulation is used to transmit morse code. It is more prone to noise, therefore this type modulation will not be considered in this project.

### 2.3.5 Frequency Shift Keying

The Frequency Shift Keying [@fsk] (FSK) is a very popular method for encoding (modulating) the binary data. The modulation works by shifting the frequency of the carrier based on the binary input. This can be even further simplified by encoding the binary input via two different frequencies. This type of modulation is widely adopted in many different systems, such as the modem. Therefore, due to its popularity and simplicity, this type of modulation was used for this project. 

## 2.3 Demodulation

The FSK was chosen as the best candidate for the modulation of the signal. Therefore, a demodulation algorithm is needed. To demodulate the signal, we simply need to check for the carrier and the shifted frequency. In more simpler terms, we need to check for two frequencies. While this can be easily done through a hardware, it is slightly more complicated through the software. This type of modulation can be easily demodulated by already existing and popular algorithms such as Fast Fourier Transform [@fft] (FFT) or Goertzel algorithm [@goertzel].

The Fast Fourier Transform works by analyzing the audio and producing bins. In simple terms, each bin represents a frequency range, and the magnitude of the bin represents the occurrence of that range in the analyzed audio sequence. In this project, I have decided not to pursue FFT, due to a fact that it is computationally expensive in a real time systems. Instead, I have decided to go with a much simpler algorithm, a Goertzel algorithm. The Goertzel algorithm simply looks for a specific frequency and creates a single magnitude of an analyzed stream of data. The algorithm is easy to implement, and as shown in the Assmebly output in the design part of this document, the algorithm only needs few instructions. Compared to the FFT, the algorithm is faster and can even run in real time on-low end hardware. Because Goertzel analyzes only one specific frequency, we will need to run the algorithm twice, one to look for 0 bit, and one to look for a 1 bit.

