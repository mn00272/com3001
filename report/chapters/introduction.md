# 1. Introduction

The aim of this project is to create a fully working GUI application and CLI application to transmit static web pages over FM Radio. This project will additionally consists of a shared library, which can be used the user. Having a shared library is beneficial for the user, if they wish to modify or enhance the Decoder or the Encoder. But, the target end user of this application is not a developer nor an engineer. Thus, a GUI application will be provided. Additionally, a CLI program will be provided alongside the GUI. The CLI can be used on IoT devices such as Raspberry Pi or simply on machines that do not have GUI capabilities.

Because of the limitations of the carrier (audio), the throughput is limited to few bytes per second. Therefore this project is intended to be used with a simple static website with little to no embedded images. A great example is a Wikipedia article.

## 1.1 Background

There have been many natural disasters around the world, such as the hurricanes Maria and Irma in Puerto Rico in 2017. People have been left without electricity for months. Because of this, the information infrastructure collapsed, people had no access to the internet or media. The idea of this project is to provide an internet access in times of similar crisis. 

This project, Radio Enabled Web, can use already existing hardware, the FM radio, to receive the static website. The FM Radio is an old technology and easily accessible and cheap. On the other had, people do not have FM transmitter laying around in their houses. However, it is possible to construct low power FM transmitter from around 20 passive components and few transistors. The FM transmitter circuit can be found in some beginner electronics DIY books. Therefore, the idea is that it only needs one person to create the FM transmitter, with little to no electronics skills. Additionally, because FM radios are common domestic appliance, the users will only require the application and a computer.

This project can be used to distribute a static website to the public, when the infrastructure collapses. The contents of the websites can provide a basic guidelines for the public, such as a map of refugee camps or specific instructions given to the citizens from the government. 

## 1.2 Motivation

The idea of this project came to me when I was working with my brother on a hobby project with Software Defined Radio (SDR). We were interested in receiving raw weather images from a NOAA [@noaa] satellite orbiting around the Earth. The satellite constantly transmits the data, and anyone who listens with the proper hardware can receive these images. I was also interested in decentralized internet, an internet created by the users, that can survive a natural disaster. Thus, the FM radio internet idea was born. The initial idea was to have a specific 


Additionally, I wanted to create something unique that can benefit the public.

## 1.3 Aims & Objectives

This project aims to develop a GUI and CLI applications to encode or decode an audio into a static website. The following criteria are:

* Design a block based algorithm that can be structured into any kind of pipeline. This pipeline will be used to decode or encode the static website into an audio.
* Create a shared (dynamic) library that consists of pre-build set of blocks and the algorithm itself. Leave the implementation open so that it can be adjusted if needed to.
* Create continuous integration scripts so that the application and the library are tested for every new added feature. The CI also serves as a proof that the application is portable across different platforms.
* Create a CLI that uses the shared library. The CLI can be used on non GUI enabled computers to transmit or decode the data.
* Create a GUI that a user can use to encode or decode the static website.
* Create a HTTP server that is embedded into both the CLI and GUI applications so that the user can browse the encoded website with a web browser.
* Perform tests with real FM hardware and modify the implementation if needed to.

## 1.4 Success Criteria

The following are success criteria needed for this project to be marked as success:

* A working implementation of the encoding and decoding algorithm.
* Ability to decode the data from noisy transmission. A noise filter must be implemented.
* A working Command Line Interface.
* A working Graphical User Interface.
* Cross platform support for Windows, Linux, and Mac OSX.

## 1.5 Report Structure

* *1. Introduction* - This section provides a brief introduction and highlights the aims and objectives of this project.
* *2. Literature Review* - This section provides an investigation into already existing or similar technologies. Additionally, relevant findings are highlighted.
* *3. System Requirements* - This section includes a set of features of the system. This section also provides software and hardware specifications.
* *4. System Design and Implementation* - The main part of this report, which consists of detailed description of the algorithm implementation and C++ project structure as a whole. This section is also dedicated to a usability of continuous integration.
* *5. System Testing* - This entire section is dedicated to testing of the algorithm via FM transmitter and receiver. The section is also dedicated to performance testing on a number of different hardware.
* *6. Conclusion and Future Work* - This section highlights the success of this project. Additionally, any changes or improvements that have not been included in the final implementation are described in here.

