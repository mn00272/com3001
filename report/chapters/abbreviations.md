

# Abbreviations

| Abbreviation | Definition |
| ------------ | ---------- |
| CI           | Continuous Integration |
| REW          | Radio Enabled Web |
| SDR          | Software Defined Radio |
| GCC          | GNU C Compiler |
| LLVM         | Apple's Clang Low Level Virtual Machine |
| MSVC         | Microsoft Studio Visual C++ |
| x64          | The 64-bit system |
| amd64        | Also refers to the 64-bit system |
| x86_64       | The 64-bit version of the x86 architecture. Also refers to the 64-bit system |
| x32          | The 32-bit system |
| x86         | The 32-bit instruction system, refers to the 32-bit system |
| i686          | The 32-bit instruction set, part of the x86 family |
| win32        | Windows specific 32-bit architecture, used by MSVC |
| win64        | Windows specific 64-bit architecture, used by MSVC |

