

# Abstract

In this report we look at the design and implementation of small scale websites transmission over FM Radio. The project, "Radio Enabled Web", consists of a Encoder and a Decoder parts. These two applications, more precisely shared libraries, can encode a static HTML website into an audio file. This audio file can be then transmitted using conventional FM Transmitter, and then received on the other side. The received audio is then decoded via the Decoder application, and shown to the user via browser.  

This project, Radio Enabled Web, utilizes the FM radio transmitter and a receiver to transmit the contents of the website. However, it is possible to use any other carrier that uses audio, for example an amateur short-band radio. 

The Radio Enabled Web (REW), splits both the Decoder and Encoder into small standalone blocks, which can be used by the user. These blocks are C++ classes, and are purely optional. These blocks can be assembled into a custom pipeline, or integrated into any other viable language. The main part of this project is the GUI application(s) and Command Line Interfaces that can be used out of the box.

This report mainly consists of software design, implementation, and testing of the Radio Enabled Web.

