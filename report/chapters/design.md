# 4. System Design and Implementation

## 4.1 Approach

Before I have began the development, I have decided to create a strategic plan to approach this project. The following stages were set based on my literature review and past experiences with programming.

| Stage |
| ----- |
| Research different modulation techniques used in modern systems. |
| Create a working concept of modulating and demodulating a simple hello-world message. |
| Based on the concept, research all necessary third party libraries needed to create a working product, and add them to the project. | 
| Add continuos integration which will launch on every commit. |
| Add documentation generator and make it a part of the continuos integration. |
| Design an easy to use public API. |
| Create a decoder and encoder library. |
| Create a command line interface that exposes this library. |
| Create a graphical user interface that exposes this library. |
| Perform performance tests. |
| Perform tests with a real FM hardware. |

## 4.2 Software

The aim of this project is not only to transmit entire web pages through an FM radio, but also to provide the API for the user. The API, in form of a library, must be easily understood and well documented. The API will provide a basic set of building blocks for the user to arrange them in any desirable pipeline. These blocks, in a form of a pipeline, can be used to encode or decode the transmission, or further enhance the library. The aim is to provide a basic CLI (Command Line Interface) with pre-built pipeline that can be used to encode and then decode the transmission. Additionally, to provide a HTTP server for localhost, in order for the user to be able to browse the decoded transmission through a web browser of their choice.

### 4.2.1 Programming language and environment

C++ was chosen as the implementation language for this project. The main reason behind this decision was performance benefits of the compiled languages over the interpreted languages [@languagesbenchmark]. Another reason for the chosen language is that I have past experience working with this language in my own personal projects. C++ is a good choice for an open source library, as it can be integrated into interpreted language. For example, using pybind11 [@pybind11] to create python bindings, or by using Node [@nodejsaddons] to create a JavaScript library. C++ libraries can be also easily integrated into C# Net Framework via dllimport [@csharpdllimport], Ruby [@rubyextensions], and many more.

Additionally, the decoding part of this project needs to listen to the raw audio input of the user's machine. Therefore, it is essential that the application needs to run at real-time speeds. The use of C++ will greatly improve the efficiency of the decoding algorithm.

The C++ offers a standard library [@stdlib], which contains the basic building blocks for what the project needs. However, unlike Java and other high level languages, C++ does not offer out of the box networking library or filesystem library. The standard library, or STD for short, only provides us with language specific additions, such as strings, vectors (dynamic arrays), optionals, and the smart pointers. The smart pointers are a new addition to the C++ standard library version 11. It is a word of mouth that C or C++ applications are memory leaky, meaning, the application built in C or C++ will eventually leak memory and therefore use all of the available resources. However, these smart pointers are meant to help the programmer to reduce the likelihood of that happening. Normally when a new object is allocated, the standard way of doing it is through malloc (in C) or new operator (in C++). The memory needs to be then deleted via free (in C) or operator delete (in C++). The smart pointers encapsulate the raw pointers in an smart pointer object instance, and delete the memory when the instance fall out of scope. The project will have a heavy use of this feature.

The C++ comes with many versions, the standards, from C++98 (1998) up to C++20 (2020). The project will use the C++ 17 standard. The reason behind this chose is that the smart pointers have been first introduced in C++11 and later enhanced with additional features in C++14. Additionally, C++17 offers better metaprograming features than its predecessors. However, due to issues with the continuous integration and Mac OSX, the standard had to be lowered to C++ 14.

For the unit testing, the Catch2 [@catch2] framework was chosen. The framework does not produce any static or dynamic libraries, instead only a single header file is needed for a integration. This Catch2 unit testing framework also offers a real time expression expansion for the each assert macro. This is extremely beneficial when it comes to asserting comparisons between two values in the unit tests. For example, when testing if a `std::vector` contains a specific number of elements, we can call `REQUIRE(vec.size() == 10);`. If this assertion fails, the Catch2 will print it out as an error `8 == 10`. This also works for more complicated types such as strings.

### 4.2.2 Package Manager and build system

The C++ does not have an official package manager. There are many third party options such as Microsoft's vcpkg [@vcpkg] or conan [@conan]. The vcpkg is available on all three major platforms (Windows, Linux, and OSX) and offers packages in i686, amd64, or arm. This seems as a great choice for a package manager of this project, however there are some issues with it. First, the vcpkg does not offer a static binaries of the library on Linux and OSX, only dynamic binaries. This is also reversed on Windows where the the manager offers mainly static binaries with little or no choice of using dynamic binaries instead. Second, the vcpkg requires user's knowledge about complicated C++ building mechanism. This breaks the idea of this project, the entire project must be easy to understand and easily built by the user. The conan has been discarded due to a reason that the official documentation lacks the essentials, and the offer of the libraries is not comprehensive as need for this project. This leaves us with no other choice than to build the libraries ourselves.

Instead of using a package manager, the Git Submodules [@gitsubmodules] will be used. The Git submodules is a way of adding other Git repositories into our project. The libraries will be simply added as a git submodule and then fetched from the remote git origin into a subfolder of the project. The contents of those libraries will not be pushed to the main repository, instead only the submodule metadata needed to fetch the library. This slightly introduces a complexity of the building system, but allows us to chose a specific commit or compilation flags for each of the library we will need.

The C++ offers no standard way of building the application from sources. There are many different offers of tools when it comes to building and generating project files. One of the popular one is automake [@automake]. The automake was primarly designed to work only on Linux based systems, and has almost non-existing support for Windows without using a virtual machine or a port of Unix shell tools. Automake is used by many open source C++ libraries. However, due to Linux only support, it can not be used in this project. Instead, CMake [@cmake] will be used. CMake offers a simple intuitive scripting language, which can be used to identify source files, add include directories, and link libraries for the target executable (this project) or a library. CMake fulfils the needs, as it is easy to install, and the commands to build the project are easy to follow for a beginner.

Additionally, CMake offers an optional variables to be added into the building script. For example, it is possible to add `BUILD_EXAMPLES` or `BUILD_TESTS` conditional boolean variables, which a user can set to true or false. Using this variables, the project's CMake script file `CMakeLists.txt` will conditionally include examples and tests into the final generated project files. These project files can be anything from a simple Makefile, Visual Studio solution files, or even Mac XCode files. The CMake also offers a GUI which greatly reduces the complexity of the user experience.

![CMake GUI when building project files](images/C__Users_Matus_Documents_Projects_com3001_build.png)

### 4.2.3 Third party Libraries

The following third party libraries have been chosen for this project.

* argagg [@argagg] - This library provides a command line argument parsing. Needed for the command line interface part of this project. The library only needs a list of arguments and their description. Additionally, a help page is automatically generated using the description. 
* asio [@asio] - A library to create a basic http server. This server will be listening on the localhost and serving the decoded transmission to the user. The main part of the Asio is asynchronous networking, which will be needed when we want to separate decoding and serving via threads.
* dirent [@dirent] - A library to wrap Windows specific file system functions to be compatible with Unix functions. The original dirent is a library provided inside of every Linux based system. However due to the differences between Linux and Windows, this library provides a translation for Windows compilers between those two environments. This is only needed when building on Windows and will be enabled or disabled by CMake during configuration.
* zlib [@zlib] - This is a common compression library. It will be used to compress the source website into raw stream of bytes before being transformed into packets and transmitted. The zlib is well used compression library among many real world applications. Therefore, its documentation has been perfected, and easily understood, over the course of many years. 
* portaudio [@portaudio] - A cross platform audio input/output library. This library will be used by the Decoder to listen for transmission. Many Linux distributions already provides this library by default, which we need to take into consideration in our CMake scripts.
* libui [@libui] - A cross platform GUI library that uses the operating system native widgets. The library uses a Manson package manager, therefore in order to use this library, a libui-cmake [@libuicmake] is used. The libui-cmake is a CMake port of the original libui, which was written by myself. Originally, the Qt5 [@qt5] was meant to be used as the GUI layer. Qt5 is a cross platform GUI library developed by Nokia. This library provides a basic set of UI widgets, alongside with some more complex ones. Because of the complex and time consuming compilation steps needed by the Qt5, the libui was chosen as the replacement.

All of the third party libraries are included as a git submodule. Only the libraries that need compilation step, such as zlib, portaudio, and Qt5, will be included in the CMake generator. Any other libraries that are header only, meaning they do not require compilation step, are only included as a include directory for the compiler.

### 4.2.4 Integrated development environment

The following features were considered when choosing an IDE for the project:

| Feature needed  | Visual Studio | Visual Studio & ReSharper C++    | CLion | Code::Blocks | XCode |
| --------------- | ------------- | -------------------------------- | ----- | ------------ | ----- |
| Free            | Yes | No (€199) | Yes | Yes | Yes  |
| Multi platform  | No | No | Yes | Yes | No |
| Works with CMake | Yes | Yes | Yes     | Yes            | Yes |
| Supports C++14 or higher | Yes | Yes | Yes | No | Yes |
| Code completion | Yes | Yes | Yes | Yes | Yes |
| Differentiate between members and locals | No | Yes | Yes | No | No |
| Good debugger GUI | Yes | Yes | No | No | ? |
| Metaprogramming code completion | Yes | Yes | Yes | No | No |
| CMake integration | Yes | Yes | Yes | No | No |
| Catch2 unit tests integration | No | Yes | Yes | No | No |

Microsoft Visual Studio 2017 [@visualstudio] with ReSharper C++ [@resharper] has been chosen as the IDE for this project. However, due to the portability of CMake, it is possible to switch to any other IDE at any given time. The ReSharper C++ further enhances the Visual Studio intellisense with the following features:

* Able to recognize between local, member, and function parameter values. The values are visually highlighted with different colors.
* Able to run Catch2 unit tests directly from the Visual Studio.
* Better auto formating of code.
* Able to report missing constnes of functions and variables. For example when a function is missing an optional `const` keyword, the ReSharper C++ will suggest it as a warning if applicable.
* Able to auto generate missing member override functions.
* Able to generate missing function definitions.

![Microsoft Visual Studio Solution Explorer and project files](images/MSVC_Solution_Explorer.png)

## 4.3 Continuous Integration

The continuous integration is an essential part when building C++ applications or libraries. Due to a reason that the output binary (either an executable or a dynamic library) is platform dependent, we will need to test the compilation and unit tests for all of the platforms we wish to distribute to. The continuous integration is an online service which listens to changes within the repository, and launches a build for every introduced change. This change can be a new commit, new tag, or a pull request. With a use of this service, we can ensure that the code written is platform independent. In this project, the three major platforms have been included: Windows, Linux, and OSX. Additionally, the i686 and amd64 versions need to be included in the Windows testing. The status of all of the continuous integration is listed in the README file of the repository. 

![Status of continuous integration listed in the README file of the project on GitLab.](images/Build_Status.png)

### 4.3.1 AppVeyor

The AppVeyor [@appveyor] is a continuous integration that offers building software on Windows virtual machines. The MinGW and MSVC compilers (both in amd64 and i686 settings) have been included in the building script. During the build, the latest commit is fetched from the repository, and using a custom script provided, the project is built and the output packed into a single artifact zip file. The artifact contains the binary distribution of the command line interface, alongside with the dynamic library and header files.

Due to the CRT [@iteratordebug] in MSVC compiler, the build system had to be adjusted for Visual Studio 2017. While the output binary for MinGW, or Unix system is built in the release mode only. The Visual Studio build needs to output both the debug and the release binaries. The debug mode produces a non optimized library with debug symbols attached. The release mode produces optimized and minimized library with no debug symbols. These debug symbols are only needed for the development of an application that used this library. However, due to the CRT in MSVC compiler, the CRT library on Windows is not compatible if the CRT used in the built binary does not match the debug level used. For example, if the user decides to download the release version, and tries to use it in their own custom application built in debug mode, it will cause compilation error. Therefore, both debug and release versions must be published. This does not apply for MinGW, GCC, or Clang compiler. 

Only Visual Studio 2017 in Win32 and Win64 mode and MinGW 6.3.0 in i686 and x86_64 mode are used in this continuous integration.

![Example of AppVeyor build](images/AppVeyor.png)

### 4.3.2 Travis CI

The Travis CI [@travisci] is a continuous integration that offers building software on both Linux and Mac OSX. Similarly, as in AppVeyor, the build is launched on every new commit pushed to the repository. The Linux GCC 7.3.0 in x64 mode and Apple Clang LLVM 8.0.0 in x64 mode is used during compilation.

![Example of Travis CI build](images/TravisCI.png)

### 4.3.3 Circle CI

The Circle CI [@circleci] is a continuous integration that only offers building software on Linux. As in both AppVeyor and Travis CI, The Circle CI launches the build on every new commit. For the purposes of this project, only Linux GCC 5.5, 6.4, 7.3, and 8.2 in x64 mode is used.

After a successful build of the software, on various version of GCC, the documentation building process is run in two separate jobs. During this, the first job "build_docs" will run Doxygen [@doxygen] which will parse the header files and produces an XML output. This XML output is further processed by DoxyBook [@doxybook] and exported as Markdown into a temporary folder. In the second job, the final job, the Markdown documentation is converted into HTML static pages via VuePress [@vuepress], and finally pushed into the `gh-pages` branch on the GitHub mirrored repository. These HTML files can be browsed online by accessing the repository.

![Example of Circle CI build](images/CircleCI.png)

### 4.3.4 Distribution of binary artifacts

When the artifacts are built on the continuous integrations, they are pushed to the BinTray [@bintray] into a custom generic repository. This repository is no more than a simple file storage with versioning labelling system. The reason behind BinTray is that if we wish to distribute the library and the header files to the user, we need to make it available to them. If this was a Java application, we could distribute the library via Maven. However, this is not possible with C++, as there is no official package distribution software to do it for us. We could host the artifacts on University's GitLab, but due to the problem with non-working pipelines, I have decided not to try this feature as it may be a waste of time. Instead, we could use the mirror repository on GitHub to host the artifacts. This can work until we reach the 1GB of used space. After this limit is reached, a friendly warning is sent to the owner forcing them to release some space. This has lead me to BinTray which allows up to 10GB of space and up to 1TB of download traffic, more than needed for these artifacts.

When the artifact is built, it is compressed into a zip file. The versioning of these zip files is the following this schema: `rew-<version>-<machine>-<compiler>.zip`, where the `<version>` is the version described by the git, the tag, and the `<machine>-<compiler>` are retrieved from the compiler itself. This versioning is useful for the developer, if they wish to integrate this software in their own application. However, for a common user, this is a highly confusing naming scheme. It would be more beneficial to host a website, for example a product website, with three simple options (Windows, Linux, and OSX) for downloading the software. This product website does not exist, but could be part of the future of this project.

![Example of all artifacts collected on BinTray](images/BinTray.png)

### 4.3.5 Documentation

The documentation is implemented via Doxygen [@doxygen] and a custom Doxygen xml parser DoxyBook [@doxybook]. The Doxygen parses the header files of this project and outputs an XML structure representing the classes, namespaces, and functions. This XML structure is then processed by DoxyBook, an open source command line utility created and maintained by myself. This utility converts the XML into Markdown files. The markdown files are then processed by VuePress [@vuepress]. The VuePress is a Vue.js [@vue] based platform for creating blogs and static page documentation. The reason for this choice is that the files generated can be browsed offline without a use of a HTTP server. Additionally, the generated static HTML files can additionally be uploaded to the GitHub's gh-pages. The gh-pages is a name of a branch of a repository. If the branch is populated with HTML files, the GitHub will then automatically publish them online, without a use of a server. These files, the documentation, can be then viewed by anyone.

\pagebreak

~~~{.cpp caption="Example of Doxygen comments within the header files"}
REW_NAMESPACE{
    /*!
     * @brief WAV file reader
     * @ingroup decoder
     */
    class REW_API WavReader: public AudioReader {
    public:
        [...]
        /*!
         * Opens a file for reading
         * @param path Relative or absolute path to the file
         * @return True if opening the file did not generate an error
         */
        bool open(const std::string& path) override;
        [...]
    };
}
~~~

![Example of the VuePress documentation generated from the Doxygen and processed by the DoxyBook.](images/vuepress_example.png)

## 4.4 System Implementation

### 4.4.1 System structure

The system was split into two separate components: the Encoder and the Decoder. The Encoder will be designed to only accept a folder containing all website files, and outputs a raw stream of samples that can be saved into an audio file. These samples, a PCM stream, is a stream of 32bit floating point values representing the entire encoded website as an audio. The samples need to be normalized values between -1.0f and +1.0f, so that they have the maximum gain possible, without causing audio clipping. Any values beyond or under +/-1.0f will be wrapped over, causing glitches - clipping. Any audio PCM sample of 0.0f is considered to be infinitely silent, while +/-1.0f is considered highest possible gain. The Decoder needs to accept these PCM samples, either through an audio input or from an audio file. The Decoder will then process these samples and extracts the compressed stream of data that holds the website contents.

Command Line Interface will be provided alongside of the encoder and decoder libraries, so that the user can use these application without integrating the library in their own software. However, our target demographic may not have the technical skills to operate the CLI. Therefore, a GUI application, one for the Encoder and one for the Decoder, will be part of the project and binary distribution provided for the user. This GUI will be implemented via Qt5 [@qt5], a cross platform library developed by Nokia. GTK+ [@gtk] was considered at the beginning, however due to incompatibility on newer Mac OSX versions, the GTK will not correctly work unless the user enables the X11 manually. Therefore, QT5 for its wide compatibility, was chosen as the GUI framework for this project. The binary distribution of Qt5 will be distributed alongside the GUI binary distribution of the Encoder and the Decoder. This is necessary to ensure that the target user does not need to install Qt5 globally into their machine.

All of the dependencies and target binaries are defined inside of the CMakeLists.txt files, one file for each Encoder or Decoder component.

![Dependency diagram of the entire project structure.](images/dependency-diagram.png){ height=50% }

### 4.4.2 API Design

Because this project is meant to be open source, it must have a consistent API that is intuitive and easy to integrate to a custom software. Additionally, the algorithm must be adjustable. To satisfy these requirements, the algorithm was split into multiple blocks. Each block, using its own specific input data type, will consume the input and outputs it either in the same data type, or a new data type. Each block only does a single specific work and is completely standalone. The blocks can be arranged together to form a pipeline (can be also described as flowgraph). This kind of implementation was inspired by the GNU Radio [@gnuradio].

Each block is a derived class of the Input and the Output base classes. However, not every block needs to inherit both Input and Output classes. For example, a file audio source block, does not need to have an input, it only produces an output. An abstract Input class contains a virtual method `process()`. This method is called by the abstract Output class in order to forward the data. Due to the strict static typing of C++, the combination of Input and Output derived classes will always match.

\pagebreak

~~~{.cpp caption="The abstract Input with omitted code"}
template<typename T>
class Input {
public:
    [...]
    virtual void process(const T* data, size_t length) = 0;
    [...]
};
~~~

~~~{.cpp caption="The abstract Output with omitted code"}
template<typename T>
class Output {
public:
    void connect(Input<T>* input) {
        [...]
        inputs.push_back(input);
    }

    void forward(const T* data, const size_t length) {
        for (auto& input : inputs)
            input->process(data, length);
    }
private:
    std::list<Input<T>*> inputs;
};
~~~

Using this idea, the following blocks have been implemented as listed below:

* `Clamp<typename I, typename O>` - A block that clamps the input based on a specific minimum and maximum variables. This is necessary in order to ensure that the algorithm produces samples in -1.0f and +1.0f range. At the same time, the output is converted into a different data type, specified by the template parameters.
* `Merge<typename T, size_t N>` - A block that can merge N number of streams into a single stream for further processing. During this, the data is not converted to another data type, instead it is re-used. During this process, the data is temporarily stored within the block. The data is forwarded once the length of all input paths matches, and then released and the input buffer reset.
* `Multiply<typename T>` - This block simply multiplies the input stream by a fixed value. No data type conversion happens.
* `AudioSource` - (Decoder only) An abstract block that is used to produce an output of 32bit float PCM samples. This block needs to be further implemented. An implementation of abstract `AudioReader` class, for reading audio files, and abstract `AudioListener` class, for listening to the microphone audio input on the machine. These two classes are then further implemented as concrete `WavAudioReader` and `PhysicalAudioListener` classes. `WavAudioReader` is meant to read audio samples from a WAV file, while the `PhysicalAudioListener` reads the audio samples from a physical input device.
* `FirFiler` - (Decoder only) This block is meant to filter out noise. It works by listening to a specific frequency from a raw stream of PCM samples.
* `Goertzel` - (Decoder only) Filters the input stream of 32bit float PCM samples and outputs a magnitude based on the frequency being tracked. The block needs to be initialized with a specific window size. This window size greatly changes the output magnitude and the accuracy of the algorithm as a whole.
* `ByteAssembler` - (Decoder only) Takes a raw stream of Goertzel processed data and assembles a byte.
* `PacketAssembler` - (Decoder only) Takes a raw stream of bytes and converts it into a Packet. An 8bit CRC is applied before the packet is constructed in order to verify its integrity.
* `FileAssembler` - (Decoder only) Takes a stream of packets and assembles the output file, only if all of the packets have been provided. This block caches the packets based on a file ID, and only outputs a file if all of the packets are provided and can be decompressed.
* `FrameSync` - (Decoder only) Waits for a frame synchronization and forwards a specific number of samples further down the pipeline. The amount of samples to forward is determined by the size of a single packet. This specific amount is necessary in order to ensure that the samples will or will not contain the packet we need to decode.
* `AudioSink` - (Encoder only) Similarly as in `AudioSource` except its role is reversed. An abstract class that is meant to be derived into a custom implementation in order to output the samples into a WAV file or a speaker output on the machine. This class is further specialized as abstract `AudioWriter`, and a block `WavAudioWriter` has been provided as the concrete class. Additionally, `PhysicalAudioOutput` is provided as well.
* `FileLoader` - (Encoder only) Loads a file into a memory and appends the file name at the start of the byte array representing the contents. Simply forwards the contents further down the connected blocks.
* `PacketGenerator` - (Encoder only) Accepts an in-memory file, and using a zlib compression, the data is compressed and packets are generated.
* `SamplesGenerator` - (Encoder only) Constructs samples using a raw stream of bytes. In this case, the stream of bytes are the packets from the `PacketGenerator`.

More custom blocks can be implemented by the user using the abstract Input and Output classes. The new blocks will automatically be compatible with the pre-existing blocks as listed above.

The detailed explanation behind the functionality of these blocks is explained in the chapters further.

### 4.4.3 Fast Fourier Transform and Goertzel Algorithm

During the prototyping phase, a Fast Fourier Transform (FFT) was considered in order to listen for the two specific frequencies representing the binary stream of data. After a lot of research done on this topic, a decision was made that using FFT was computationally heavy for a simple work we need. In this project, we are only looking at the two specific frequencies, representing high and low bits, and a complete silence, representing no transmission. In the end, the Goertzel [@goertzel] algorithm was used to perform this process. While the FFT produces buckets for a range of frequencies, the Goertzel only produces a magnitude of a specific frequency we are trying to listen to. If no frequency is detected, the output of the algorithm is close or equal to zero. The Goertzel can be sumarized in the following code:

\pagebreak

~~~{.cpp caption="Processing samples via Goertzel to produce a magnitude."}
void processSamples(const float* samples, const size_t length) {
    // Q0, Q1, Q2, and coeff are initialized based on the frequency and sample rate.
    // The initialization happens only once when the program starts.
    // Therefore, it is excluded in here.
    auto src = samples;
    while (src != samples + length) {
        const auto Q0 = coeff * Q1 - Q2 + *src++;
        Q2 = Q1;
        Q1 = Q0;
    }
}
~~~

The same algorithm can be represented in less than 23 assembly instructions. Not only the Goertzel is faster than the FFT, but it also solves the same problem we have.

~~~{.S caption="Processing samples via Goertzel represented as assembly. Compiled via GCC 8.3 amd64 with -O2."}
processSamples(float const*, unsigned long):
        lea     rax, [rdi+rsi*4]
        movss   xmm3, DWORD PTR coeff[rip]
        movss   xmm1, DWORD PTR Q1[rip]
        movss   xmm2, DWORD PTR Q2[rip]
        cmp     rdi, rax
        jne     .L4
        jmp     .L1
.L7:
        movaps  xmm1, xmm0
.L4:
        movaps  xmm0, xmm3
        add     rdi, 4
        mulss   xmm0, xmm1
        subss   xmm0, xmm2
        addss   xmm0, DWORD PTR [rdi-4]
        movaps  xmm2, xmm1
        movss   DWORD PTR Q2[rip], xmm1
        movss   DWORD PTR Q1[rip], xmm0
        cmp     rdi, rax
        jne     .L7
.L1:
        ret
~~~

### 4.4.4 Packet design and the Algorithm

In order to transmit the files, or any large data, over an FM radio, we need to convert the files into an audio. There are multiple ways how to do this. For this project, Frequency Shift Keying (FSK) [@fsk] will be used. The FSK encodes the stream of bits into two different frequencies. For the purpose of the naming simplicity, the frequency to encode the low bit will be called low frequency, and the frequency to encode the high bit will be called high frequency. To implement this, a simple `ToneSynth` class has been constructed. Combined with the `SamplesGenerator` block, it creates PCM samples from a raw stream of bytes. Each byte is encoded into a fixed number of samples. This way we ensure that the Decoder can listen to a fix sized block of samples, in order to decode it back into a byte. Additionally, frequencies 2400hz and 4800hz have been chosen for the low and high frequencies. Each byte is additionally delimited by a silence with a width of a single encoded bit. This way, we can easily calculate the expected number of samples for the entire input. The choice of having the high frequency the double of the low frequency, is to ensure that the start and the end of the sine wave matches. This increases the accuracy of the Goertzel algorithm.

![UTF-8 string "Hello" encoded as a PCM samples using Frequency Shift Keying.](images/hello_pcm.png)

Next, to decode the audio, and make sense of the PCM samples, we apply the Goertzel algorithm. The Goertzel algorithm works in a fixed number of samples, a window. The size of the Goertzel window greatly changes the magnitude of the output. After many attempts and tests made, a window size of a length of a single encoded bit has been chosen. Using sample rate of 48Khz and a single bit length of 1.25 millisecond, the window size is 60 samples. This window also slides per single sample. Using this method I have been able to produce the best bit detection results.

![UTF-8 string "Hello" decoded via Goertzel from PCM samples. Orange magnitude representing the magnitude of the high frequency and the blue magnitude representing the low frequency.](images/hello_goertzel.png)

After the Goertzel algorithm is applied, we get a raw stream of two different magnitudes. These two magnitudes are further multiplied by a fraction and clamped to create a stream of zeros or ones. The clamping is simply a process of testing whether the input value exceeds a threshold. If the threshold is exceeded, the value 1 is applied, if the threshold is lower, the value 0 is applied.

![UTF-8 string "Hello" decoded via Goertzel and clamped from PCM samples. Orange magnitude representing the magnitude of the high frequency and the blue magnitude representing the low frequency. The low magnitude is mirrored vertically in order to simplify the visualization.](images/hello_clamp.png)

To find the start and the end of the encoded byte, we simply look for the empty areas with zero magnitude.

![UTF-8 string "Hello" decoded via Goertzel and clamped from PCM samples. The delimiter between the bytes is highlighted.](images/hello_clamp_arrows.png)

When we find the start and the end of the magnitude sequence, we can test for the length of the samples. If the sequence does not match the expected length, it is discarded. However, not exact amount of samples may be found, while still being a valid sequence of samples. For example, an audio input of the user's machine may record in 47.9Khz due to lower-quality hardware, while the application is configured in 48Khz. To fix this, a tiny margin has been applied during the length verification. When the byte is constructed, it is simply forwarded down the block pipeline for further processing.

The process of detecting wether the bits in the byte are low or high, we simply count the incoming magnitudes. These magnitudes come from a `Merge` block. This block outputs N number of streams. In our case, we are listening to two frequencies, therefore two streams. This stream is a clamped value of the Goertzel algorithm. Because it is clamped, we either get 1 or 0. To check wether we have a high or low bit, we simply divide the entire byte sequence into 8 equal buckets. Within each of the bucket, we count the number of `1` within each of the high or low stream. The higher count wins and we get a high or low bit.

However, to encode a large number of data, we need to split the source data into a fixed sized packets. If we transmit the entire large file, the user's receiver may be interrupted, or simply temporarily blocked due to ambient noise, and the entire file lost. Instead, we use packets. Each packet has a simple fixed sized header that identifies the file ID and a packet sequence number. The packet structure then follows by a raw stream of data, in our case a 32 byte chunk, and finally by a single byte representing the 8-bit CRC of the entire packet. 

![Binary representation of the Packet structure.](images/packet_bytes.png)

\pagebreak

Because of how C++ compilers lay out the structure in the memory, we can ensure that the order of the fields defined in the Packet structure matches the one in the figure above.

~~~{.cpp caption="C++ representation of the packet."}
struct Packet {
    [...]
    uint16_t fileIndex = 0;
    uint16_t packetIndex = 0;
    uint8_t data[PACKET_DATA_LENGTH] = { 0 }; // PACKET_DATA_LENGTH = 32
    uint8_t crc = 0;
    [...]
};
~~~

When we receive the packet, the algorithm, more specifically the `FileAssembler` block, will cache the received packets until the final packet has been received. To check whether we have the final packet, the `FileAssembler` block simply searches for the `{ 0xff, 0xff, 0x00, 0x00 }` sequence. This sequence is the footer of the zlib [@zlib] stream. Before the packets are constructed on the Encoder side, the data is compressed through zlib and the footer is appended automatically. We can use this to detect the end of the stream, and therefore the end of the packet sequence. To decompress the data, we simply feed the 32 byte chunks of data into the zlib deflator in the correct order. The CRC of the packet, and the zlib deflating process, should ensure that the decoded data is valid.

However, to mark the start and the end of the packet, we need to include a synchronization. To implement this, we need to consider the following scenarios:

* The receiver, the FM radio, on the user's side may be interrupted by a noise or a hardware defect.
* The transmission may be blocked by machine's interrupts (the machine, for example a computer, that transmits the data).
* The receiver, the FM radio, may start receiving data in the middle of the packet. (The user has plugged in the audio input in the middle of the transmission).
* The Decoder may be blocked by the user's machine interrupts or other tasks.

The Decoder is not meant to run on a dedicated hardware, therefore interruption of the audio input may happen at any time. To ensure that we have the beginning of the packet, we introduce the synchronization bytes. These bytes is a sequence of bytes that are transmitted at the start of each packet. The idea comes from NOAA satellites [@noaa]. More precisely, the synchronization of the weather images sent by the satellites [@fmpicture]. 

![Example of the transmission sent by the NOAA satellites. The synchronization is seen on the right as a quick transition between black and white pixels. The credits for the image belong to Mike Richards from his blog post "137MHz Polar Orbiting Weather Satellites" [@noaadecoding]](images/noaa.png)

In this project, we use the same idea. The beginning of the packet is marked with a synchronization sequence of `{0xff, 0x00, 0xff, 0x00}` bytes. This was later changed to sequence `{0xf0, 0xf0, 0xf0, 0xf0}`. In the NOAA satellites, the synchronization sequence also marks the speed at which the data is transmitted, the baudrate. However due to the time constraints of this project, this was not implemented. Instead, the receiver must know the baudrate beforehand. This is something that could be added in the future.

When the synchronization sequence is detected, the Decoder then starts to buffer fixed number of samples before forwarding it into a similar algorithm (explained in the section further). This fixed number of samples is calculated based on the baudrate (width of a single bit in number of samples), the sample rate of the audio input (usually 48Khz), and the spacing (the delimiter) between the bytes. When the samples are buffered, they are forwarded into the next stage, which follors the same algorithm with some minor modifications, and packets are generated. If the packet is generated, it is verified for its integrity by calculating the CRC and comparing the value with the CRC value appended in the packet.

![Packet visualized in Adobe Audition. Because this is start of the transmission (the exported audio file), the file ID starts with zero. Additionally, this is the first packet, therefore the sequence ID is also zero.](images/packet_example_audition.png)

### 4.4.5 Block pipeline as the Algorithm

The idea behind the algorithm is simple: accept a raw stream of 32bit float PCM samples and outputs a file. For the Encoder, the algorithm is simplified and reversed. Initially the algorithm, using the Input/Output blocks, was constructed in the following way:

![The algorithm (pipeline) that proved to be unsuccessful. Each color represents a different data type produced based on the block.](images/piepline_old.png)

This pipeline as shown above proved to be unsuccessful due to a reason that the packet synchronization breaks down. The reason is that the number of data that enters a block may not match the number of data that comes out of it. For example, when we are converting Goertzel decoded output of raw ones and zeros, it is converted into bytes. This is not a 1:1 conversion. Therefore, the track of time is lost. The algorithm can detect the synchronization bytes, however if the data failed to decode, the system will still have partial data that can affect the next packet. For example, it is possible that the Decoder will receive only the first three bytes of the synchronization sequence, the last byte fails to decode, and then the first byte of the file ID happens to be `0xf0`. The Decoder will mark that as the start of the packet, and the next 32+5 bytes are used, whether they form a real packet or a random sequence of mixed data. 

This is a problem that we can solve by adding a frame synchronization (`FrameSync` class) block. This block internally implements the same pipeline as above, however the track of the time is added into the bytes decoded. The track of time is measured in the number of samples and not via the system clock. The reason for this is that the algorithm does not run in a real time environment, unless a dedicated hardware is used. It is possible for the Decoder to be temporarily interrupted by the system process scheduler, therefore the system time would not match, even if we have received the bytes in correct distance apart. Additionally, the raw samples from the audio input device may not come at exact intervals. It all depends on the OS process scheduler. The `FrameSync` block remembers the last N bytes. This N matches the number of bytes in the expected synchronization sequence. This sequence must be the same for the Encoder and the Decoder, otherwise no packets would be detected. If the received N bytes matches the expected sequence, the `FrameSync` block starts to buffer the next X number of samples. Additionally, the buffered bytes also need to verified for the distance at which the bytes have been received. The distance is the number of samples it takes to encode a single byte. If the distance is higher, this simply means the bytes received are not neighbours, and the buffer is reset. As mentioned previously, the X number (in the internal buffer for forwarding packet samples) is the amount of samples that is expected to hold the entire packet. This is calculated based on the expected width of a single byte, and the delimiter between the bytes. When the buffer is filled, it is then forwarded into the next pipeline (see figure below). If the packet is decoded, it is added into the `FileAssembler`. If no packet is decoded, for example due to noise or transmission errors, the pipeline is reset and no packet is added. When the pipeline is reset, it simply listens for the next synchronization sequence.

![The algorithm (pipeline) with functional synchronization. Each color represents a different data type produced based on the block.](images/piepline_new.png)

This pipeline is provided in the dynamic library of the Decoder. However, the user, if they wish to, is able to reconstruct this pipeline in their own specific way. The user is also able to further connect their own custom blocks, for example to forward the data into their own software for further processing. This is an optional feature and not the main usage of this algorithm. The main usage is simply to encode or decode a static websites. The target user of this application may not have programming skills to implement this. This block design is purely an optional feature.

### 4.4.6 Command Line Interface

The command line interface (CLI) allows the user to use the Encoder or Decoder in case the user has no access to a machine with a GUI capabilities. For example, this can be useful when the user wants to run the application on Raspberry Pi or similar Internet of Things device. These devices usually do not have GUI capabilities (the X11 is disabled), and are purely terminal based.

The Encoder CLI is a simple application that accepts an input folder and outputs a WAV file. It is also possible to automatically stream the encoded data, as audio, into the default audio output device on the user's machine. The application will transmit the data in a loop forever until the user terminates the application. For example, via system `SIGTERM` signal, or simply by pressing Ctrl+C on the keyboard. The Decoder CLI is essentially a clone of the Encoder CLI application, except it utilizes the Decoder pipeline as described in the previous section. Additionally, the Decoder CLI can start a HTTP server on localhost, so that the user is able to browse the website once decoded.

\pagebreak

~~~{.txt caption="Help output of the Encoder and Decoder CLI"}
build/encoder-cli -h
    -h, --help
        Shows this help message
    -i, --input
        The input folder
    -o, --output
        The output WAV file
    -s, --stream
        Stream the audio directly to the audio output device
    --stream-devices
        Print out all available stream devices

build/decoder-cli -h
    -h, --help
        Shows this help message
    -i, --input
        The input WAV file
    -l, --listen
        Listen to a specific device
    --listen-devices
        Print out all available listen devices
    -o, --output
        The output folder
    -s, --serve
        Serve the files over http server
~~~

The decoder CLI can be run with `--serve` argument, which will serve the decoded files via a http server on `http://localhost:8080`. It is possible to run the http server on the default http port 80, but theis requies raised privileges to do so [@privilegedports], because all ports below 1024 are considered special and do not allow the user to use them. Due to this limitation, port 8080, which is considered as an alternative port for http, was used to serve the files. The server in the decoder was constructed via asio [@asio] library, and runs on a single thread. Using a single thread to serve the files is good enough, because each request only has to serve a single file, by copying a cached file into the response buffer. It is possible to run the server on multiple threads at the same time thanks to the asynchronous design of the asio library. The only change needed would be to call the asio's io service from multiple threads [@asioioservice]. The server is a simple http server that receives a decoded file, stores it in a key-value pairs, and copies the contents of the key-value pair once the http requests comes in. The key-value store contains a key, which is the filename with a relative path, and a value, which is the contents of that file. The file extension is used to determine what content type header to send back to the client. This content type is necessary, so that the browser (the client) knows what to do with this file. For example, serving a simple html file needs to be sent back as `text/html`. Otherwise the file won't be rendered by the server and parsed as a simple text file, the markup is lost.

### 4.4.7 Graphical User Interface

The decoder and the encoder also comes with a graphical user interface executables, named `decoder-gui` and `encoder-gui`. These executables, an application, are using native GUI widgets to do the same functionality as the CLI versions of the encoder and the decoder. These GUI applications are using libui-cmake [@libuicmake], which is a CMake port of the original libui [@libui], created by myself as an open source project. Originally, the plan was to use Qt5 [@qt5] to create the GUI, but after using the Qt5 in the continous integration many problems started to appear. The problem with using Qt5 is adding it into a platform independent application, such as this project. There are pre-built windows version of Qt5 that can be downloaded automatically by CMake, but the linux version must be compiled manually, or downloaded through the Linux distro specific package manager. Because this project's dependencies are all managed through git submodules, I have decided to include Qt5 through a git repository, and compile it as any of the other dependencies. This worked at the beginning, but there were many issues compiling Qt5 on windows (such as library compatibility with Qt5 dependencies) and the documentation was very unhelpful. Instead of using Qt5 I have decided to move to libui [@libui]. This library offers a very limited susbet of the Qt5's widgets, but it is enough for this project. In this project, we only need simple buttons, checkboxes, combo lists, and selecting folders and files. Qt5 would be an overkill for this kind of application. The libui can be easily compiled through my port libui-cmake [@libuicmake] and is platform independent. 

![The encoder and the decoder GUI running on Manjaro Linux.](images/decoder-encoder-gui.png)
 
### 4.4.8 Unit Testing

The unit tests are compiled into an executable and run through ctest, an application that is part of the CMake. The CMake scripts (the `CMakeLists.txt` file) specifies what executables to test and what arguments to pass into them. The ctest will automatically pick up these configurations and will run all of the executables and simply reports back the number of executables that have returned non zero code. A code of zero, as by default on all operating systems, specifies that the application has run successfully. Any other number is considered an error. These unit tests are built and run on all three common platforms (Windows, Linux, and OSX) via the continous integration. The tests contain a simple and a more complex scenarios, such as compressing and decompressing a "Hello World" string, or encoding more complex structures and displaying them as graphs that are also used in this report. These tests were essential part of building this project, as it ensures that code written is indeed platform independent.

