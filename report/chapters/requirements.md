# 3. System Requirements

The functional and non-functional requirements were completed after the development and testing of the project.

## 3.1 Functional Requirements

The following table is a list of functional requirements of this project:

| Requirement | Description | Completed |
| ----------- | ----------- | --------- |
| Encode a buffer | Encode a raw stream of bytes into packets and export them as an audio file | Yes |
| Encode an entire folder | Encode for example an entire folder consisting of an website and export as an audio file | Yes |
| Decode a buffer | Decode a stream of audio back into a raw stream of bytes | Yes |
| Decode an entire website | Decode a stream of audio back into a fully functioning static website | Yes |
| Serve decode files | Serve the decoded files over http://localhost:8080 | Yes |
| Public API | Public API so that anyone can use this project as a library | Yes |
| Noise reduction filters | Implement different noise reduction algorithms | No |
| Command Line Interface | Implement CLI that can be used by the user via Bash or CMD | Yes |
| Graphical User Interface | Implement GUI that can be used by the user via the desktop | Yes |

All of the requirements above have been met except for a noise reduction filter. I have not found an available open source library that would have a correct licensing requirements and available across platforms. I have tried to create my own noise filters but I was unsuccessful. I have decided not to dedicate large amount of my time for this feature as there were other more necessary features to implement.

## 3.2 Non-Functional Requirements

The following table is a list of non-functional requirements of this project:

| Requirement | Description | Completed |
| ----------- | ----------- | --------- |
| Documentation | Provide documentation of the public API hosted publicly on GitHub pages | Yes |
| Distribution | Provide distribution of the library and the CLI with GUI executables on all three platforms via Bintray | Yes |
| Home page | Create a home page for the product | No |
| Performance | Ensure that the algorithm used in this project does not require a powerful machine and scales efficiently | Yes |
| Availability | The decoded transmission must be accessible by the user by accessing a destination folder or via the web browser | Yes |

All of the above non functional requirements were met except for a home page. The home page would require me to pay monthly hosting fees and is something that can be done in the future work of this project. The home page is not critical at all, so I have moved my focus on other requirements.

## 3.3 Software and Hardware Specifications

The project was written in C++14 and written on both Windows (via Visual Studio) and on Linux (via Visual Studio Code). The code is platform independent and tested on all three common operating systems: Windows, Linux (Ubuntu), and Mac OSX. The development was primarily done on a Windows machine, however manual tests needed to be done on both Linux and Mac OSX as well. For the primary development, the following workstation was used:

* Windows NT 10
* AMD Threadripper 1950X @ 4.5GHz
* 32GB of RAM

However, to compile the code, a powerful workstation is not needed at all. The code was compiled and tested on a t2.micro AWS EC2 machine, which uses 0.5 CPU and only 480MB of RAM.

For the manual testing of the Linux and the Mac OSX, I have decided to use virtual machines, instead of triple booting into the same workstation. For the Linux VM I have used a Kubuntu distribution 17.04 with GCC 7, and for the Mac OSX VM I have used OSX El Capitan with XCode 8. There were some adjustment needed to make the Mac OSX work under an AMD CPU, but using two operating systems at the same time has worked without an issue, and has drastically improved my development speed.

## 3.4 Feasibility

### 3.4.1 Legal Feasibility

This project has no legal issues except for testing with a real FM hardware. Depending on the country, using a FM transmitter may be legal up to a specific distance or power output. For the tests, I have specifically used a low power FM transmitter so that I comply the country rules. The tests were conducted at my home in Slovakia where the legal rules for FM transmission are slightly less strict that the ones in the UK. Therefore I did not break any rules.

### 3.4.2 Operational Feasibility

This project has no operational concerns, the project is open source, and for research purposes only.

### 3.4.3 Economical Feasibility

This project has no economical concerns, the project is open source, and for research purposes only.


