# 5. System Testing

The following section describes methods that were used to test the decoder and the encoder.

## 5.1 Hardware testing

For the following test I have decided to use a real FM transmitter and a FM receiver. To perform the test, I have encoded a simple 132 byte HTML file. This audio was automatically transmitted out via the encoder (by using `--stream` command line argument). This audio was then forwarded into a FM transmitter via the workstation's speaker output. The audio was transmitted and then received on the other end by the FM receiver. This audio was then forwarded back to the workstation through its microphone input. The audio was then finally decoded through the decoder (by using `--listen` command line argument) and the file was decoded back to its original HTML form and saved into a specified destination folder.

### 5.1.1 FM hardware

The FM hardware used in these tests were pre-built purchased FM modules. The FM transmitter is a module consisting of BH1417F [@bh1417f] analog integrated circuit. The BH1417F offers a frequency range of 87.7Mhz and 107.9Mhz. This frequency can be chosen by toggling the pins (or in this case, switches on the module board), but the default frequency was set to 107.9MHz. The BH1417F is a pure analog integrated circuit and does not contain any features that could affect the audio, such as automatic gain control. The module was relatively cheap and easy to set up. It only needs a 5V 2.4W input power and audio input signal through the 3.5mm jack connector. The antenna used here is a simple ~20cm wire. I have not used a proper antenna, because both the FM transmitter and the FM receiver were located in the same room next to the workstation. There was simply no reason to add a proper antenna that would extend the transmission distance.

The FM receiver is a module consisting of TEA5767 [@tea5767] digital integrated circuit. This integrated circuit uses a I2C protocol [@i2c] which is used to configure the TEA5767 device. I have used an Arduino based board Arduino Uno [@arduinouno]. The Arduino board contained a custom program that would modify the TEA5767 registers in order to specify the target listening frequency - 107.9MHz. The device also contains registers which allow enabling or disabling of additional extra features such as automatic station finder, automatic gain control, or automatic noise suppression. The device used relatively low power and only required 5V and 0.5W of power.

![The FM transmitter with the BH1417 analog integrated circuit. The actual chip is the SOIC SSOP package in the middle.](images/CH2BH1417.jpg)

![The FM receiver with the TEA5767 digital integrated circuit. The actual chip is the BGA package on the bottom left.](images/TEA5767.jpg)

### 5.1.2 FM results

Before I have tested this project with a real FM hardware, I have first performed a baseline test. This baseline test was made to verify that everything, excluding the FM hardware, works as expected. The test was done by connecting the audio output straight into the microphone input of the workstation. By doing this, we eliminate a possibility that a problem, if any, is inside of the motherboard (the sound card) or in the program itself. I have encoded a simple 132 HTML file, sent it out via the encoder-cli, and let the decoder-cli listen to the audio. The test was successful, I was able to get the exact same HTML file back. In the next figures, you can see the intercepted recording of the audio (that has returned back via microphone). Additionally, I have performed a fast fourier transform analysis on an empty region of the audio, to see if there are any artifacts or high noise that could affect other tests.

![The recording of the transmitted audio that has returned back via the microphone input. This is a baseline test, no FM hardware was used. The audio has returned in a perfect shape, therefore no issues with the workstation or the setup.](images/wav-base.png)

![The fast fourier transform applied over an empty region of the recording above. This shows that there is no background noise introduced by the workstation (the sound card) or by the cable that connects the audio input/output.](images/fft-silence.png)

The first FM test conduced was not a success. As shown in the following figures below, the audio that has returned back to the decoder contains artifacts and is completely out of the shape compared to the baseline test. After spending a lot of time figuring out the cause of this issue, I have found out that this was due to the internal settings of the TEA5767, and the fact that the power supply was insufficient. The power supply in the first tests was a 5V 500mA supply via a USB-A from the workstation. The USB power is not meant to be used this way at all. Additionally, the default values of the TEA5767 are set in a way to enable noise reduction and automatic gain control. This was especially noticeable when a random audio recording was played through the FM transmitter, the receiver has lowered it's output audio gain once it has noticed the audio recording has stopped and a silence (with a background noise) started.

![The recording of the transmitted audio that has returned back via the microphone input via the FM transmitter and FM receiver.](images/wav-bad.png)

![The fast fourier transform applied over the recorded region in the previous figure. As we can see here, the frequency spectrum is chaotic, the frequencies we need (4.8KHz and 2.4KHz) are hard to detect.](images/fft-bad.png)

After playing around, I have decided to switch the power supply to a 400W ATX power supply that was laying around. A 400W power supply is indeed more than enough for the test, but it was the only one that was available to me at the time. After changing the power supply and disabling many automatic features of the TEA5767, by setting the correct bits in the registers via I2C, I was able to perform a successful tests. The decoder was able to detect and decode the packets, and finally assemble the HTML file which was identical to the original.

![The new recording of the transmitted audio that has returned back via the microphone input via the FM transmitter and FM receiver.](images/wav-good.png)

![The fast fourier transform applied over the recorded region in the previous figure. As we can see here, the frequencies we are looking for can be easily detected.](images/fft-good.png)

## 5.2 Performance testing

To test the performance of the decoder, I have chosen to test the decoder on three different machines. The encoder was not tested, because it does not have to be in real-time. However, the decoder must be able to receive audio and process it in real-time. The following benchmarks below are using a unit test, which decodes 1MB of data. The 1MB of data is encoded into 48000KHz audio, which creates 734580 float PCM samples, or about 15 second audio file. This audio is pushed into the decoder and the time it takes for the decoder to finish is marked both in the Debug and the MinSizeRel builds. The difference between these two builds is due to the compiler that adds additional real-time checks into the Debug release, while the MinSizeRel release contains no real-time checks, and is optimized and minimized. I was able to only test the decoder on 3 different CPUs, because I did not have more hardware to test on.

The following bar charts contain an average single-thread CPU score and a multi-thread CPU score. These two scores give a rough estimate of the power of the CPU. All CPU scores were extracted from a public database of CPU benchmark data from the PassMark Software [@passmark]. The time it took for the decoder to process the data is marked in milliseconds, where lower score is better. We can clearly see that the decoder scales efficiently with a better and faster CPU. A problem would arise if the decoder time would remain flat across different CPUs. The decoder runs on a single thread, therefore it does not scale with the number of CPUs. While it can seem that ~500ms on a laptop CPU (i5-3380M) as a long time, the length of the audio encoded is around 15 seconds. This means that the decoder is capable of processing 16-17x times of the speed of the audio as it comes in.

![Benchmark of the decoder parsing 1MB data on three different CPUs - Debug build.](images/benchmark-debug.png)

![Benchmark of the decoder parsing 1MB data on three different CPUs - MinSizeRel build.](images/benchmark-minsizerel.png)

