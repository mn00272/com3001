mkdir build
markdown-pp index.mdpp -o build/report.md
pandoc --filter pandoc-citeproc --bibliography=bibliography.bib --variable papersize=a4paper --csl=ieee.csl --listings --include-in-header mixins/titlesec.tex --include-in-header mixins/fix-captions.tex --template %APPDATA%\pandoc\templates\eisvogel.tex -o build/report.pdf build/report.md
