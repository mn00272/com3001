#!/bin/bash
set -e

mkdir -p build

cat HEAD.md > build/report.md
cat chapters/originality.md >> build/report.md
cat chapters/abstract.md >> build/report.md
cat chapters/acknowledgement.md >> build/report.md
cat chapters/abbreviations.md >> build/report.md
cat chapters/contents.md >> build/report.md
cat chapters/introduction.md >> build/report.md
cat chapters/literature.md >> build/report.md
cat chapters/requirements.md >> build/report.md
cat chapters/design.md >> build/report.md
cat chapters/testing.md >> build/report.md
cat chapters/conclusion.md >> build/report.md
cat TAIL.md >> build/report.md

pandoc \
    --filter pandoc-citeproc \
    --bibliography=bibliography.bib \
    --variable papersize=a4paper \
    --csl=ieee.csl \
    --listings \
    --include-in-header mixins/titlesec.tex \
    --include-in-header mixins/fix-captions.tex \
    --template pandoc-latex-template/eisvogel.tex \
    -o build/report.pdf \
    build/report.md
